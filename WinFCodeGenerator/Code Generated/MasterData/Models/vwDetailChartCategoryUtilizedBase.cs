using System;
using System.Collections.Generic;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
     public partial class vwDetailChartCategoryUtilizedBase : BaseModel
     {
         public vwDetailChartCategoryUtilized vwDetailChartCategoryUtilized { get; set; }
         public List<vwDetailChartCategoryUtilized> vwDetailChartCategoryUtilizedList { get; set; }
     }
}
