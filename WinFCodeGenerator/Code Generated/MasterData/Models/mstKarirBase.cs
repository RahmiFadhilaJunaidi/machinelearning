using System;
using System.Collections.Generic;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
     public partial class mstKarirBase : BaseModel
     {
         public mstKarir mstKarir { get; set; }
         public List<mstKarir> mstKarirList { get; set; }
     }
}
