using System;
using System.Collections.Generic;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
     public partial class MstSoalBase : BaseModel
     {
         public MstSoal MstSoal { get; set; }
         public List<MstSoal> MstSoalList { get; set; }
     }
}
