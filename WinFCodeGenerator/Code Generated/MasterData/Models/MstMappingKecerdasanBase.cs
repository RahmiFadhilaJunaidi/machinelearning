using System;
using System.Collections.Generic;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
     public partial class MstMappingKecerdasanBase : BaseModel
     {
         public MstMappingKecerdasan MstMappingKecerdasan { get; set; }
         public List<MstMappingKecerdasan> MstMappingKecerdasanList { get; set; }
     }
}
