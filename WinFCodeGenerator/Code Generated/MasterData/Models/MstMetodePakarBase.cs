using System;
using System.Collections.Generic;

namespace MechineLearningMinatBakat.DA.Models.MasterData
{
     public partial class MstMetodePakarBase : BaseModel
     {
         public MstMetodePakar MstMetodePakar { get; set; }
         public List<MstMetodePakar> MstMetodePakarList { get; set; }
     }
}
