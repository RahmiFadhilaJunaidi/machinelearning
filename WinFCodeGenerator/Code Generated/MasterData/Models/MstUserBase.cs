using System;
using System.Collections.Generic;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
     public partial class MstUserBase : BaseModel
     {
         public MstUser MstUser { get; set; }
         public List<MstUser> MstUserList { get; set; }
     }
}
