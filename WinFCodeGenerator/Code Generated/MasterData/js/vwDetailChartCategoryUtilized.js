var tblvwDetailChartCategoryUtilized='#tblvwDetailChartCategoryUtilized';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#vwDetailChartCategoryUtilizedForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                Nama: $('#Nama').val(),
                SangatTidakSetuju: $('#SangatTidakSetuju').val(),
                TidakSetuju: $('#TidakSetuju').val(),
                KurangSetuju: $('#KurangSetuju').val(),
                Setuju: $('#Setuju').val(),
                KodeCategory: $('#KodeCategory').val(),
                };
     var result = Common.Form.Save('/vwDetailChartCategoryUtilized/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/vwDetailChartCategoryUtilized/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#Nama').val('');
      $('#SangatTidakSetuju').val('');
      $('#TidakSetuju').val('');
      $('#KurangSetuju').val('');
      $('#Setuju').val('');
      $('#KodeCategory').val('');
    },
}

var FormValidation = {
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblvwDetailChartCategoryUtilized);
       },
       LoadData: function () {
          return Common.GetData.Get('/vwDetailChartCategoryUtilized/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'nama'},
                    {'data':'sangatTidakSetuju'},
                    {'data':'tidakSetuju'},
                    {'data':'kurangSetuju'},
                    {'data':'setuju'},
                    {'data':'kodeCategory'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblvwDetailChartCategoryUtilized, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblvwDetailChartCategoryUtilized).unbind();
              $(tblvwDetailChartCategoryUtilized).on('click', '.btnRemove', function (e) {
                  var tb = $(tblvwDetailChartCategoryUtilized).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblvwDetailChartCategoryUtilized).on('click', '.btnEdit', function (e) {
                  var tb = $(tblvwDetailChartCategoryUtilized).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#Nama').val(row.nama);
                  $('#SangatTidakSetuju').val(row.sangatTidakSetuju);
                  $('#TidakSetuju').val(row.tidakSetuju);
                  $('#KurangSetuju').val(row.kurangSetuju);
                  $('#Setuju').val(row.setuju);
                  $('#KodeCategory').val(row.kodeCategory);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
