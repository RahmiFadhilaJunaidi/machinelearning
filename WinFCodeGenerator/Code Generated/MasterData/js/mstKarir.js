var tblmstKarir='#tblmstKarir';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#mstKarirForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                IdKarir: $('#IdKarir').val(),
                Karir: $('#Karir').val(),
                Description: $('#Description').val(),
                CreatedBy: $('#CreatedBy').val(),
                CreatedDate: $('#CreatedDate').val(),
                UpdateBy: $('#UpdateBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                };
     var result = Common.Form.Save('/mstKarir/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/mstKarir/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#IdKarir').val('');
      $('#Karir').val('');
      $('#Description').val('');
      $('#CreatedBy').val('');
      $('#CreatedDate').val('');
      $('#UpdateBy').val('');
      $('#UpdatedDate').val('');
    },
}

var FormValidation = {
    IdKarir: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblmstKarir);
       },
       LoadData: function () {
          return Common.GetData.Get('/mstKarir/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'idKarir'},
                    {'data':'karir'},
                    {'data':'description'},
                    {'data':'createdBy'},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updateBy'},
                    {'data':'updatedDate', render: function (data){ return Common.Format.Date(data);}},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblmstKarir, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblmstKarir).unbind();
              $(tblmstKarir).on('click', '.btnRemove', function (e) {
                  var tb = $(tblmstKarir).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        IdKarir : row.idKarir,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblmstKarir).on('click', '.btnEdit', function (e) {
                  var tb = $(tblmstKarir).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#IdKarir').val(row.idKarir);
                  $('#Karir').val(row.karir);
                  $('#Description').val(row.description);
                  $('#CreatedBy').val(row.createdBy);
                  $('#CreatedDate').val(row.createdDate);
                  $('#UpdateBy').val(row.updateBy);
                  $('#UpdatedDate').val(row.updatedDate);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
