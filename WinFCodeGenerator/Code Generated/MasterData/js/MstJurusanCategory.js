var tblMstJurusanCategory='#tblMstJurusanCategory';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#MstJurusanCategoryForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                IdJurusanCatgeory: $('#IdJurusanCatgeory').val(),
                KodeJurusanCategory: $('#KodeJurusanCategory').val(),
                JurusanCategory: $('#JurusanCategory').val(),
                CreatedBy: $('#CreatedBy').val(),
                CreatedDate: $('#CreatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                UpdateDate: $('#UpdateDate').val(),
                };
     var result = Common.Form.Save('/MstJurusanCategory/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/MstJurusanCategory/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#IdJurusanCatgeory').val('');
      $('#KodeJurusanCategory').val('');
      $('#JurusanCategory').val('');
      $('#CreatedBy').val('');
      $('#CreatedDate').val('');
      $('#UpdatedBy').val('');
      $('#UpdateDate').val('');
    },
}

var FormValidation = {
    IdJurusanCatgeory: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblMstJurusanCategory);
       },
       LoadData: function () {
          return Common.GetData.Get('/MstJurusanCategory/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'idJurusanCatgeory'},
                    {'data':'kodeJurusanCategory'},
                    {'data':'jurusanCategory'},
                    {'data':'createdBy'},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updatedBy'},
                    {'data':'updateDate', render: function (data){ return Common.Format.Date(data);}},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblMstJurusanCategory, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblMstJurusanCategory).unbind();
              $(tblMstJurusanCategory).on('click', '.btnRemove', function (e) {
                  var tb = $(tblMstJurusanCategory).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        IdJurusanCatgeory : row.idJurusanCatgeory,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblMstJurusanCategory).on('click', '.btnEdit', function (e) {
                  var tb = $(tblMstJurusanCategory).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#IdJurusanCatgeory').val(row.idJurusanCatgeory);
                  $('#KodeJurusanCategory').val(row.kodeJurusanCategory);
                  $('#JurusanCategory').val(row.jurusanCategory);
                  $('#CreatedBy').val(row.createdBy);
                  $('#CreatedDate').val(row.createdDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  $('#UpdateDate').val(row.updateDate);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
