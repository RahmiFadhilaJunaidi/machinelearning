var tblMstMappingKecerdasan='#tblMstMappingKecerdasan';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#MstMappingKecerdasanForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                IdMappingKecerdasan: $('#IdMappingKecerdasan').val(),
                KodeCategory: $('#KodeCategory').val(),
                KodeJurusan: $('#KodeJurusan').val(),
                KodeSoal: $('#KodeSoal').val(),
                CreatedBy: $('#CreatedBy').val(),
                CreatedDate: $('#CreatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                UpdateDate: $('#UpdateDate').val(),
                };
     var result = Common.Form.Save('/MstMappingKecerdasan/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/MstMappingKecerdasan/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#IdMappingKecerdasan').val('');
      $('#KodeCategory').val('');
      $('#KodeJurusan').val('');
      $('#KodeSoal').val('');
      $('#CreatedBy').val('');
      $('#CreatedDate').val('');
      $('#UpdatedBy').val('');
      $('#UpdateDate').val('');
    },
}

var FormValidation = {
    IdMappingKecerdasan: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblMstMappingKecerdasan);
       },
       LoadData: function () {
          return Common.GetData.Get('/MstMappingKecerdasan/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'idMappingKecerdasan'},
                    {'data':'kodeCategory'},
                    {'data':'kodeJurusan'},
                    {'data':'kodeSoal'},
                    {'data':'createdBy'},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updatedBy'},
                    {'data':'updateDate', render: function (data){ return Common.Format.Date(data);}},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblMstMappingKecerdasan, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblMstMappingKecerdasan).unbind();
              $(tblMstMappingKecerdasan).on('click', '.btnRemove', function (e) {
                  var tb = $(tblMstMappingKecerdasan).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        IdMappingKecerdasan : row.idMappingKecerdasan,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblMstMappingKecerdasan).on('click', '.btnEdit', function (e) {
                  var tb = $(tblMstMappingKecerdasan).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#IdMappingKecerdasan').val(row.idMappingKecerdasan);
                  $('#KodeCategory').val(row.kodeCategory);
                  $('#KodeJurusan').val(row.kodeJurusan);
                  $('#KodeSoal').val(row.kodeSoal);
                  $('#CreatedBy').val(row.createdBy);
                  $('#CreatedDate').val(row.createdDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  $('#UpdateDate').val(row.updateDate);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
