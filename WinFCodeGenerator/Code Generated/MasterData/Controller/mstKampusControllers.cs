using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;

namespace MachineLearningMinatBakat.Controllers
{
 public class mstKampusController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/mstKampus.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new mstKampusBase();
         try
         {
             var service = new mstKampusService();
             result = service.mstKampusGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.mstKampusList });
     }


     [HttpPost]
     public IActionResult Create(mstKampus Post)
     {
         var respon = new Respon();
         try
         {
             var service = new mstKampusService();
             respon.errorMessage = service.mstKampusCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(mstKampus Post)
     {
         var respon = new Respon();
         try
         {
             var service = new mstKampusService();
             respon.errorMessage = service.mstKampusUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(mstKampus Post)
     {
         var respon = new Respon();
         try
         {
             var service = new mstKampusService();
             respon.errorMessage = service.mstKampusDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
