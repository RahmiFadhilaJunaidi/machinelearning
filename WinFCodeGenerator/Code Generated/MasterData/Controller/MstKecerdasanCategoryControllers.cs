using System;
using Microsoft.AspNetCore.Mvc;
using MechineLearningMinatBakat.DA.Service.MasterData;
using MechineLearningMinatBakat.DA.Models.MasterData;

namespace MechineLearningMinatBakat.Controllers
{
 public class MstKecerdasanCategoryController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/MstKecerdasanCategory.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstKecerdasanCategoryBase();
         try
         {
             var service = new MstKecerdasanCategoryService();
             result = service.MstKecerdasanCategoryGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstKecerdasanCategoryList });
     }


     [HttpPost]
     public IActionResult Create(MstKecerdasanCategory Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstKecerdasanCategoryService();
             respon.errorMessage = service.MstKecerdasanCategoryCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstKecerdasanCategory Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstKecerdasanCategoryService();
             respon.errorMessage = service.MstKecerdasanCategoryUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstKecerdasanCategory Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstKecerdasanCategoryService();
             respon.errorMessage = service.MstKecerdasanCategoryDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
