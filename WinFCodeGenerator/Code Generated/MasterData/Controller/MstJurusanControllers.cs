using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;

namespace MachineLearningMinatBakat.Controllers
{
 public class MstJurusanController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/MstJurusan.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstJurusanBase();
         try
         {
             var service = new MstJurusanService();
             result = service.MstJurusanGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstJurusanList });
     }


     [HttpPost]
     public IActionResult Create(MstJurusan Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstJurusanService();
             respon.errorMessage = service.MstJurusanCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstJurusan Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstJurusanService();
             respon.errorMessage = service.MstJurusanUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstJurusan Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstJurusanService();
             respon.errorMessage = service.MstJurusanDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
