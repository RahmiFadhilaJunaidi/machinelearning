using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;

namespace MachineLearningMinatBakat.Controllers
{
 public class vwDetailChartCategoryUtilizedController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/vwDetailChartCategoryUtilized.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new vwDetailChartCategoryUtilizedBase();
         try
         {
             var service = new vwDetailChartCategoryUtilizedService();
             result = service.vwDetailChartCategoryUtilizedGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.vwDetailChartCategoryUtilizedList });
     }


     [HttpPost]
     public IActionResult Create(vwDetailChartCategoryUtilized Post)
     {
         var respon = new Respon();
         try
         {
             var service = new vwDetailChartCategoryUtilizedService();
             respon.errorMessage = service.vwDetailChartCategoryUtilizedCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(vwDetailChartCategoryUtilized Post)
     {
         var respon = new Respon();
         try
         {
             var service = new vwDetailChartCategoryUtilizedService();
             respon.errorMessage = service.vwDetailChartCategoryUtilizedUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(vwDetailChartCategoryUtilized Post)
     {
         var respon = new Respon();
         try
         {
             var service = new vwDetailChartCategoryUtilizedService();
             respon.errorMessage = service.vwDetailChartCategoryUtilizedDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
