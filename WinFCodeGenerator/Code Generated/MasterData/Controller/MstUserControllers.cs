using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;

namespace MachineLearningMinatBakat.Controllers
{
 public class MstUserController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/MstUser.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstUserBase();
         try
         {
             var service = new MstUserService();
             result = service.MstUserGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstUserList });
     }


     [HttpPost]
     public IActionResult Create(MstUser Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstUserService();
             respon.errorMessage = service.MstUserCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstUser Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstUserService();
             respon.errorMessage = service.MstUserUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstUser Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstUserService();
             respon.errorMessage = service.MstUserDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
