using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using MachineLearningMinatBakat.DA.Context;
using MachineLearningMinatBakat.DA.Models.MasterData;

namespace MachineLearningMinatBakat.DA.Service.MasterData
{
 public class MasterDataMasterService
 {
     BaseModel baseModel = new BaseModel();

     public  vwDetailChartCategoryUtilizedBase vwDetailChartCategoryUtilizedGetList()
     {
         var listBase = new vwDetailChartCategoryUtilizedBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.vwDetailChartCategoryUtilizedList = context.vwDetailChartCategoryUtilized.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
         return listBase;
         }
     }

 }
}
