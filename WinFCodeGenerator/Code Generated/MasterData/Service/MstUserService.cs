using System;
using System.Collections.Generic;
using System.Linq;
using MachineLearningMinatBakat.DA.Models.MasterData;

namespace MachineLearningMinatBakat.DA.Service.MasterData
 {
 public class MstUserService
 {
     BaseModel baseModel = new BaseModel();

     public MstUserBase MstUserGetList()
     {
         var listBase = new MstUserBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.MstUserList = context.MstUser.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstUserDelete(MstUser Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstUser.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstUserCreate(MstUser Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstUser.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstUserUpdate(MstUser Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstUser.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
