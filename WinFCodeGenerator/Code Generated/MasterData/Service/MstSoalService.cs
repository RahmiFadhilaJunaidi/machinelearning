using System;
using System.Collections.Generic;
using System.Linq;
using MachineLearningMinatBakat.DA.Models.MasterData;

namespace MachineLearningMinatBakat.DA.Service.MasterData
 {
 public class MstSoalService
 {
     BaseModel baseModel = new BaseModel();

     public MstSoalBase MstSoalGetList()
     {
         var listBase = new MstSoalBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.MstSoalList = context.MstSoal.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstSoalDelete(MstSoal Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstSoal.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstSoalCreate(MstSoal Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstSoal.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstSoalUpdate(MstSoal Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstSoal.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
