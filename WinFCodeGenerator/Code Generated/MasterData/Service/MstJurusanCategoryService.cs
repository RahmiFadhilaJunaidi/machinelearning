using System;
using System.Collections.Generic;
using System.Linq;
using MachineLearningMinatBakat.DA.Models.MasterData;

namespace MachineLearningMinatBakat.DA.Service.MasterData
 {
 public class MstJurusanCategoryService
 {
     BaseModel baseModel = new BaseModel();

     public MstJurusanCategoryBase MstJurusanCategoryGetList()
     {
         var listBase = new MstJurusanCategoryBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.MstJurusanCategoryList = context.MstJurusanCategory.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstJurusanCategoryDelete(MstJurusanCategory Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstJurusanCategory.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstJurusanCategoryCreate(MstJurusanCategory Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstJurusanCategory.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstJurusanCategoryUpdate(MstJurusanCategory Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstJurusanCategory.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
