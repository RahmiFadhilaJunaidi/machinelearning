using System;
using System.Collections.Generic;
using System.Linq;
using MechineLearningMinatBakat.DA.Models.MasterData;

namespace MechineLearningMinatBakat.DA.Service.MasterData
 {
 public class MstKecerdasanCategoryService
 {
     BaseModel baseModel = new BaseModel();

     public MstKecerdasanCategoryBase MstKecerdasanCategoryGetList()
     {
         var listBase = new MstKecerdasanCategoryBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.MstKecerdasanCategoryList = context.MstKecerdasanCategory.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstKecerdasanCategoryDelete(MstKecerdasanCategory Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstKecerdasanCategory.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstKecerdasanCategoryCreate(MstKecerdasanCategory Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstKecerdasanCategory.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstKecerdasanCategoryUpdate(MstKecerdasanCategory Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstKecerdasanCategory.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
