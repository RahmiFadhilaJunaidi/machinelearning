using System;
using System.Collections.Generic;
using System.Linq;
using MachineLearningMinatBakat.DA.Models.MasterData;

namespace MachineLearningMinatBakat.DA.Service.MasterData
 {
 public class MstJurusanService
 {
     BaseModel baseModel = new BaseModel();

     public MstJurusanBase MstJurusanGetList()
     {
         var listBase = new MstJurusanBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.MstJurusanList = context.MstJurusan.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstJurusanDelete(MstJurusan Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstJurusan.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstJurusanCreate(MstJurusan Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstJurusan.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstJurusanUpdate(MstJurusan Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstJurusan.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
