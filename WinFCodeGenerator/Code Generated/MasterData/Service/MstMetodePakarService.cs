using System;
using System.Collections.Generic;
using System.Linq;
using MechineLearningMinatBakat.DA.Models.MasterData;

namespace MechineLearningMinatBakat.DA.Service.MasterData
 {
 public class MstMetodePakarService
 {
     BaseModel baseModel = new BaseModel();

     public MstMetodePakarBase MstMetodePakarGetList()
     {
         var listBase = new MstMetodePakarBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.MstMetodePakarList = context.MstMetodePakar.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstMetodePakarDelete(MstMetodePakar Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstMetodePakar.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstMetodePakarCreate(MstMetodePakar Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstMetodePakar.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstMetodePakarUpdate(MstMetodePakar Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstMetodePakar.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
