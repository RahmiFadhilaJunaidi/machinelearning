var tblTrxAttendanceTeacher='#tblTrxAttendanceTeacher';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#TrxAttendanceTeacherForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                TrxAttendanceId: $('#TrxAttendanceId').val(),
                TimeIn: $('#TimeIn').val(),
                TimeOut: $('#TimeOut').val(),
                TeacherId: $('#TeacherId').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                };
     var result = Common.Form.Save('/TrxAttendanceTeacher/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/TrxAttendanceTeacher/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#TrxAttendanceId').val('');
      $('#TimeIn').val('');
      $('#TimeOut').val('');
      $('#TeacherId').val('');
      $('#UpdatedDate').val('');
      $('#UpdatedBy').val('');
    },
}

var FormValidation = {
    TrxAttendanceId: {
     presence: true,
     },
    TimeIn: {
     presence: true,
     },
    TimeOut: {
     presence: true,
     },
    TeacherId: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblTrxAttendanceTeacher);
       },
       LoadData: function () {
          return Common.GetData.Get('/TrxAttendanceTeacher/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'trxAttendanceId'},
                    {'data':'timeIn', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'timeOut', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'teacherId'},
                    {'data':'updatedDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updatedBy'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblTrxAttendanceTeacher, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblTrxAttendanceTeacher).unbind();
              $(tblTrxAttendanceTeacher).on('click', '.btnRemove', function (e) {
                  var tb = $(tblTrxAttendanceTeacher).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        TrxAttendanceId : row.trxAttendanceId,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblTrxAttendanceTeacher).on('click', '.btnEdit', function (e) {
                  var tb = $(tblTrxAttendanceTeacher).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#TrxAttendanceId').val(row.trxAttendanceId);
                  $('#TimeIn').val(row.timeIn);
                  $('#TimeOut').val(row.timeOut);
                  $('#TeacherId').val(row.teacherId);
                  $('#UpdatedDate').val(row.updatedDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
