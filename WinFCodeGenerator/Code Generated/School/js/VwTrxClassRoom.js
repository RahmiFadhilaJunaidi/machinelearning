var tblVwTrxClassRoom='#tblVwTrxClassRoom';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#VwTrxClassRoomForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                TrxClassRoomId: $('#TrxClassRoomId').val(),
                ClassRoomId: $('#ClassRoomId').val(),
                StudentId: $('#StudentId').val(),
                LessonYear: $('#LessonYear').val(),
                CreatedDate: $('#CreatedDate').val(),
                CreatedBy: $('#CreatedBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                Studentname: $('#Studentname').val(),
                };
     var result = Common.Form.Save('/VwTrxClassRoom/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/VwTrxClassRoom/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#TrxClassRoomId').val('');
      $('#ClassRoomId').val('');
      $('#StudentId').val('');
      $('#LessonYear').val('');
      $('#CreatedDate').val('');
      $('#CreatedBy').val('');
      $('#UpdatedDate').val('');
      $('#UpdatedBy').val('');
      $('#Studentname').val('');
    },
}

var FormValidation = {
    TrxClassRoomId: {
     presence: true,
     },
    ClassRoomId: {
     presence: true,
     },
    StudentId: {
     presence: true,
     },
    LessonYear: {
     presence: true,
     },
    CreatedDate: {
     presence: true,
     },
    CreatedBy: {
     presence: true,
     },
    Studentname: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblVwTrxClassRoom);
       },
       LoadData: function () {
          return Common.GetData.Get('/VwTrxClassRoom/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'trxClassRoomId'},
                    {'data':'classRoomId'},
                    {'data':'studentId'},
                    {'data':'lessonYear'},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'createdBy'},
                    {'data':'updatedDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updatedBy'},
                    {'data':'studentname'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblVwTrxClassRoom, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblVwTrxClassRoom).unbind();
              $(tblVwTrxClassRoom).on('click', '.btnRemove', function (e) {
                  var tb = $(tblVwTrxClassRoom).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblVwTrxClassRoom).on('click', '.btnEdit', function (e) {
                  var tb = $(tblVwTrxClassRoom).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#TrxClassRoomId').val(row.trxClassRoomId);
                  $('#ClassRoomId').val(row.classRoomId);
                  $('#StudentId').val(row.studentId);
                  $('#LessonYear').val(row.lessonYear);
                  $('#CreatedDate').val(row.createdDate);
                  $('#CreatedBy').val(row.createdBy);
                  $('#UpdatedDate').val(row.updatedDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  $('#Studentname').val(row.studentname);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
