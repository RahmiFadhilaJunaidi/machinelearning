var tblVwTrxExam='#tblVwTrxExam';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#VwTrxExamForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                MstExamId: $('#MstExamId').val(),
                TrxLessonId: $('#TrxLessonId').val(),
                StudentId: $('#StudentId').val(),
                Score: $('#Score').val(),
                CreatedDate: $('#CreatedDate').val(),
                CreatedBy: $('#CreatedBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                StudentName: $('#StudentName').val(),
                MstLessonId: $('#MstLessonId').val(),
                LessonCode: $('#LessonCode').val(),
                LessonName: $('#LessonName').val(),
                LessonYear: $('#LessonYear').val(),
                Semester: $('#Semester').val(),
                StandardScore: $('#StandardScore').val(),
                ClassRoomId: $('#ClassRoomId').val(),
                LessonId: $('#LessonId').val(),
                TeacherId: $('#TeacherId').val(),
                TeacherName: $('#TeacherName').val(),
                };
     var result = Common.Form.Save('/VwTrxExam/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/VwTrxExam/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#MstExamId').val('');
      $('#TrxLessonId').val('');
      $('#StudentId').val('');
      $('#Score').val('');
      $('#CreatedDate').val('');
      $('#CreatedBy').val('');
      $('#UpdatedDate').val('');
      $('#UpdatedBy').val('');
      $('#StudentName').val('');
      $('#MstLessonId').val('');
      $('#LessonCode').val('');
      $('#LessonName').val('');
      $('#LessonYear').val('');
      $('#Semester').val('');
      $('#StandardScore').val('');
      $('#ClassRoomId').val('');
      $('#LessonId').val('');
      $('#TeacherId').val('');
      $('#TeacherName').val('');
    },
}

var FormValidation = {
    MstExamId: {
     presence: true,
     },
    TrxLessonId: {
     presence: true,
     },
    StudentId: {
     presence: true,
     },
    Score: {
     presence: true,
     },
    CreatedDate: {
     presence: true,
     },
    CreatedBy: {
     presence: true,
     },
    StudentName: {
     presence: true,
     },
    MstLessonId: {
     presence: true,
     },
    LessonCode: {
     presence: true,
     },
    LessonName: {
     presence: true,
     },
    LessonYear: {
     presence: true,
     },
    Semester: {
     presence: true,
     },
    StandardScore: {
     presence: true,
     },
    ClassRoomId: {
     presence: true,
     },
    LessonId: {
     presence: true,
     },
    TeacherId: {
     presence: true,
     },
    TeacherName: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblVwTrxExam);
       },
       LoadData: function () {
          return Common.GetData.Get('/VwTrxExam/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'mstExamId'},
                    {'data':'trxLessonId'},
                    {'data':'studentId'},
                    {'data':'score'},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'createdBy'},
                    {'data':'updatedDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updatedBy'},
                    {'data':'studentName'},
                    {'data':'mstLessonId'},
                    {'data':'lessonCode'},
                    {'data':'lessonName'},
                    {'data':'lessonYear'},
                    {'data':'semester'},
                    {'data':'standardScore'},
                    {'data':'classRoomId'},
                    {'data':'lessonId'},
                    {'data':'teacherId'},
                    {'data':'teacherName'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblVwTrxExam, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblVwTrxExam).unbind();
              $(tblVwTrxExam).on('click', '.btnRemove', function (e) {
                  var tb = $(tblVwTrxExam).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblVwTrxExam).on('click', '.btnEdit', function (e) {
                  var tb = $(tblVwTrxExam).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#MstExamId').val(row.mstExamId);
                  $('#TrxLessonId').val(row.trxLessonId);
                  $('#StudentId').val(row.studentId);
                  $('#Score').val(row.score);
                  $('#CreatedDate').val(row.createdDate);
                  $('#CreatedBy').val(row.createdBy);
                  $('#UpdatedDate').val(row.updatedDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  $('#StudentName').val(row.studentName);
                  $('#MstLessonId').val(row.mstLessonId);
                  $('#LessonCode').val(row.lessonCode);
                  $('#LessonName').val(row.lessonName);
                  $('#LessonYear').val(row.lessonYear);
                  $('#Semester').val(row.semester);
                  $('#StandardScore').val(row.standardScore);
                  $('#ClassRoomId').val(row.classRoomId);
                  $('#LessonId').val(row.lessonId);
                  $('#TeacherId').val(row.teacherId);
                  $('#TeacherName').val(row.teacherName);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
