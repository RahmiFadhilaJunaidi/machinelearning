var tblVwTrxLesson='#tblVwTrxLesson';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#VwTrxLessonForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                TrxLessonId: $('#TrxLessonId').val(),
                TeacherId: $('#TeacherId').val(),
                LessonId: $('#LessonId').val(),
                LessonYear: $('#LessonYear').val(),
                ClassRoomId: $('#ClassRoomId').val(),
                StandardScore: $('#StandardScore').val(),
                Semester: $('#Semester').val(),
                CreatedDate: $('#CreatedDate').val(),
                CreatedBy: $('#CreatedBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                ClassRoomCode: $('#ClassRoomCode').val(),
                ClassRoomName: $('#ClassRoomName').val(),
                TeacherName: $('#TeacherName').val(),
                LessonName: $('#LessonName').val(),
                };
     var result = Common.Form.Save('/VwTrxLesson/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/VwTrxLesson/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#TrxLessonId').val('');
      $('#TeacherId').val('');
      $('#LessonId').val('');
      $('#LessonYear').val('');
      $('#ClassRoomId').val('');
      $('#StandardScore').val('');
      $('#Semester').val('');
      $('#CreatedDate').val('');
      $('#CreatedBy').val('');
      $('#UpdatedDate').val('');
      $('#UpdatedBy').val('');
      $('#ClassRoomCode').val('');
      $('#ClassRoomName').val('');
      $('#TeacherName').val('');
      $('#LessonName').val('');
    },
}

var FormValidation = {
    TrxLessonId: {
     presence: true,
     },
    TeacherId: {
     presence: true,
     },
    LessonId: {
     presence: true,
     },
    LessonYear: {
     presence: true,
     },
    ClassRoomId: {
     presence: true,
     },
    StandardScore: {
     presence: true,
     },
    Semester: {
     presence: true,
     },
    CreatedDate: {
     presence: true,
     },
    CreatedBy: {
     presence: true,
     },
    ClassRoomCode: {
     presence: true,
     },
    ClassRoomName: {
     presence: true,
     },
    TeacherName: {
     presence: true,
     },
    LessonName: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblVwTrxLesson);
       },
       LoadData: function () {
          return Common.GetData.Get('/VwTrxLesson/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'trxLessonId'},
                    {'data':'teacherId'},
                    {'data':'lessonId'},
                    {'data':'lessonYear'},
                    {'data':'classRoomId'},
                    {'data':'standardScore'},
                    {'data':'semester'},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'createdBy'},
                    {'data':'updatedDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updatedBy'},
                    {'data':'classRoomCode'},
                    {'data':'classRoomName'},
                    {'data':'teacherName'},
                    {'data':'lessonName'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblVwTrxLesson, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblVwTrxLesson).unbind();
              $(tblVwTrxLesson).on('click', '.btnRemove', function (e) {
                  var tb = $(tblVwTrxLesson).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblVwTrxLesson).on('click', '.btnEdit', function (e) {
                  var tb = $(tblVwTrxLesson).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#TrxLessonId').val(row.trxLessonId);
                  $('#TeacherId').val(row.teacherId);
                  $('#LessonId').val(row.lessonId);
                  $('#LessonYear').val(row.lessonYear);
                  $('#ClassRoomId').val(row.classRoomId);
                  $('#StandardScore').val(row.standardScore);
                  $('#Semester').val(row.semester);
                  $('#CreatedDate').val(row.createdDate);
                  $('#CreatedBy').val(row.createdBy);
                  $('#UpdatedDate').val(row.updatedDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  $('#ClassRoomCode').val(row.classRoomCode);
                  $('#ClassRoomName').val(row.classRoomName);
                  $('#TeacherName').val(row.teacherName);
                  $('#LessonName').val(row.lessonName);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
