var tblMstTeacher='#tblMstTeacher';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#MstTeacherForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                MstTeacherId: $('#MstTeacherId').val(),
                TeacherName: $('#TeacherName').val(),
                Degree: $('#Degree').val(),
                Address: $('#Address').val(),
                PhoneNumber: $('#PhoneNumber').val(),
                Email: $('#Email').val(),
                BirthDay: $('#BirthDay').val(),
                CreatedDate: $('#CreatedDate').val(),
                CreatedBy: $('#CreatedBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                };
     var result = Common.Form.Save('/MstTeacher/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/MstTeacher/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#MstTeacherId').val('');
      $('#TeacherName').val('');
      $('#Degree').val('');
      $('#Address').val('');
      $('#PhoneNumber').val('');
      $('#Email').val('');
      $('#BirthDay').val('');
      $('#CreatedDate').val('');
      $('#CreatedBy').val('');
      $('#UpdatedDate').val('');
      $('#UpdatedBy').val('');
    },
}

var FormValidation = {
    MstTeacherId: {
     presence: true,
     },
    TeacherName: {
     presence: true,
     },
    Address: {
     presence: true,
     },
    PhoneNumber: {
     presence: true,
     },
    BirthDay: {
     presence: true,
     },
    CreatedDate: {
     presence: true,
     },
    CreatedBy: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblMstTeacher);
       },
       LoadData: function () {
          return Common.GetData.Get('/MstTeacher/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'mstTeacherId'},
                    {'data':'teacherName'},
                    {'data':'degree'},
                    {'data':'address'},
                    {'data':'phoneNumber'},
                    {'data':'email'},
                    {'data':'birthDay', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'createdBy'},
                    {'data':'updatedDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updatedBy'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblMstTeacher, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblMstTeacher).unbind();
              $(tblMstTeacher).on('click', '.btnRemove', function (e) {
                  var tb = $(tblMstTeacher).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        MstTeacherId : row.mstTeacherId,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblMstTeacher).on('click', '.btnEdit', function (e) {
                  var tb = $(tblMstTeacher).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#MstTeacherId').val(row.mstTeacherId);
                  $('#TeacherName').val(row.teacherName);
                  $('#Degree').val(row.degree);
                  $('#Address').val(row.address);
                  $('#PhoneNumber').val(row.phoneNumber);
                  $('#Email').val(row.email);
                  $('#BirthDay').val(row.birthDay);
                  $('#CreatedDate').val(row.createdDate);
                  $('#CreatedBy').val(row.createdBy);
                  $('#UpdatedDate').val(row.updatedDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
