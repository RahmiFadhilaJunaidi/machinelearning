using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.School;
using LibrarySys.DA.Models.School;

namespace LibrarySys.Controllers
{
 public class MstTeacherController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/School/MstTeacher.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstTeacherBase();
         try
         {
             var service = new MstTeacherService();
             result = service.MstTeacherGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstTeacherList });
     }


     [HttpPost]
     public IActionResult Create(MstTeacher Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstTeacherService();
             respon.errorMessage = service.MstTeacherCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstTeacher Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstTeacherService();
             respon.errorMessage = service.MstTeacherUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstTeacher Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstTeacherService();
             respon.errorMessage = service.MstTeacherDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
