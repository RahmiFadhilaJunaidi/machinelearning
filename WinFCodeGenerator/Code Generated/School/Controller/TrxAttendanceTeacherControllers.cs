using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.School;
using LibrarySys.DA.Models.School;

namespace LibrarySys.Controllers
{
 public class TrxAttendanceTeacherController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/School/TrxAttendanceTeacher.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new TrxAttendanceTeacherBase();
         try
         {
             var service = new TrxAttendanceTeacherService();
             result = service.TrxAttendanceTeacherGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.TrxAttendanceTeacherList });
     }


     [HttpPost]
     public IActionResult Create(TrxAttendanceTeacher Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxAttendanceTeacherService();
             respon.errorMessage = service.TrxAttendanceTeacherCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(TrxAttendanceTeacher Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxAttendanceTeacherService();
             respon.errorMessage = service.TrxAttendanceTeacherUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(TrxAttendanceTeacher Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxAttendanceTeacherService();
             respon.errorMessage = service.TrxAttendanceTeacherDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
