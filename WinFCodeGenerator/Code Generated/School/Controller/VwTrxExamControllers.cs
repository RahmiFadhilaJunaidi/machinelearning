using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.School;
using LibrarySys.DA.Models.School;

namespace LibrarySys.Controllers
{
 public class VwTrxExamController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/School/VwTrxExam.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwTrxExamBase();
         try
         {
             var service = new VwTrxExamService();
             result = service.VwTrxExamGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwTrxExamList });
     }


     [HttpPost]
     public IActionResult Create(VwTrxExam Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwTrxExamService();
             respon.errorMessage = service.VwTrxExamCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(VwTrxExam Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwTrxExamService();
             respon.errorMessage = service.VwTrxExamUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(VwTrxExam Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwTrxExamService();
             respon.errorMessage = service.VwTrxExamDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
