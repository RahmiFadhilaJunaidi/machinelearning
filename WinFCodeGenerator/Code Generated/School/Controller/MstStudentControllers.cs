using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.School;
using LibrarySys.DA.Models.School;

namespace LibrarySys.Controllers
{
 public class MstStudentController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/School/MstStudent.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstStudentBase();
         try
         {
             var service = new MstStudentService();
             result = service.MstStudentGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstStudentList });
     }


     [HttpPost]
     public IActionResult Create(MstStudent Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstStudentService();
             respon.errorMessage = service.MstStudentCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstStudent Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstStudentService();
             respon.errorMessage = service.MstStudentUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstStudent Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstStudentService();
             respon.errorMessage = service.MstStudentDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
