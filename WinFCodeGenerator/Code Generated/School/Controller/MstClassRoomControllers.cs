using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.School;
using LibrarySys.DA.Models.School;

namespace LibrarySys.Controllers
{
 public class MstClassRoomController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/School/MstClassRoom.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstClassRoomBase();
         try
         {
             var service = new MstClassRoomService();
             result = service.MstClassRoomGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstClassRoomList });
     }


     [HttpPost]
     public IActionResult Create(MstClassRoom Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstClassRoomService();
             respon.errorMessage = service.MstClassRoomCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstClassRoom Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstClassRoomService();
             respon.errorMessage = service.MstClassRoomUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstClassRoom Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstClassRoomService();
             respon.errorMessage = service.MstClassRoomDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
