using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.School;
using LibrarySys.DA.Models.School;

namespace LibrarySys.Controllers
{
 public class TrxAttendanceController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/School/TrxAttendance.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new TrxAttendanceBase();
         try
         {
             var service = new TrxAttendanceService();
             result = service.TrxAttendanceGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.TrxAttendanceList });
     }


     [HttpPost]
     public IActionResult Create(TrxAttendance Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxAttendanceService();
             respon.errorMessage = service.TrxAttendanceCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(TrxAttendance Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxAttendanceService();
             respon.errorMessage = service.TrxAttendanceUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(TrxAttendance Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxAttendanceService();
             respon.errorMessage = service.TrxAttendanceDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
