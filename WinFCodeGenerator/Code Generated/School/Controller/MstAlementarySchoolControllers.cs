using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.School;
using LibrarySys.DA.Models.School;

namespace LibrarySys.Controllers
{
 public class MstAlementarySchoolController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/School/MstAlementarySchool.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstAlementarySchoolBase();
         try
         {
             var service = new MstAlementarySchoolService();
             result = service.MstAlementarySchoolGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstAlementarySchoolList });
     }


     [HttpPost]
     public IActionResult Create(MstAlementarySchool Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstAlementarySchoolService();
             respon.errorMessage = service.MstAlementarySchoolCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstAlementarySchool Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstAlementarySchoolService();
             respon.errorMessage = service.MstAlementarySchoolUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstAlementarySchool Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstAlementarySchoolService();
             respon.errorMessage = service.MstAlementarySchoolDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
