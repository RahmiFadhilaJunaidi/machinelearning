using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.School;
using LibrarySys.DA.Models.School;

namespace LibrarySys.Controllers
{
 public class TrxAttendanceStudentController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/School/TrxAttendanceStudent.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new TrxAttendanceStudentBase();
         try
         {
             var service = new TrxAttendanceStudentService();
             result = service.TrxAttendanceStudentGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.TrxAttendanceStudentList });
     }


     [HttpPost]
     public IActionResult Create(TrxAttendanceStudent Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxAttendanceStudentService();
             respon.errorMessage = service.TrxAttendanceStudentCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(TrxAttendanceStudent Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxAttendanceStudentService();
             respon.errorMessage = service.TrxAttendanceStudentUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(TrxAttendanceStudent Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxAttendanceStudentService();
             respon.errorMessage = service.TrxAttendanceStudentDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
