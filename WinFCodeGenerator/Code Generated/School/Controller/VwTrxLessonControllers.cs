using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.School;
using LibrarySys.DA.Models.School;

namespace LibrarySys.Controllers
{
 public class VwTrxLessonController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/School/VwTrxLesson.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwTrxLessonBase();
         try
         {
             var service = new VwTrxLessonService();
             result = service.VwTrxLessonGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwTrxLessonList });
     }


     [HttpPost]
     public IActionResult Create(VwTrxLesson Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwTrxLessonService();
             respon.errorMessage = service.VwTrxLessonCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(VwTrxLesson Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwTrxLessonService();
             respon.errorMessage = service.VwTrxLessonUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(VwTrxLesson Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwTrxLessonService();
             respon.errorMessage = service.VwTrxLessonDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
