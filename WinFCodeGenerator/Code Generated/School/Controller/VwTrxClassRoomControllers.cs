using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.School;
using LibrarySys.DA.Models.School;

namespace LibrarySys.Controllers
{
 public class VwTrxClassRoomController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/School/VwTrxClassRoom.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwTrxClassRoomBase();
         try
         {
             var service = new VwTrxClassRoomService();
             result = service.VwTrxClassRoomGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwTrxClassRoomList });
     }


     [HttpPost]
     public IActionResult Create(VwTrxClassRoom Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwTrxClassRoomService();
             respon.errorMessage = service.VwTrxClassRoomCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(VwTrxClassRoom Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwTrxClassRoomService();
             respon.errorMessage = service.VwTrxClassRoomUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(VwTrxClassRoom Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwTrxClassRoomService();
             respon.errorMessage = service.VwTrxClassRoomDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
