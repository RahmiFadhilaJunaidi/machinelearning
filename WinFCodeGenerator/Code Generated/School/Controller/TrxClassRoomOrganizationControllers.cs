using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.School;
using LibrarySys.DA.Models.School;

namespace LibrarySys.Controllers
{
 public class TrxClassRoomOrganizationController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/School/TrxClassRoomOrganization.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new TrxClassRoomOrganizationBase();
         try
         {
             var service = new TrxClassRoomOrganizationService();
             result = service.TrxClassRoomOrganizationGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.TrxClassRoomOrganizationList });
     }


     [HttpPost]
     public IActionResult Create(TrxClassRoomOrganization Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxClassRoomOrganizationService();
             respon.errorMessage = service.TrxClassRoomOrganizationCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(TrxClassRoomOrganization Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxClassRoomOrganizationService();
             respon.errorMessage = service.TrxClassRoomOrganizationUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(TrxClassRoomOrganization Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxClassRoomOrganizationService();
             respon.errorMessage = service.TrxClassRoomOrganizationDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
