using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class TrxAttendanceBase : BaseModel
     {
         public TrxAttendance TrxAttendance { get; set; }
         public List<TrxAttendance> TrxAttendanceList { get; set; }
     }
}
