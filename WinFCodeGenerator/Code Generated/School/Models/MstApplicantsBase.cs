using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class MstApplicantsBase : BaseModel
     {
         public MstApplicants MstApplicants { get; set; }
         public List<MstApplicants> MstApplicantsList { get; set; }
     }
}
