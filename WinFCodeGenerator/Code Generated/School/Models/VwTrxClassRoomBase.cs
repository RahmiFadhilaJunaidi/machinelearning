using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class VwTrxClassRoomBase : BaseModel
     {
         public VwTrxClassRoom VwTrxClassRoom { get; set; }
         public List<VwTrxClassRoom> VwTrxClassRoomList { get; set; }
     }
}
