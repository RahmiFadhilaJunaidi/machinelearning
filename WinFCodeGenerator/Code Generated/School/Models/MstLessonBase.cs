using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class MstLessonBase : BaseModel
     {
         public MstLesson MstLesson { get; set; }
         public List<MstLesson> MstLessonList { get; set; }
     }
}
