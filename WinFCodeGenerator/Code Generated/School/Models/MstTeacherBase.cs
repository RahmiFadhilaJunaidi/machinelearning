using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class MstTeacherBase : BaseModel
     {
         public MstTeacher MstTeacher { get; set; }
         public List<MstTeacher> MstTeacherList { get; set; }
     }
}
