using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class TrxClassRoomBase : BaseModel
     {
         public TrxClassRoom TrxClassRoom { get; set; }
         public List<TrxClassRoom> TrxClassRoomList { get; set; }
     }
}
