using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class TrxAttendanceTeacherBase : BaseModel
     {
         public TrxAttendanceTeacher TrxAttendanceTeacher { get; set; }
         public List<TrxAttendanceTeacher> TrxAttendanceTeacherList { get; set; }
     }
}
