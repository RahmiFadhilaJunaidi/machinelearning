using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class VwTrxExamBase : BaseModel
     {
         public VwTrxExam VwTrxExam { get; set; }
         public List<VwTrxExam> VwTrxExamList { get; set; }
     }
}
