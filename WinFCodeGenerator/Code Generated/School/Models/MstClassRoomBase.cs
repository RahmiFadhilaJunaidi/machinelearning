using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class MstClassRoomBase : BaseModel
     {
         public MstClassRoom MstClassRoom { get; set; }
         public List<MstClassRoom> MstClassRoomList { get; set; }
     }
}
