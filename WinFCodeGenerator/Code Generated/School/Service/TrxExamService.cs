using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class TrxExamService
 {
     BaseModel baseModel = new BaseModel();

     public VwTrxExamBase VwTrxExamGetList()
     {
         var listBase = new VwTrxExamBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.VwTrxExamList = context.VwTrxExam.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string TrxExamDelete(TrxExam Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxExam.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string TrxExamCreate(TrxExam Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxExam.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string TrxExamUpdate(TrxExam Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxExam.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
