using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class TrxClassRoomService
 {
     BaseModel baseModel = new BaseModel();

     public VwTrxClassRoomBase VwTrxClassRoomGetList()
     {
         var listBase = new VwTrxClassRoomBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.VwTrxClassRoomList = context.VwTrxClassRoom.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string TrxClassRoomDelete(TrxClassRoom Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxClassRoom.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string TrxClassRoomCreate(TrxClassRoom Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxClassRoom.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string TrxClassRoomUpdate(TrxClassRoom Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxClassRoom.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
