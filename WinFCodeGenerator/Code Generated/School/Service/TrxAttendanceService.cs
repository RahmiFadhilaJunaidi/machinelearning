using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class TrxAttendanceService
 {
     BaseModel baseModel = new BaseModel();

     public TrxAttendanceBase TrxAttendanceGetList()
     {
         var listBase = new TrxAttendanceBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.TrxAttendanceList = context.TrxAttendance.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string TrxAttendanceDelete(TrxAttendance Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxAttendance.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string TrxAttendanceCreate(TrxAttendance Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxAttendance.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string TrxAttendanceUpdate(TrxAttendance Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxAttendance.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
