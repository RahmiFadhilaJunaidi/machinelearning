using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class TrxAttendanceStudentService
 {
     BaseModel baseModel = new BaseModel();

     public TrxAttendanceStudentBase TrxAttendanceStudentGetList()
     {
         var listBase = new TrxAttendanceStudentBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.TrxAttendanceStudentList = context.TrxAttendanceStudent.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string TrxAttendanceStudentDelete(TrxAttendanceStudent Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxAttendanceStudent.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string TrxAttendanceStudentCreate(TrxAttendanceStudent Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxAttendanceStudent.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string TrxAttendanceStudentUpdate(TrxAttendanceStudent Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxAttendanceStudent.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
