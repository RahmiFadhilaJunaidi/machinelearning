using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class TrxAttendanceTeacherService
 {
     BaseModel baseModel = new BaseModel();

     public TrxAttendanceTeacherBase TrxAttendanceTeacherGetList()
     {
         var listBase = new TrxAttendanceTeacherBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.TrxAttendanceTeacherList = context.TrxAttendanceTeacher.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string TrxAttendanceTeacherDelete(TrxAttendanceTeacher Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxAttendanceTeacher.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string TrxAttendanceTeacherCreate(TrxAttendanceTeacher Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxAttendanceTeacher.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string TrxAttendanceTeacherUpdate(TrxAttendanceTeacher Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxAttendanceTeacher.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
