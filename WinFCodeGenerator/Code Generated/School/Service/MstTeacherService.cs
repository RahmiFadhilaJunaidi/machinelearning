using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class MstTeacherService
 {
     BaseModel baseModel = new BaseModel();

     public MstTeacherBase MstTeacherGetList()
     {
         var listBase = new MstTeacherBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.MstTeacherList = context.MstTeacher.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstTeacherDelete(MstTeacher Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstTeacher.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstTeacherCreate(MstTeacher Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstTeacher.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstTeacherUpdate(MstTeacher Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstTeacher.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
