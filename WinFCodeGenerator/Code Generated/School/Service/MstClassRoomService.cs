using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class MstClassRoomService
 {
     BaseModel baseModel = new BaseModel();

     public MstClassRoomBase MstClassRoomGetList()
     {
         var listBase = new MstClassRoomBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.MstClassRoomList = context.MstClassRoom.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstClassRoomDelete(MstClassRoom Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstClassRoom.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstClassRoomCreate(MstClassRoom Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstClassRoom.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstClassRoomUpdate(MstClassRoom Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstClassRoom.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
