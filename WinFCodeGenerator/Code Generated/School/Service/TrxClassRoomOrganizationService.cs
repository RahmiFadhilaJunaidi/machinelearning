using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class TrxClassRoomOrganizationService
 {
     BaseModel baseModel = new BaseModel();

     public TrxClassRoomOrganizationBase TrxClassRoomOrganizationGetList()
     {
         var listBase = new TrxClassRoomOrganizationBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.TrxClassRoomOrganizationList = context.TrxClassRoomOrganization.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string TrxClassRoomOrganizationDelete(TrxClassRoomOrganization Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxClassRoomOrganization.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string TrxClassRoomOrganizationCreate(TrxClassRoomOrganization Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxClassRoomOrganization.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string TrxClassRoomOrganizationUpdate(TrxClassRoomOrganization Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxClassRoomOrganization.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
