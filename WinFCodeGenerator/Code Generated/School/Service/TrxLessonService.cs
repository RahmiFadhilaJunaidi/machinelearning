using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class TrxLessonService
 {
     BaseModel baseModel = new BaseModel();

     public VwTrxLessonBase VwTrxLessonGetList()
     {
         var listBase = new VwTrxLessonBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.VwTrxLessonList = context.VwTrxLesson.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string TrxLessonDelete(TrxLesson Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxLesson.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string TrxLessonCreate(TrxLesson Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxLesson.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string TrxLessonUpdate(TrxLesson Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.TrxLesson.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
