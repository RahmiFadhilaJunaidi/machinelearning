using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class TrxExamBase : BaseModel
     {
         public TrxExam TrxExam { get; set; }
         public List<TrxExam> TrxExamList { get; set; }
     }
}
