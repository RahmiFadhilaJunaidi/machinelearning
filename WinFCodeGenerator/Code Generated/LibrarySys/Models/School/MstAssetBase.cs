using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class MstAssetBase : BaseModel
     {
         public MstAsset MstAsset { get; set; }
         public List<MstAsset> MstAssetList { get; set; }
     }
}
