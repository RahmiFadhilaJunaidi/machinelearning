using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class VwTrxLessonBase : BaseModel
     {
         public VwTrxLesson VwTrxLesson { get; set; }
         public List<VwTrxLesson> VwTrxLessonList { get; set; }
     }
}
