using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class TrxAttendanceStudentBase : BaseModel
     {
         public TrxAttendanceStudent TrxAttendanceStudent { get; set; }
         public List<TrxAttendanceStudent> TrxAttendanceStudentList { get; set; }
     }
}
