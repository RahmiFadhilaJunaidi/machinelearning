using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class MstPositionBase : BaseModel
     {
         public MstPosition MstPosition { get; set; }
         public List<MstPosition> MstPositionList { get; set; }
     }
}
