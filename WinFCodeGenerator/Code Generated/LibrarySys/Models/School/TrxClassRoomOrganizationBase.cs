using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class TrxClassRoomOrganizationBase : BaseModel
     {
         public TrxClassRoomOrganization TrxClassRoomOrganization { get; set; }
         public List<TrxClassRoomOrganization> TrxClassRoomOrganizationList { get; set; }
     }
}
