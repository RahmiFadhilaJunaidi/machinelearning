using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class MstStudentBase : BaseModel
     {
         public MstStudent MstStudent { get; set; }
         public List<MstStudent> MstStudentList { get; set; }
     }
}
