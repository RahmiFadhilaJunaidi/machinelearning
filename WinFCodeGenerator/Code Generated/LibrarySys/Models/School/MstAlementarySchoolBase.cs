using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class MstAlementarySchoolBase : BaseModel
     {
         public MstAlementarySchool MstAlementarySchool { get; set; }
         public List<MstAlementarySchool> MstAlementarySchoolList { get; set; }
     }
}
