using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.School
{
     public partial class TrxLessonBase : BaseModel
     {
         public TrxLesson TrxLesson { get; set; }
         public List<TrxLesson> TrxLessonList { get; set; }
     }
}
