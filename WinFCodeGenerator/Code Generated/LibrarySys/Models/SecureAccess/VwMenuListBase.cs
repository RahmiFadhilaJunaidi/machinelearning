using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class VwMenuListBase : BaseModel
     {
         public VwMenuList VwMenuList { get; set; }
         public List<VwMenuList> VwMenuListList { get; set; }
     }
}
