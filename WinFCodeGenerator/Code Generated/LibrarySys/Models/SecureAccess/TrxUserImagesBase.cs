using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class TrxUserImagesBase : BaseModel
     {
         public TrxUserImages TrxUserImages { get; set; }
         public List<TrxUserImages> TrxUserImagesList { get; set; }
     }
}
