using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class VwTrxUserImagesBase : BaseModel
     {
         public VwTrxUserImages VwTrxUserImages { get; set; }
         public List<VwTrxUserImages> VwTrxUserImagesList { get; set; }
     }
}
