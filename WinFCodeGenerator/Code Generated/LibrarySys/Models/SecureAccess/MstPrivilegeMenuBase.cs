using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class MstPrivilegeMenuBase : BaseModel
     {
         public MstPrivilegeMenu MstPrivilegeMenu { get; set; }
         public List<MstPrivilegeMenu> MstPrivilegeMenuList { get; set; }
     }
}
