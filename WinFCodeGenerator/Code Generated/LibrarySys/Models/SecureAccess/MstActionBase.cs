using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class MstActionBase : BaseModel
     {
         public MstAction MstAction { get; set; }
         public List<MstAction> MstActionList { get; set; }
     }
}
