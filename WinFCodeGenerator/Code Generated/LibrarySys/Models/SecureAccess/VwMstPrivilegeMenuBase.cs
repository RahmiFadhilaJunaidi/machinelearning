using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class VwMstPrivilegeMenuBase : BaseModel
     {
         public VwMstPrivilegeMenu VwMstPrivilegeMenu { get; set; }
         public List<VwMstPrivilegeMenu> VwMstPrivilegeMenuList { get; set; }
     }
}
