using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class VwMstMenuBase : BaseModel
     {
         public VwMstMenu VwMstMenu { get; set; }
         public List<VwMstMenu> VwMstMenuList { get; set; }
     }
}
