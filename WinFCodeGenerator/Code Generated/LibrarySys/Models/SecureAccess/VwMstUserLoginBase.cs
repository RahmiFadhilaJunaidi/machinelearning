using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class VwMstUserLoginBase : BaseModel
     {
         public VwMstUserLogin VwMstUserLogin { get; set; }
         public List<VwMstUserLogin> VwMstUserLoginList { get; set; }
     }
}
