using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class VwMenuBase : BaseModel
     {
         public VwMenu VwMenu { get; set; }
         public List<VwMenu> VwMenuList { get; set; }
     }
}
