using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class SecureAccessContextBase : BaseModel
     {
         public SecureAccessContext SecureAccessContext { get; set; }
         public List<SecureAccessContext> SecureAccessContextList { get; set; }
     }
}
