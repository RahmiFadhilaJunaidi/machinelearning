using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class UserImagesBase : BaseModel
     {
         public UserImages UserImages { get; set; }
         public List<UserImages> UserImagesList { get; set; }
     }
}
