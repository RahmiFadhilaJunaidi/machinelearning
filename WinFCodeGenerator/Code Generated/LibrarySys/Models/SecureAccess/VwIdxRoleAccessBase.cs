using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class VwIdxRoleAccessBase : BaseModel
     {
         public VwIdxRoleAccess VwIdxRoleAccess { get; set; }
         public List<VwIdxRoleAccess> VwIdxRoleAccessList { get; set; }
     }
}
