using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class VwPrivilegeMenuBase : BaseModel
     {
         public VwPrivilegeMenu VwPrivilegeMenu { get; set; }
         public List<VwPrivilegeMenu> VwPrivilegeMenuList { get; set; }
     }
}
