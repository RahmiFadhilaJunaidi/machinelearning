using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class MstUserLoginBase : BaseModel
     {
         public MstUserLogin MstUserLogin { get; set; }
         public List<MstUserLogin> MstUserLoginList { get; set; }
     }
}
