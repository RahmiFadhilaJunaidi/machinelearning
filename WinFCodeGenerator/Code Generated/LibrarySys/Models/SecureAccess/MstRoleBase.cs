using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class MstRoleBase : BaseModel
     {
         public MstRole MstRole { get; set; }
         public List<MstRole> MstRoleList { get; set; }
     }
}
