using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class VwMstRoleBase : BaseModel
     {
         public VwMstRole VwMstRole { get; set; }
         public List<VwMstRole> VwMstRoleList { get; set; }
     }
}
