using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class IdxRoleAccessBase : BaseModel
     {
         public IdxRoleAccess IdxRoleAccess { get; set; }
         public List<IdxRoleAccess> IdxRoleAccessList { get; set; }
     }
}
