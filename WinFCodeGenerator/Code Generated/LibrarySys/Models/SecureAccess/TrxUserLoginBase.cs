using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class TrxUserLoginBase : BaseModel
     {
         public TrxUserLogin TrxUserLogin { get; set; }
         public List<TrxUserLogin> TrxUserLoginList { get; set; }
     }
}
