using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.SecureAccess
{
     public partial class VwMstActionBase : BaseModel
     {
         public VwMstAction VwMstAction { get; set; }
         public List<VwMstAction> VwMstActionList { get; set; }
     }
}
