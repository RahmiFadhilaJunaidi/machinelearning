using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.Workflow
{
     public partial class WorkFlowHeaderBase : BaseModel
     {
         public WorkFlowHeader WorkFlowHeader { get; set; }
         public List<WorkFlowHeader> WorkFlowHeaderList { get; set; }
     }
}
