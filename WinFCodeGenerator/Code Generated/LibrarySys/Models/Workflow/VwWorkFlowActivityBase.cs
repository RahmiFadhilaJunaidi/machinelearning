using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.Workflow
{
     public partial class VwWorkFlowActivityBase : BaseModel
     {
         public VwWorkFlowActivity VwWorkFlowActivity { get; set; }
         public List<VwWorkFlowActivity> VwWorkFlowActivityList { get; set; }
     }
}
