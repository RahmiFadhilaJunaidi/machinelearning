using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.Workflow
{
     public partial class WorkFlowActivityBase : BaseModel
     {
         public WorkFlowActivity WorkFlowActivity { get; set; }
         public List<WorkFlowActivity> WorkFlowActivityList { get; set; }
     }
}
