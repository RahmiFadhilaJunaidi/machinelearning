using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.Workflow
{
     public partial class VwWorkFlowNextActivityBase : BaseModel
     {
         public VwWorkFlowNextActivity VwWorkFlowNextActivity { get; set; }
         public List<VwWorkFlowNextActivity> VwWorkFlowNextActivityList { get; set; }
     }
}
