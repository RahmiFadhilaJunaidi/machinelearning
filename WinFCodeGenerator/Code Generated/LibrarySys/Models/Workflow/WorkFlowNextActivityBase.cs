using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.Workflow
{
     public partial class WorkFlowNextActivityBase : BaseModel
     {
         public WorkFlowNextActivity WorkFlowNextActivity { get; set; }
         public List<WorkFlowNextActivity> WorkFlowNextActivityList { get; set; }
     }
}
