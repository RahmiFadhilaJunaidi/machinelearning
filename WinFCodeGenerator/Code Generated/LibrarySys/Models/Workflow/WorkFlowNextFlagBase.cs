using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.Workflow
{
     public partial class WorkFlowNextFlagBase : BaseModel
     {
         public WorkFlowNextFlag WorkFlowNextFlag { get; set; }
         public List<WorkFlowNextFlag> WorkFlowNextFlagList { get; set; }
     }
}
