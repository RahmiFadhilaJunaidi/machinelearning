using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.Workflow
{
     public partial class VwWorkFlowNextFlagBase : BaseModel
     {
         public VwWorkFlowNextFlag VwWorkFlowNextFlag { get; set; }
         public List<VwWorkFlowNextFlag> VwWorkFlowNextFlagList { get; set; }
     }
}
