using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.Workflow
{
     public partial class WorkFlowJobBase : BaseModel
     {
         public WorkFlowJob WorkFlowJob { get; set; }
         public List<WorkFlowJob> WorkFlowJobList { get; set; }
     }
}
