using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.Workflow
{
     public partial class VwMstActivityBase : BaseModel
     {
         public VwMstActivity VwMstActivity { get; set; }
         public List<VwMstActivity> VwMstActivityList { get; set; }
     }
}
