using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.Workflow
{
     public partial class WorkFlowLogBase : BaseModel
     {
         public WorkFlowLog WorkFlowLog { get; set; }
         public List<WorkFlowLog> WorkFlowLogList { get; set; }
     }
}
