using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.School;
using LibrarySys.DA.Models.School;

namespace LibrarySys.Controllers
{
 public class MstApplicantsController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/School/MstApplicants.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstApplicantsBase();
         try
         {
             var service = new MstApplicantsService();
             result = service.MstApplicantsGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstApplicantsList });
     }


     [HttpPost]
     public IActionResult Create(MstApplicants Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstApplicantsService();
             respon.errorMessage = service.MstApplicantsCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstApplicants Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstApplicantsService();
             respon.errorMessage = service.MstApplicantsUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstApplicants Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstApplicantsService();
             respon.errorMessage = service.MstApplicantsDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
