using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.SecureAccess;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.Controllers
{
 public class TrxUserLoginController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/SecureAccess/TrxUserLogin.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new TrxUserLoginBase();
         try
         {
             var service = new TrxUserLoginService();
             result = service.TrxUserLoginGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.TrxUserLoginList });
     }


     [HttpPost]
     public IActionResult Create(TrxUserLogin Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxUserLoginService();
             respon.errorMessage = service.TrxUserLoginCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(TrxUserLogin Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxUserLoginService();
             respon.errorMessage = service.TrxUserLoginUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(TrxUserLogin Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxUserLoginService();
             respon.errorMessage = service.TrxUserLoginDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
