using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.SecureAccess;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.Controllers
{
 public class IdxRoleAccessController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/SecureAccess/IdxRoleAccess.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwIdxRoleAccessBase();
         try
         {
             var service = new IdxRoleAccessService();
             result = service.VwIdxRoleAccessGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwIdxRoleAccessList });
     }


     [HttpPost]
     public IActionResult Create(IdxRoleAccess Post)
     {
         var respon = new Respon();
         try
         {
             var service = new IdxRoleAccessService();
             respon.errorMessage = service.IdxRoleAccessCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(IdxRoleAccess Post)
     {
         var respon = new Respon();
         try
         {
             var service = new IdxRoleAccessService();
             respon.errorMessage = service.IdxRoleAccessUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(IdxRoleAccess Post)
     {
         var respon = new Respon();
         try
         {
             var service = new IdxRoleAccessService();
             respon.errorMessage = service.IdxRoleAccessDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
