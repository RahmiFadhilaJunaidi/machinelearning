using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.SecureAccess;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.Controllers
{
 public class MstRoleController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/SecureAccess/MstRole.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstRoleBase();
         try
         {
             var service = new MstRoleService();
             result = service.MstRoleGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstRoleList });
     }


     [HttpPost]
     public IActionResult Create(MstRole Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstRoleService();
             respon.errorMessage = service.MstRoleCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(MstRole Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstRoleService();
             respon.errorMessage = service.MstRoleUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstRole Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstRoleService();
             respon.errorMessage = service.MstRoleDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
