using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.SecureAccess;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.Controllers
{
 public class MstUserLoginController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/SecureAccess/MstUserLogin.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwMstUserLoginBase();
         try
         {
             var service = new MstUserLoginService();
             result = service.VwMstUserLoginGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwMstUserLoginList });
     }


     [HttpPost]
     public IActionResult Create(MstUserLogin Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstUserLoginService();
             respon.errorMessage = service.MstUserLoginCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(MstUserLogin Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstUserLoginService();
             respon.errorMessage = service.MstUserLoginUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstUserLogin Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstUserLoginService();
             respon.errorMessage = service.MstUserLoginDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
