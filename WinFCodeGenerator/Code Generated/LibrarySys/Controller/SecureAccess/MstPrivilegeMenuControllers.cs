using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.SecureAccess;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.Controllers
{
 public class MstPrivilegeMenuController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/SecureAccess/MstPrivilegeMenu.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwMstPrivilegeMenuBase();
         try
         {
             var service = new MstPrivilegeMenuService();
             result = service.VwMstPrivilegeMenuGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwMstPrivilegeMenuList });
     }


     [HttpPost]
     public IActionResult Create(MstPrivilegeMenu Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstPrivilegeMenuService();
             respon.errorMessage = service.MstPrivilegeMenuCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(MstPrivilegeMenu Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstPrivilegeMenuService();
             respon.errorMessage = service.MstPrivilegeMenuUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstPrivilegeMenu Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstPrivilegeMenuService();
             respon.errorMessage = service.MstPrivilegeMenuDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
