using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.SecureAccess;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.Controllers
{
 public class TrxUserImagesController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/SecureAccess/TrxUserImages.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwTrxUserImagesBase();
         try
         {
             var service = new TrxUserImagesService();
             result = service.VwTrxUserImagesGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwTrxUserImagesList });
     }


     [HttpPost]
     public IActionResult Create(TrxUserImages Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxUserImagesService();
             respon.errorMessage = service.TrxUserImagesCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(TrxUserImages Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxUserImagesService();
             respon.errorMessage = service.TrxUserImagesUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(TrxUserImages Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxUserImagesService();
             respon.errorMessage = service.TrxUserImagesDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
