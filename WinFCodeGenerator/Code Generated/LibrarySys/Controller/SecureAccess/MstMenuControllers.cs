using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.SecureAccess;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.Controllers
{
 public class MstMenuController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/SecureAccess/MstMenu.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwMstMenuBase();
         try
         {
             var service = new MstMenuService();
             result = service.VwMstMenuGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwMstMenuList });
     }


     [HttpPost]
     public IActionResult Create(MstMenu Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstMenuService();
             respon.errorMessage = service.MstMenuCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(MstMenu Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstMenuService();
             respon.errorMessage = service.MstMenuUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstMenu Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstMenuService();
             respon.errorMessage = service.MstMenuDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
