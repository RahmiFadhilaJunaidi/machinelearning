using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.Workflow;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.Controllers
{
 public class WorkFlowNextActivityController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/Workflow/WorkFlowNextActivity.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwWorkFlowNextActivityBase();
         try
         {
             var service = new WorkFlowNextActivityService();
             result = service.VwWorkFlowNextActivityGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwWorkFlowNextActivityList });
     }


     [HttpPost]
     public IActionResult Create(WorkFlowNextActivity Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowNextActivityService();
             respon.errorMessage = service.WorkFlowNextActivityCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(WorkFlowNextActivity Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowNextActivityService();
             respon.errorMessage = service.WorkFlowNextActivityUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(WorkFlowNextActivity Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowNextActivityService();
             respon.errorMessage = service.WorkFlowNextActivityDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
