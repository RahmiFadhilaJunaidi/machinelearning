using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.Workflow;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.Controllers
{
 public class WorkFlowJobController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/Workflow/WorkFlowJob.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new WorkFlowJobBase();
         try
         {
             var service = new WorkFlowJobService();
             result = service.WorkFlowJobGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.WorkFlowJobList });
     }


     [HttpPost]
     public IActionResult Create(WorkFlowJob Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowJobService();
             respon.errorMessage = service.WorkFlowJobCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(WorkFlowJob Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowJobService();
             respon.errorMessage = service.WorkFlowJobUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(WorkFlowJob Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowJobService();
             respon.errorMessage = service.WorkFlowJobDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
