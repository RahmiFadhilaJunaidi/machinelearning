using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.Workflow;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.Controllers
{
 public class WorkFlowLogController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/Workflow/WorkFlowLog.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new WorkFlowLogBase();
         try
         {
             var service = new WorkFlowLogService();
             result = service.WorkFlowLogGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.WorkFlowLogList });
     }


     [HttpPost]
     public IActionResult Create(WorkFlowLog Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowLogService();
             respon.errorMessage = service.WorkFlowLogCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(WorkFlowLog Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowLogService();
             respon.errorMessage = service.WorkFlowLogUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(WorkFlowLog Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowLogService();
             respon.errorMessage = service.WorkFlowLogDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
