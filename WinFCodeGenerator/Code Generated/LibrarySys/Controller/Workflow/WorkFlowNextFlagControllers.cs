using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.Workflow;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.Controllers
{
 public class WorkFlowNextFlagController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/Workflow/WorkFlowNextFlag.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwWorkFlowNextFlagBase();
         try
         {
             var service = new WorkFlowNextFlagService();
             result = service.VwWorkFlowNextFlagGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwWorkFlowNextFlagList });
     }


     [HttpPost]
     public IActionResult Create(WorkFlowNextFlag Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowNextFlagService();
             respon.errorMessage = service.WorkFlowNextFlagCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(WorkFlowNextFlag Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowNextFlagService();
             respon.errorMessage = service.WorkFlowNextFlagUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(WorkFlowNextFlag Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowNextFlagService();
             respon.errorMessage = service.WorkFlowNextFlagDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
