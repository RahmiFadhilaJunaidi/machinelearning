using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.Workflow;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.Controllers
{
 public class WorkFlowActivityController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/Workflow/WorkFlowActivity.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwWorkFlowActivityBase();
         try
         {
             var service = new WorkFlowActivityService();
             result = service.VwWorkFlowActivityGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwWorkFlowActivityList });
     }


     [HttpPost]
     public IActionResult Create(WorkFlowActivity Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowActivityService();
             respon.errorMessage = service.WorkFlowActivityCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(WorkFlowActivity Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowActivityService();
             respon.errorMessage = service.WorkFlowActivityUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(WorkFlowActivity Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowActivityService();
             respon.errorMessage = service.WorkFlowActivityDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
