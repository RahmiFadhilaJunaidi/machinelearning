using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.Workflow;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.Controllers
{
 public class WorkFlowHeaderController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/Workflow/WorkFlowHeader.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new WorkFlowHeaderBase();
         try
         {
             var service = new WorkFlowHeaderService();
             result = service.WorkFlowHeaderGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.WorkFlowHeaderList });
     }


     [HttpPost]
     public IActionResult Create(WorkFlowHeader Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowHeaderService();
             respon.errorMessage = service.WorkFlowHeaderCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Update(WorkFlowHeader Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowHeaderService();
             respon.errorMessage = service.WorkFlowHeaderUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController + respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(WorkFlowHeader Post)
     {
         var respon = new Respon();
         try
         {
             var service = new WorkFlowHeaderService();
             respon.errorMessage = service.WorkFlowHeaderDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
