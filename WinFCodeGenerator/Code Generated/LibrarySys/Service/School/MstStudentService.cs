using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class MstStudentService
 {
     BaseModel baseModel = new BaseModel();

     public MstStudentBase MstStudentGetList()
     {
         var listBase = new MstStudentBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.MstStudentList = context.MstStudent.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstStudentDelete(MstStudent Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstStudent.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstStudentCreate(MstStudent Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstStudent.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstStudentUpdate(MstStudent Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstStudent.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
