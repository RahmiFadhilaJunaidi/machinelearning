using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class MstAlementarySchoolService
 {
     BaseModel baseModel = new BaseModel();

     public MstAlementarySchoolBase MstAlementarySchoolGetList()
     {
         var listBase = new MstAlementarySchoolBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.MstAlementarySchoolList = context.MstAlementarySchool.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstAlementarySchoolDelete(MstAlementarySchool Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstAlementarySchool.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstAlementarySchoolCreate(MstAlementarySchool Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstAlementarySchool.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstAlementarySchoolUpdate(MstAlementarySchool Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstAlementarySchool.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
