using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class MstApplicantsService
 {
     BaseModel baseModel = new BaseModel();

     public MstApplicantsBase MstApplicantsGetList()
     {
         var listBase = new MstApplicantsBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.MstApplicantsList = context.MstApplicants.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstApplicantsDelete(MstApplicants Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstApplicants.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstApplicantsCreate(MstApplicants Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstApplicants.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstApplicantsUpdate(MstApplicants Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstApplicants.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
