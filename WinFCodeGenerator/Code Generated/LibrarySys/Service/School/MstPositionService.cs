using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class MstPositionService
 {
     BaseModel baseModel = new BaseModel();

     public MstPositionBase MstPositionGetList()
     {
         var listBase = new MstPositionBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.MstPositionList = context.MstPosition.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstPositionDelete(MstPosition Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstPosition.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstPositionCreate(MstPosition Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstPosition.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstPositionUpdate(MstPosition Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstPosition.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
