using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class MstAssetService
 {
     BaseModel baseModel = new BaseModel();

     public MstAssetBase MstAssetGetList()
     {
         var listBase = new MstAssetBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.MstAssetList = context.MstAsset.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstAssetDelete(MstAsset Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstAsset.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstAssetCreate(MstAsset Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstAsset.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstAssetUpdate(MstAsset Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstAsset.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
