using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.School;

namespace LibrarySys.DA.Service.School
 {
 public class MstLessonService
 {
     BaseModel baseModel = new BaseModel();

     public MstLessonBase MstLessonGetList()
     {
         var listBase = new MstLessonBase();
         try
         {
             using (var context = new SchoolContext())
             {
                 listBase.MstLessonList = context.MstLesson.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstLessonDelete(MstLesson Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstLesson.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstLessonCreate(MstLesson Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstLesson.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstLessonUpdate(MstLesson Post)
     {
         try
         {
             using (var context = new SchoolContext())
             {
                 context.MstLesson.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
