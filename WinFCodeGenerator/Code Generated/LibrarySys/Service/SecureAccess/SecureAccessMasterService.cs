using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.DA.Service.SecureAccess
{
 public class SecureAccessMasterService
 {
     BaseModel baseModel = new BaseModel();

     public  VwMstActionBase VwMstActionGetList()
     {
         var listBase = new VwMstActionBase();
         try
         {
             using (var context = new SecureAccessContext())
             {
                 listBase.VwMstActionList = context.VwMstAction.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
         return listBase;
         }
     }

 }
}
