using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.DA.Service.SecureAccess
 {
 public class MstUserLoginService
 {
     BaseModel baseModel = new BaseModel();

     public VwMstUserLoginBase VwMstUserLoginGetList()
     {
         var listBase = new VwMstUserLoginBase();
         try
         {
             using (var context = new SecureAccessContext())
             {
                 listBase.VwMstUserLoginList = context.VwMstUserLogin.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstUserLoginDelete(MstUserLogin Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstUserLogin.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstUserLoginCreate(MstUserLogin Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstUserLogin.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstUserLoginUpdate(MstUserLogin Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstUserLogin.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
