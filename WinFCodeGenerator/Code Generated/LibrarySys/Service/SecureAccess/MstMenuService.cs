using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.DA.Service.SecureAccess
 {
 public class MstMenuService
 {
     BaseModel baseModel = new BaseModel();

     public VwMstMenuBase VwMstMenuGetList()
     {
         var listBase = new VwMstMenuBase();
         try
         {
             using (var context = new SecureAccessContext())
             {
                 listBase.VwMstMenuList = context.VwMstMenu.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstMenuDelete(MstMenu Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstMenu.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstMenuCreate(MstMenu Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstMenu.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstMenuUpdate(MstMenu Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstMenu.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
