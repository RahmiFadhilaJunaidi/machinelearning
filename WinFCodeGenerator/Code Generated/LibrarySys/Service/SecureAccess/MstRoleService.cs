using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.DA.Service.SecureAccess
 {
 public class MstRoleService
 {
     BaseModel baseModel = new BaseModel();

     public MstRoleBase MstRoleGetList()
     {
         var listBase = new MstRoleBase();
         try
         {
             using (var context = new SecureAccessContext())
             {
                 listBase.MstRoleList = context.MstRole.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstRoleDelete(MstRole Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstRole.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstRoleCreate(MstRole Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstRole.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstRoleUpdate(MstRole Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstRole.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
