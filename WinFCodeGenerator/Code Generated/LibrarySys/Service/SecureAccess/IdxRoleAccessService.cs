using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.DA.Service.SecureAccess
 {
 public class IdxRoleAccessService
 {
     BaseModel baseModel = new BaseModel();

     public VwIdxRoleAccessBase VwIdxRoleAccessGetList()
     {
         var listBase = new VwIdxRoleAccessBase();
         try
         {
             using (var context = new SecureAccessContext())
             {
                 listBase.VwIdxRoleAccessList = context.VwIdxRoleAccess.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string IdxRoleAccessDelete(IdxRoleAccess Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.IdxRoleAccess.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string IdxRoleAccessCreate(IdxRoleAccess Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.IdxRoleAccess.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string IdxRoleAccessUpdate(IdxRoleAccess Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.IdxRoleAccess.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
