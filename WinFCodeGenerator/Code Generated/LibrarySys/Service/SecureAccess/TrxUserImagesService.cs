using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.DA.Service.SecureAccess
 {
 public class TrxUserImagesService
 {
     BaseModel baseModel = new BaseModel();

     public VwTrxUserImagesBase VwTrxUserImagesGetList()
     {
         var listBase = new VwTrxUserImagesBase();
         try
         {
             using (var context = new SecureAccessContext())
             {
                 listBase.VwTrxUserImagesList = context.VwTrxUserImages.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string TrxUserImagesDelete(TrxUserImages Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.TrxUserImages.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string TrxUserImagesCreate(TrxUserImages Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.TrxUserImages.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string TrxUserImagesUpdate(TrxUserImages Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.TrxUserImages.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
