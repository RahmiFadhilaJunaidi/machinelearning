using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.DA.Service.SecureAccess
 {
 public class MstActionService
 {
     BaseModel baseModel = new BaseModel();

     public VwMstActionBase VwMstActionGetList()
     {
         var listBase = new VwMstActionBase();
         try
         {
             using (var context = new SecureAccessContext())
             {
                 listBase.VwMstActionList = context.VwMstAction.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstActionDelete(MstAction Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstAction.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstActionCreate(MstAction Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstAction.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstActionUpdate(MstAction Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstAction.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
