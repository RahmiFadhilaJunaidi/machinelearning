using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.DA.Service.SecureAccess
 {
 public class UserImagesService
 {
     BaseModel baseModel = new BaseModel();

     public UserImagesBase UserImagesGetList()
     {
         var listBase = new UserImagesBase();
         try
         {
             using (var context = new SecureAccessContext())
             {
                 listBase.UserImagesList = context.UserImages.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string UserImagesDelete(UserImages Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.UserImages.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string UserImagesCreate(UserImages Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.UserImages.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string UserImagesUpdate(UserImages Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.UserImages.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
