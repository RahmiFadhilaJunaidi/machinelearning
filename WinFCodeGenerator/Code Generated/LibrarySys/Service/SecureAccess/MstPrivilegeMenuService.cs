using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.DA.Service.SecureAccess
 {
 public class MstPrivilegeMenuService
 {
     BaseModel baseModel = new BaseModel();

     public VwMstPrivilegeMenuBase VwMstPrivilegeMenuGetList()
     {
         var listBase = new VwMstPrivilegeMenuBase();
         try
         {
             using (var context = new SecureAccessContext())
             {
                 listBase.VwMstPrivilegeMenuList = context.VwMstPrivilegeMenu.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstPrivilegeMenuDelete(MstPrivilegeMenu Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstPrivilegeMenu.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstPrivilegeMenuCreate(MstPrivilegeMenu Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstPrivilegeMenu.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstPrivilegeMenuUpdate(MstPrivilegeMenu Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.MstPrivilegeMenu.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
