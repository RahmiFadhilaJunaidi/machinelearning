using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.SecureAccess;

namespace LibrarySys.DA.Service.SecureAccess
 {
 public class TrxUserLoginService
 {
     BaseModel baseModel = new BaseModel();

     public TrxUserLoginBase TrxUserLoginGetList()
     {
         var listBase = new TrxUserLoginBase();
         try
         {
             using (var context = new SecureAccessContext())
             {
                 listBase.TrxUserLoginList = context.TrxUserLogin.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string TrxUserLoginDelete(TrxUserLogin Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.TrxUserLogin.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string TrxUserLoginCreate(TrxUserLogin Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.TrxUserLogin.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string TrxUserLoginUpdate(TrxUserLogin Post)
     {
         try
         {
             using (var context = new SecureAccessContext())
             {
                 context.TrxUserLogin.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
