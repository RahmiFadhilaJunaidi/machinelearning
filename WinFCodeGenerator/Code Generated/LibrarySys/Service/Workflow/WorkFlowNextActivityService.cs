using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.DA.Service.Workflow
 {
 public class WorkFlowNextActivityService
 {
     BaseModel baseModel = new BaseModel();

     public VwWorkFlowNextActivityBase VwWorkFlowNextActivityGetList()
     {
         var listBase = new VwWorkFlowNextActivityBase();
         try
         {
             using (var context = new WorkflowContext())
             {
                 listBase.VwWorkFlowNextActivityList = context.VwWorkFlowNextActivity.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string WorkFlowNextActivityDelete(WorkFlowNextActivity Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowNextActivity.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string WorkFlowNextActivityCreate(WorkFlowNextActivity Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowNextActivity.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string WorkFlowNextActivityUpdate(WorkFlowNextActivity Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowNextActivity.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
