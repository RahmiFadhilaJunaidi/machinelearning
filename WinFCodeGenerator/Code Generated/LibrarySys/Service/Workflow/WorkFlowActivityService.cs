using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.DA.Service.Workflow
 {
 public class WorkFlowActivityService
 {
     BaseModel baseModel = new BaseModel();

     public VwWorkFlowActivityBase VwWorkFlowActivityGetList()
     {
         var listBase = new VwWorkFlowActivityBase();
         try
         {
             using (var context = new WorkflowContext())
             {
                 listBase.VwWorkFlowActivityList = context.VwWorkFlowActivity.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string WorkFlowActivityDelete(WorkFlowActivity Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowActivity.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string WorkFlowActivityCreate(WorkFlowActivity Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowActivity.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string WorkFlowActivityUpdate(WorkFlowActivity Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowActivity.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
