using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.DA.Service.Workflow
 {
 public class WorkFlowHeaderService
 {
     BaseModel baseModel = new BaseModel();

     public WorkFlowHeaderBase WorkFlowHeaderGetList()
     {
         var listBase = new WorkFlowHeaderBase();
         try
         {
             using (var context = new WorkflowContext())
             {
                 listBase.WorkFlowHeaderList = context.WorkFlowHeader.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string WorkFlowHeaderDelete(WorkFlowHeader Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowHeader.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string WorkFlowHeaderCreate(WorkFlowHeader Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowHeader.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string WorkFlowHeaderUpdate(WorkFlowHeader Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowHeader.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
