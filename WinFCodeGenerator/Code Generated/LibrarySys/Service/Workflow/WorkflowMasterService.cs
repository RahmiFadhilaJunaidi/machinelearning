using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.DA.Service.Workflow
{
 public class WorkflowMasterService
 {
     BaseModel baseModel = new BaseModel();

     public  VwWorkFlowActivityBase VwWorkFlowActivityGetList()
     {
         var listBase = new VwWorkFlowActivityBase();
         try
         {
             using (var context = new WorkflowContext())
             {
                 listBase.VwWorkFlowActivityList = context.VwWorkFlowActivity.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
         return listBase;
         }
     }

     public  VwWorkFlowNextActivityBase VwWorkFlowNextActivityGetList()
     {
         var listBase = new VwWorkFlowNextActivityBase();
         try
         {
             using (var context = new WorkflowContext())
             {
                 listBase.VwWorkFlowNextActivityList = context.VwWorkFlowNextActivity.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
         return listBase;
         }
     }

     public  VwWorkFlowNextFlagBase VwWorkFlowNextFlagGetList()
     {
         var listBase = new VwWorkFlowNextFlagBase();
         try
         {
             using (var context = new WorkflowContext())
             {
                 listBase.VwWorkFlowNextFlagList = context.VwWorkFlowNextFlag.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
         return listBase;
         }
     }

 }
}
