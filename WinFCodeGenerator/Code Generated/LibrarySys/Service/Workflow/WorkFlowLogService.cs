using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.DA.Service.Workflow
 {
 public class WorkFlowLogService
 {
     BaseModel baseModel = new BaseModel();

     public WorkFlowLogBase WorkFlowLogGetList()
     {
         var listBase = new WorkFlowLogBase();
         try
         {
             using (var context = new WorkflowContext())
             {
                 listBase.WorkFlowLogList = context.WorkFlowLog.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string WorkFlowLogDelete(WorkFlowLog Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowLog.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string WorkFlowLogCreate(WorkFlowLog Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowLog.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string WorkFlowLogUpdate(WorkFlowLog Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowLog.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
