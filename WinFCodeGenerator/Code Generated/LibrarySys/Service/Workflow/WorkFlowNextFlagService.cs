using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.DA.Service.Workflow
 {
 public class WorkFlowNextFlagService
 {
     BaseModel baseModel = new BaseModel();

     public VwWorkFlowNextFlagBase VwWorkFlowNextFlagGetList()
     {
         var listBase = new VwWorkFlowNextFlagBase();
         try
         {
             using (var context = new WorkflowContext())
             {
                 listBase.VwWorkFlowNextFlagList = context.VwWorkFlowNextFlag.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string WorkFlowNextFlagDelete(WorkFlowNextFlag Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowNextFlag.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string WorkFlowNextFlagCreate(WorkFlowNextFlag Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowNextFlag.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string WorkFlowNextFlagUpdate(WorkFlowNextFlag Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowNextFlag.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
