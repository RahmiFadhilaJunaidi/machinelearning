using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.Workflow;

namespace LibrarySys.DA.Service.Workflow
 {
 public class WorkFlowJobService
 {
     BaseModel baseModel = new BaseModel();

     public WorkFlowJobBase WorkFlowJobGetList()
     {
         var listBase = new WorkFlowJobBase();
         try
         {
             using (var context = new WorkflowContext())
             {
                 listBase.WorkFlowJobList = context.WorkFlowJob.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string WorkFlowJobDelete(WorkFlowJob Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowJob.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string WorkFlowJobCreate(WorkFlowJob Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowJob.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string WorkFlowJobUpdate(WorkFlowJob Post)
     {
         try
         {
             using (var context = new WorkflowContext())
             {
                 context.WorkFlowJob.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
