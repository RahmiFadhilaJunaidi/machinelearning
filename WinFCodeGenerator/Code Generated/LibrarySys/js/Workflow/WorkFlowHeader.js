var tblWorkFlowHeader='#tblWorkFlowHeader';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#WorkFlowHeaderForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                WFHeaderID: $('#WFHeaderID').val(),
                JobCode: $('#JobCode').val(),
                RequestNumber: $('#RequestNumber').val(),
                Initiator: $('#Initiator').val(),
                ActivityOwner: $('#ActivityOwner').val(),
                PotentialOwner: $('#PotentialOwner').val(),
                WFStatus: $('#WFStatus').val(),
                WFNextFlag: $('#WFNextFlag').val(),
                Remarks: $('#Remarks').val(),
                CreatedDate: $('#CreatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                };
     var result = Common.Form.Save('/WorkFlowHeader/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/WorkFlowHeader/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#WFHeaderID').val('');
      $('#JobCode').val('');
      $('#RequestNumber').val('');
      $('#Initiator').val('');
      $('#ActivityOwner').val('');
      $('#PotentialOwner').val('');
      $('#WFStatus').val('');
      $('#WFNextFlag').val('');
      $('#Remarks').val('');
      $('#CreatedDate').val('');
      $('#UpdatedBy').val('');
      $('#UpdatedDate').val('');
    },
}

var FormValidation = {
    WFHeaderID: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblWorkFlowHeader);
       },
       LoadData: function () {
          return Common.GetData.Get('/WorkFlowHeader/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'wfHeaderid'},
                    {'data':'jobCode'},
                    {'data':'requestNumber'},
                    {'data':'initiator'},
                    {'data':'activityOwner'},
                    {'data':'potentialOwner'},
                    {'data':'wfStatus'},
                    {'data':'wfNextFlag'},
                    {'data':'remarks'},
                    {'data':'createdDate'},
                    {'data':'updatedBy'},
                    {'data':'updatedDate'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblWorkFlowHeader, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblWorkFlowHeader).unbind();
              $(tblWorkFlowHeader).on('click', '.btnRemove', function (e) {
                  var tb = $(tblWorkFlowHeader).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        WFHeaderID : row.wFHeaderID,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: true
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblWorkFlowHeader).on('click', '.btnEdit', function (e) {
                  var tb = $(tblWorkFlowHeader).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#WFHeaderID').val(row.wFHeaderID);
                  $('#JobCode').val(row.jobCode);
                  $('#RequestNumber').val(row.requestNumber);
                  $('#Initiator').val(row.initiator);
                  $('#ActivityOwner').val(row.activityOwner);
                  $('#PotentialOwner').val(row.potentialOwner);
                  $('#WFStatus').val(row.wFStatus);
                  $('#WFNextFlag').val(row.wFNextFlag);
                  $('#Remarks').val(row.remarks);
                  $('#CreatedDate').val(row.createdDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  $('#UpdatedDate').val(row.updatedDate);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
