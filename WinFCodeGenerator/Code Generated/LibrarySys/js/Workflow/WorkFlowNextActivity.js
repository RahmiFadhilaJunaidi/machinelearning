var tblWorkFlowNextActivity='#tblWorkFlowNextActivity';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#WorkFlowNextActivityForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                NextActivityID: $('#NextActivityID').val(),
                JobID: $('#JobID').val(),
                NextFlag: $('#NextFlag').val(),
                FromActivityID: $('#FromActivityID').val(),
                ToActivityID: $('#ToActivityID').val(),
                CreatedDate: $('#CreatedDate').val(),
                CreatedBy: $('#CreatedBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                };
     var result = Common.Form.Save('/WorkFlowNextActivity/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/WorkFlowNextActivity/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#NextActivityID').val('');
      $('#JobID').val('');
      $('#NextFlag').val('');
      $('#FromActivityID').val('');
      $('#ToActivityID').val('');
      $('#CreatedDate').val('');
      $('#CreatedBy').val('');
      $('#UpdatedDate').val('');
      $('#UpdatedBy').val('');
    },
}

var FormValidation = {
    NextActivityID: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblWorkFlowNextActivity);
       },
       LoadData: function () {
          return Common.GetData.Get('/WorkFlowNextActivity/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'nextActivityid'},
                    {'data':'jobid'},
                    {'data':'nextFlag'},
                    {'data':'fromActivityid'},
                    {'data':'toActivityid'},
                    {'data':'createdDate'},
                    {'data':'createdBy'},
                    {'data':'updatedDate'},
                    {'data':'updatedBy'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblWorkFlowNextActivity, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblWorkFlowNextActivity).unbind();
              $(tblWorkFlowNextActivity).on('click', '.btnRemove', function (e) {
                  var tb = $(tblWorkFlowNextActivity).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        NextActivityID : row.nextActivityID,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: true
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblWorkFlowNextActivity).on('click', '.btnEdit', function (e) {
                  var tb = $(tblWorkFlowNextActivity).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#NextActivityID').val(row.nextActivityID);
                  $('#JobID').val(row.jobID);
                  $('#NextFlag').val(row.nextFlag);
                  $('#FromActivityID').val(row.fromActivityID);
                  $('#ToActivityID').val(row.toActivityID);
                  $('#CreatedDate').val(row.createdDate);
                  $('#CreatedBy').val(row.createdBy);
                  $('#UpdatedDate').val(row.updatedDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
