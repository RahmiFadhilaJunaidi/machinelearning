var tblWorkFlowLog='#tblWorkFlowLog';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#WorkFlowLogForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                WFLogID: $('#WFLogID').val(),
                WFHeaderID: $('#WFHeaderID').val(),
                WFNextFlag: $('#WFNextFlag').val(),
                CreatedBy: $('#CreatedBy').val(),
                CreatedDate: $('#CreatedDate').val(),
                Remarks: $('#Remarks').val(),
                };
     var result = Common.Form.Save('/WorkFlowLog/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/WorkFlowLog/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#WFLogID').val('');
      $('#WFHeaderID').val('');
      $('#WFNextFlag').val('');
      $('#CreatedBy').val('');
      $('#CreatedDate').val('');
      $('#Remarks').val('');
    },
}

var FormValidation = {
    WFLogID: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblWorkFlowLog);
       },
       LoadData: function () {
          return Common.GetData.Get('/WorkFlowLog/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'wfLogid'},
                    {'data':'wfHeaderid'},
                    {'data':'wfNextFlag'},
                    {'data':'createdBy'},
                    {'data':'createdDate'},
                    {'data':'remarks'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblWorkFlowLog, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblWorkFlowLog).unbind();
              $(tblWorkFlowLog).on('click', '.btnRemove', function (e) {
                  var tb = $(tblWorkFlowLog).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        WFLogID : row.wFLogID,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: true
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblWorkFlowLog).on('click', '.btnEdit', function (e) {
                  var tb = $(tblWorkFlowLog).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#WFLogID').val(row.wFLogID);
                  $('#WFHeaderID').val(row.wFHeaderID);
                  $('#WFNextFlag').val(row.wFNextFlag);
                  $('#CreatedBy').val(row.createdBy);
                  $('#CreatedDate').val(row.createdDate);
                  $('#Remarks').val(row.remarks);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
