var tblIdxRoleAccess='#tblIdxRoleAccess';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#IdxRoleAccessForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                MstIdxRoleAccessId: $('#MstIdxRoleAccessId').val(),
                MenuId: $('#MenuId').val(),
                RoleId: $('#RoleId').val(),
                };
     var result = Common.Form.Save('/IdxRoleAccess/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/IdxRoleAccess/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#MstIdxRoleAccessId').val('');
      $('#MenuId').val('');
      $('#RoleId').val('');
    },
}

var FormValidation = {
    MstIdxRoleAccessId: {
     presence: true,
     },
    MenuId: {
     presence: true,
     },
    RoleId: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblIdxRoleAccess);
       },
       LoadData: function () {
          return Common.GetData.Get('/IdxRoleAccess/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'mstIdxRoleAccessId'},
                    {'data':'menuId'},
                    {'data':'roleId'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblIdxRoleAccess, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblIdxRoleAccess).unbind();
              $(tblIdxRoleAccess).on('click', '.btnRemove', function (e) {
                  var tb = $(tblIdxRoleAccess).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        MstIdxRoleAccessId : row.mstIdxRoleAccessId,
                        MenuId : row.menuId,
                        RoleId : row.roleId,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblIdxRoleAccess).on('click', '.btnEdit', function (e) {
                  var tb = $(tblIdxRoleAccess).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#MstIdxRoleAccessId').val(row.mstIdxRoleAccessId);
                  $('#MenuId').val(row.menuId);
                  $('#RoleId').val(row.roleId);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
