var tblTrxUserImages='#tblTrxUserImages';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#TrxUserImagesForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                UserImageId: $('#UserImageId').val(),
                UserId: $('#UserId').val(),
                ImageSource: $('#ImageSource').val(),
                ImageFaceRecogSource: $('#ImageFaceRecogSource').val(),
                IsValidImage: $('#IsValidImage').val(),
                CreatedDate: $('#CreatedDate').val(),
                CreatedBy: $('#CreatedBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdateBy: $('#UpdateBy').val(),
                };
     var result = Common.Form.Save('/TrxUserImages/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/TrxUserImages/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#UserImageId').val('');
      $('#UserId').val('');
      $('#ImageSource').val('');
      $('#ImageFaceRecogSource').val('');
      $('#IsValidImage').val('');
      $('#CreatedDate').val('');
      $('#CreatedBy').val('');
      $('#UpdatedDate').val('');
      $('#UpdateBy').val('');
    },
}

var FormValidation = {
    UserImageId: {
     presence: true,
     },
    UserId: {
     presence: true,
     },
    ImageSource: {
     presence: true,
     },
    ImageFaceRecogSource: {
     presence: true,
     },
    IsValidImage: {
     presence: true,
     },
    CreatedDate: {
     presence: true,
     },
    CreatedBy: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblTrxUserImages);
       },
       LoadData: function () {
          return Common.GetData.Get('/TrxUserImages/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'userImageId'},
                    {'data':'userId'},
                    {'data':'imageSource'},
                    {'data':'imageFaceRecogSource'},
                    {'data':'isValidImage'},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'createdBy'},
                    {'data':'updatedDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updateBy'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblTrxUserImages, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblTrxUserImages).unbind();
              $(tblTrxUserImages).on('click', '.btnRemove', function (e) {
                  var tb = $(tblTrxUserImages).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        UserImageId : row.userImageId,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblTrxUserImages).on('click', '.btnEdit', function (e) {
                  var tb = $(tblTrxUserImages).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#UserImageId').val(row.userImageId);
                  $('#UserId').val(row.userId);
                  $('#ImageSource').val(row.imageSource);
                  $('#ImageFaceRecogSource').val(row.imageFaceRecogSource);
                  $('#IsValidImage').val(row.isValidImage);
                  $('#CreatedDate').val(row.createdDate);
                  $('#CreatedBy').val(row.createdBy);
                  $('#UpdatedDate').val(row.updatedDate);
                  $('#UpdateBy').val(row.updateBy);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
