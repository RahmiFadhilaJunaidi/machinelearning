var tblMstStudent='#tblMstStudent';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#MstStudentForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                MstStudentId: $('#MstStudentId').val(),
                StudentName: $('#StudentName').val(),
                Sex: $('#Sex').val(),
                BirthDay: $('#BirthDay').val(),
                PhoneNumber: $('#PhoneNumber').val(),
                PhoneNumberParent: $('#PhoneNumberParent').val(),
                Address: $('#Address').val(),
                AlementarySchoolId: $('#AlementarySchoolId').val(),
                CreatedDate: $('#CreatedDate').val(),
                CreatedBy: $('#CreatedBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                };
     var result = Common.Form.Save('/MstStudent/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/MstStudent/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#MstStudentId').val('');
      $('#StudentName').val('');
      $('#Sex').val('');
      $('#BirthDay').val('');
      $('#PhoneNumber').val('');
      $('#PhoneNumberParent').val('');
      $('#Address').val('');
      $('#AlementarySchoolId').val('');
      $('#CreatedDate').val('');
      $('#CreatedBy').val('');
      $('#UpdatedDate').val('');
      $('#UpdatedBy').val('');
    },
}

var FormValidation = {
    MstStudentId: {
     presence: true,
     },
    StudentName: {
     presence: true,
     },
    Sex: {
     presence: true,
     },
    BirthDay: {
     presence: true,
     },
    Address: {
     presence: true,
     },
    AlementarySchoolId: {
     presence: true,
     },
    CreatedDate: {
     presence: true,
     },
    CreatedBy: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblMstStudent);
       },
       LoadData: function () {
          return Common.GetData.Get('/MstStudent/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'mstStudentId'},
                    {'data':'studentName'},
                    {'data':'sex'},
                    {'data':'birthDay', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'phoneNumber'},
                    {'data':'phoneNumberParent'},
                    {'data':'address'},
                    {'data':'alementarySchoolId'},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'createdBy'},
                    {'data':'updatedDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updatedBy'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblMstStudent, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblMstStudent).unbind();
              $(tblMstStudent).on('click', '.btnRemove', function (e) {
                  var tb = $(tblMstStudent).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        MstStudentId : row.mstStudentId,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblMstStudent).on('click', '.btnEdit', function (e) {
                  var tb = $(tblMstStudent).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#MstStudentId').val(row.mstStudentId);
                  $('#StudentName').val(row.studentName);
                  $('#Sex').val(row.sex);
                  $('#BirthDay').val(row.birthDay);
                  $('#PhoneNumber').val(row.phoneNumber);
                  $('#PhoneNumberParent').val(row.phoneNumberParent);
                  $('#Address').val(row.address);
                  $('#AlementarySchoolId').val(row.alementarySchoolId);
                  $('#CreatedDate').val(row.createdDate);
                  $('#CreatedBy').val(row.createdBy);
                  $('#UpdatedDate').val(row.updatedDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
