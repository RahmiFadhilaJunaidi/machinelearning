var tblVwTrxMappingModulDetail='#tblVwTrxMappingModulDetail';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#VwTrxMappingModulDetailForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                MstObjectId: $('#MstObjectId').val(),
                TrxMappingModulId: $('#TrxMappingModulId').val(),
                ObjectName: $('#ObjectName').val(),
                };
     var result = Common.Form.Save('/VwTrxMappingModulDetail/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/VwTrxMappingModulDetail/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#MstObjectId').val('');
      $('#TrxMappingModulId').val('');
      $('#ObjectName').val('');
    },
}

var FormValidation = {
    MstObjectId: {
     presence: true,
     },
    TrxMappingModulId: {
     presence: true,
     },
    ObjectName: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblVwTrxMappingModulDetail);
       },
       LoadData: function () {
          return Common.GetData.Get('/VwTrxMappingModulDetail/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'mstObjectId'},
                    {'data':'trxMappingModulId'},
                    {'data':'objectName'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblVwTrxMappingModulDetail, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblVwTrxMappingModulDetail).unbind();
              $(tblVwTrxMappingModulDetail).on('click', '.btnRemove', function (e) {
                  var tb = $(tblVwTrxMappingModulDetail).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblVwTrxMappingModulDetail).on('click', '.btnEdit', function (e) {
                  var tb = $(tblVwTrxMappingModulDetail).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#MstObjectId').val(row.mstObjectId);
                  $('#TrxMappingModulId').val(row.trxMappingModulId);
                  $('#ObjectName').val(row.objectName);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
