var tblMstObjectCategory='#tblMstObjectCategory';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#MstObjectCategoryForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                MstObjectCategoryId: $('#MstObjectCategoryId').val(),
                ObjectCategory: $('#ObjectCategory').val(),
                IsActive: $('#IsActive').val(),
                CreatedDate: $('#CreatedDate').val(),
                };
     var result = Common.Form.Save('/MstObjectCategory/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/MstObjectCategory/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#MstObjectCategoryId').val('');
      $('#ObjectCategory').val('');
      $('#IsActive').val('');
      $('#CreatedDate').val('');
    },
}

var FormValidation = {
    MstObjectCategoryId: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblMstObjectCategory);
       },
       LoadData: function () {
          return Common.GetData.Get('/MstObjectCategory/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'mstObjectCategoryId'},
                    {'data':'objectCategory'},
                    {'data':'isActive'},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblMstObjectCategory, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblMstObjectCategory).unbind();
              $(tblMstObjectCategory).on('click', '.btnRemove', function (e) {
                  var tb = $(tblMstObjectCategory).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        MstObjectCategoryId : row.mstObjectCategoryId,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblMstObjectCategory).on('click', '.btnEdit', function (e) {
                  var tb = $(tblMstObjectCategory).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#MstObjectCategoryId').val(row.mstObjectCategoryId);
                  $('#ObjectCategory').val(row.objectCategory);
                  $('#IsActive').val(row.isActive);
                  $('#CreatedDate').val(row.createdDate);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
