var tblVwMstObjectHierarchy='#tblVwMstObjectHierarchy';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#VwMstObjectHierarchyForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                MstObjectId: $('#MstObjectId').val(),
                ObjectName: $('#ObjectName').val(),
                ObjectParentId: $('#ObjectParentId').val(),
                Sort: $('#Sort').val(),
                SystemId: $('#SystemId').val(),
                ProjectId: $('#ProjectId').val(),
                MstModulTypeId: $('#MstModulTypeId').val(),
                CreatedBy: $('#CreatedBy').val(),
                CreatedDate: $('#CreatedDate').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                MenuParent: $('#MenuParent').val(),
                };
     var result = Common.Form.Save('/VwMstObjectHierarchy/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/VwMstObjectHierarchy/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#MstObjectId').val('');
      $('#ObjectName').val('');
      $('#ObjectParentId').val('');
      $('#Sort').val('');
      $('#SystemId').val('');
      $('#ProjectId').val('');
      $('#MstModulTypeId').val('');
      $('#CreatedBy').val('');
      $('#CreatedDate').val('');
      $('#UpdatedDate').val('');
      $('#UpdatedBy').val('');
      $('#MenuParent').val('');
    },
}

var FormValidation = {
    MstObjectId: {
     presence: true,
     },
    ObjectName: {
     presence: true,
     },
    ObjectParentId: {
     presence: true,
     },
    Sort: {
     presence: true,
     },
    SystemId: {
     presence: true,
     },
    ProjectId: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblVwMstObjectHierarchy);
       },
       LoadData: function () {
          return Common.GetData.Get('/VwMstObjectHierarchy/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'mstObjectId'},
                    {'data':'objectName'},
                    {'data':'objectParentId'},
                    {'data':'sort'},
                    {'data':'systemId'},
                    {'data':'projectId'},
                    {'data':'mstModulTypeId'},
                    {'data':'createdBy'},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updatedDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updatedBy'},
                    {'data':'menuParent'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblVwMstObjectHierarchy, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblVwMstObjectHierarchy).unbind();
              $(tblVwMstObjectHierarchy).on('click', '.btnRemove', function (e) {
                  var tb = $(tblVwMstObjectHierarchy).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblVwMstObjectHierarchy).on('click', '.btnEdit', function (e) {
                  var tb = $(tblVwMstObjectHierarchy).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#MstObjectId').val(row.mstObjectId);
                  $('#ObjectName').val(row.objectName);
                  $('#ObjectParentId').val(row.objectParentId);
                  $('#Sort').val(row.sort);
                  $('#SystemId').val(row.systemId);
                  $('#ProjectId').val(row.projectId);
                  $('#MstModulTypeId').val(row.mstModulTypeId);
                  $('#CreatedBy').val(row.createdBy);
                  $('#CreatedDate').val(row.createdDate);
                  $('#UpdatedDate').val(row.updatedDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  $('#MenuParent').val(row.menuParent);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
