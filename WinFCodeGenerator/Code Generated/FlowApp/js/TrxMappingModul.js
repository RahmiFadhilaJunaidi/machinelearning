var tblTrxMappingModul='#tblTrxMappingModul';
jQuery(document).ready(function () {
 Table.Init();
 Table.LoadTable();
 Helper.PanelHeaderShow();
 Helper.InitButtons();

 var form = document.querySelector('form#TrxMappingModulForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Form = {
    Submit: function () {
     var data = {
                TrxMappingModulId: $('#TrxMappingModulId').val(),
                ProjectId: $('#ProjectId').val(),
                SystemId: $('#SystemId').val(),
                ModulTypeId: $('#ModulTypeId').val(),
                ObjectName: $('#ObjectName').val(),
                CratedDate: $('#CratedDate').val(),
                CreatedBy: $('#CreatedBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                };
     var result = Common.Form.Save('/TrxMappingModul/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/TrxMappingModul/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#TrxMappingModulId').val('');
      $('#ProjectId').val('');
      $('#SystemId').val('');
      $('#ModulTypeId').val('');
      $('#ObjectName').val('');
      $('#CratedDate').val('');
      $('#CreatedBy').val('');
      $('#UpdatedDate').val('');
      $('#UpdatedBy').val('');
    },
}

var FormValidation = {
    TrxMappingModulId: {
     presence: true,
     },
    ProjectId: {
     presence: true,
     },
    SystemId: {
     presence: true,
     },
    ModulTypeId: {
     presence: true,
     },
    ObjectName: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          Common.Table.InitClient(tblTrxMappingModul);
       },
       LoadData: function () {
          return Common.GetData.Get('/TrxMappingModul/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'trxMappingModulId'},
                    {'data':'projectId'},
                    {'data':'systemId'},
                    {'data':'modulTypeId'},
                    {'data':'objectName'},
                    {'data':'cratedDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'createdBy'},
                    {'data':'updatedDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updatedBy'},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblTrxMappingModul, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblTrxMappingModul).unbind();
              $(tblTrxMappingModul).on('click', '.btnRemove', function (e) {
                  var tb = $(tblTrxMappingModul).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        TrxMappingModulId : row.trxMappingModulId,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblTrxMappingModul).on('click', '.btnEdit', function (e) {
                  var tb = $(tblTrxMappingModul).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#TrxMappingModulId').val(row.trxMappingModulId);
                  $('#ProjectId').val(row.projectId);
                  $('#SystemId').val(row.systemId);
                  $('#ModulTypeId').val(row.modulTypeId);
                  $('#ObjectName').val(row.objectName);
                  $('#CratedDate').val(row.cratedDate);
                  $('#CreatedBy').val(row.createdBy);
                  $('#UpdatedDate').val(row.updatedDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
       }
}
