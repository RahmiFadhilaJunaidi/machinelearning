using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.DA.Service.FlowApp
 {
 public class MstMenuService
 {
     BaseModel baseModel = new BaseModel();

     public MstMenuBase MstMenuGetList()
     {
         var listBase = new MstMenuBase();
         try
         {
             using (var context = new FlowAppContext())
             {
                 listBase.MstMenuList = context.MstMenu.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstMenuDelete(MstMenu Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstMenu.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstMenuCreate(MstMenu Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstMenu.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstMenuUpdate(MstMenu Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstMenu.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
