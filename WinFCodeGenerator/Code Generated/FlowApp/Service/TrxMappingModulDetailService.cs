using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.DA.Service.FlowApp
 {
 public class TrxMappingModulDetailService
 {
     BaseModel baseModel = new BaseModel();

     public TrxMappingModulDetailBase TrxMappingModulDetailGetList()
     {
         var listBase = new TrxMappingModulDetailBase();
         try
         {
             using (var context = new FlowAppContext())
             {
                 listBase.TrxMappingModulDetailList = context.TrxMappingModulDetail.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string TrxMappingModulDetailDelete(TrxMappingModulDetail Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.TrxMappingModulDetail.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string TrxMappingModulDetailCreate(TrxMappingModulDetail Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.TrxMappingModulDetail.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string TrxMappingModulDetailUpdate(TrxMappingModulDetail Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.TrxMappingModulDetail.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
