using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.DA.Service.FlowApp
 {
 public class MstObjectCategoryService
 {
     BaseModel baseModel = new BaseModel();

     public MstObjectCategoryBase MstObjectCategoryGetList()
     {
         var listBase = new MstObjectCategoryBase();
         try
         {
             using (var context = new FlowAppContext())
             {
                 listBase.MstObjectCategoryList = context.MstObjectCategory.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstObjectCategoryDelete(MstObjectCategory Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstObjectCategory.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstObjectCategoryCreate(MstObjectCategory Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstObjectCategory.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstObjectCategoryUpdate(MstObjectCategory Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstObjectCategory.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
