using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.DA.Service.FlowApp
 {
 public class MstApplicationVersionService
 {
     BaseModel baseModel = new BaseModel();

     public MstApplicationVersionBase MstApplicationVersionGetList()
     {
         var listBase = new MstApplicationVersionBase();
         try
         {
             using (var context = new FlowAppContext())
             {
                 listBase.MstApplicationVersionList = context.MstApplicationVersion.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstApplicationVersionDelete(MstApplicationVersion Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstApplicationVersion.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstApplicationVersionCreate(MstApplicationVersion Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstApplicationVersion.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstApplicationVersionUpdate(MstApplicationVersion Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstApplicationVersion.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
