using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.DA.Service.FlowApp
 {
 public class TrxMappingModulService
 {
     BaseModel baseModel = new BaseModel();

     public TrxMappingModulBase TrxMappingModulGetList()
     {
         var listBase = new TrxMappingModulBase();
         try
         {
             using (var context = new FlowAppContext())
             {
                 listBase.TrxMappingModulList = context.TrxMappingModul.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string TrxMappingModulDelete(TrxMappingModul Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.TrxMappingModul.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string TrxMappingModulCreate(TrxMappingModul Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.TrxMappingModul.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string TrxMappingModulUpdate(TrxMappingModul Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.TrxMappingModul.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
