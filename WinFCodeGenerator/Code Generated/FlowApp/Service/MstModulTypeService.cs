using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.DA.Service.FlowApp
 {
 public class MstModulTypeService
 {
     BaseModel baseModel = new BaseModel();

     public MstModulTypeBase MstModulTypeGetList()
     {
         var listBase = new MstModulTypeBase();
         try
         {
             using (var context = new FlowAppContext())
             {
                 listBase.MstModulTypeList = context.MstModulType.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstModulTypeDelete(MstModulType Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstModulType.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstModulTypeCreate(MstModulType Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstModulType.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstModulTypeUpdate(MstModulType Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstModulType.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
