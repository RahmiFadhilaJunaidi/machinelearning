using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.DA.Service.FlowApp
 {
 public class MstObjectService
 {
     BaseModel baseModel = new BaseModel();

     public MstObjectBase MstObjectGetList()
     {
         var listBase = new MstObjectBase();
         try
         {
             using (var context = new FlowAppContext())
             {
                 listBase.MstObjectList = context.MstObject.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstObjectDelete(MstObject Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstObject.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstObjectCreate(MstObject Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstObject.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstObjectUpdate(MstObject Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstObject.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
