using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.DA.Service.FlowApp
 {
 public class MstSystemService
 {
     BaseModel baseModel = new BaseModel();

     public MstSystemBase MstSystemGetList()
     {
         var listBase = new MstSystemBase();
         try
         {
             using (var context = new FlowAppContext())
             {
                 listBase.MstSystemList = context.MstSystem.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstSystemDelete(MstSystem Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstSystem.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstSystemCreate(MstSystem Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstSystem.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstSystemUpdate(MstSystem Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstSystem.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
