using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseAccess;
using LibrarySys.DA.Context;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.DA.Service.FlowApp
{
 public class FlowAppMasterService
 {
     BaseModel baseModel = new BaseModel();

     public  VwTrxMappingModulDetailBase VwTrxMappingModulDetailGetList()
     {
         var listBase = new VwTrxMappingModulDetailBase();
         try
         {
             using (var context = new FlowAppContext())
             {
                 listBase.VwTrxMappingModulDetailList = context.VwTrxMappingModulDetail.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
         return listBase;
         }
     }

 }
}
