using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.DA.Service.FlowApp
 {
 public class MstProjectService
 {
     BaseModel baseModel = new BaseModel();

     public MstProjectBase MstProjectGetList()
     {
         var listBase = new MstProjectBase();
         try
         {
             using (var context = new FlowAppContext())
             {
                 listBase.MstProjectList = context.MstProject.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstProjectDelete(MstProject Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstProject.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstProjectCreate(MstProject Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstProject.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstProjectUpdate(MstProject Post)
     {
         try
         {
             using (var context = new FlowAppContext())
             {
                 context.MstProject.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
