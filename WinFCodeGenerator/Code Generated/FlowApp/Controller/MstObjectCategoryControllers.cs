using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.FlowApp;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.Controllers
{
 public class MstObjectCategoryController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/FlowApp/MstObjectCategory.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstObjectCategoryBase();
         try
         {
             var service = new MstObjectCategoryService();
             result = service.MstObjectCategoryGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstObjectCategoryList });
     }


     [HttpPost]
     public IActionResult Create(MstObjectCategory Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstObjectCategoryService();
             respon.errorMessage = service.MstObjectCategoryCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstObjectCategory Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstObjectCategoryService();
             respon.errorMessage = service.MstObjectCategoryUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstObjectCategory Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstObjectCategoryService();
             respon.errorMessage = service.MstObjectCategoryDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
