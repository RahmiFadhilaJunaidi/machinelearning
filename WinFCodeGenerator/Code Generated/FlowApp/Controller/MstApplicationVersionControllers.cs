using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.FlowApp;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.Controllers
{
 public class MstApplicationVersionController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/FlowApp/MstApplicationVersion.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstApplicationVersionBase();
         try
         {
             var service = new MstApplicationVersionService();
             result = service.MstApplicationVersionGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstApplicationVersionList });
     }


     [HttpPost]
     public IActionResult Create(MstApplicationVersion Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstApplicationVersionService();
             respon.errorMessage = service.MstApplicationVersionCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstApplicationVersion Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstApplicationVersionService();
             respon.errorMessage = service.MstApplicationVersionUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstApplicationVersion Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstApplicationVersionService();
             respon.errorMessage = service.MstApplicationVersionDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
