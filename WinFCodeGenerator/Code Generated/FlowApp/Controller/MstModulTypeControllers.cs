using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.FlowApp;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.Controllers
{
 public class MstModulTypeController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/FlowApp/MstModulType.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstModulTypeBase();
         try
         {
             var service = new MstModulTypeService();
             result = service.MstModulTypeGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstModulTypeList });
     }


     [HttpPost]
     public IActionResult Create(MstModulType Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstModulTypeService();
             respon.errorMessage = service.MstModulTypeCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstModulType Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstModulTypeService();
             respon.errorMessage = service.MstModulTypeUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstModulType Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstModulTypeService();
             respon.errorMessage = service.MstModulTypeDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
