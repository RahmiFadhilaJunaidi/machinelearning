using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.FlowApp;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.Controllers
{
 public class MstMenuController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/FlowApp/MstMenu.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstMenuBase();
         try
         {
             var service = new MstMenuService();
             result = service.MstMenuGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstMenuList });
     }


     [HttpPost]
     public IActionResult Create(MstMenu Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstMenuService();
             respon.errorMessage = service.MstMenuCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstMenu Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstMenuService();
             respon.errorMessage = service.MstMenuUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstMenu Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstMenuService();
             respon.errorMessage = service.MstMenuDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
