using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.FlowApp;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.Controllers
{
 public class VwMstObjectHierarchyController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/FlowApp/VwMstObjectHierarchy.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwMstObjectHierarchyBase();
         try
         {
             var service = new VwMstObjectHierarchyService();
             result = service.VwMstObjectHierarchyGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwMstObjectHierarchyList });
     }


     [HttpPost]
     public IActionResult Create(VwMstObjectHierarchy Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwMstObjectHierarchyService();
             respon.errorMessage = service.VwMstObjectHierarchyCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(VwMstObjectHierarchy Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwMstObjectHierarchyService();
             respon.errorMessage = service.VwMstObjectHierarchyUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(VwMstObjectHierarchy Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwMstObjectHierarchyService();
             respon.errorMessage = service.VwMstObjectHierarchyDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
