using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.FlowApp;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.Controllers
{
 public class VwMstObjectController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/FlowApp/VwMstObject.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwMstObjectBase();
         try
         {
             var service = new VwMstObjectService();
             result = service.VwMstObjectGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwMstObjectList });
     }


     [HttpPost]
     public IActionResult Create(VwMstObject Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwMstObjectService();
             respon.errorMessage = service.VwMstObjectCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(VwMstObject Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwMstObjectService();
             respon.errorMessage = service.VwMstObjectUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(VwMstObject Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwMstObjectService();
             respon.errorMessage = service.VwMstObjectDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
