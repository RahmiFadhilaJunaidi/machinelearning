using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.FlowApp;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.Controllers
{
 public class MstProjectController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/FlowApp/MstProject.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstProjectBase();
         try
         {
             var service = new MstProjectService();
             result = service.MstProjectGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstProjectList });
     }


     [HttpPost]
     public IActionResult Create(MstProject Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstProjectService();
             respon.errorMessage = service.MstProjectCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstProject Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstProjectService();
             respon.errorMessage = service.MstProjectUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstProject Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstProjectService();
             respon.errorMessage = service.MstProjectDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
