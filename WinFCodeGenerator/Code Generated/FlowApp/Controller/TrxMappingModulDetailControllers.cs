using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.FlowApp;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.Controllers
{
 public class TrxMappingModulDetailController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/FlowApp/TrxMappingModulDetail.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new TrxMappingModulDetailBase();
         try
         {
             var service = new TrxMappingModulDetailService();
             result = service.TrxMappingModulDetailGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.TrxMappingModulDetailList });
     }


     [HttpPost]
     public IActionResult Create(TrxMappingModulDetail Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxMappingModulDetailService();
             respon.errorMessage = service.TrxMappingModulDetailCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(TrxMappingModulDetail Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxMappingModulDetailService();
             respon.errorMessage = service.TrxMappingModulDetailUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(TrxMappingModulDetail Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxMappingModulDetailService();
             respon.errorMessage = service.TrxMappingModulDetailDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
