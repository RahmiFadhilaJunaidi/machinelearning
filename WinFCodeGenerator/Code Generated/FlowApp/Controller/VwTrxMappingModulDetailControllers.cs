using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.FlowApp;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.Controllers
{
 public class VwTrxMappingModulDetailController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/FlowApp/VwTrxMappingModulDetail.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new VwTrxMappingModulDetailBase();
         try
         {
             var service = new VwTrxMappingModulDetailService();
             result = service.VwTrxMappingModulDetailGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwTrxMappingModulDetailList });
     }


     [HttpPost]
     public IActionResult Create(VwTrxMappingModulDetail Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwTrxMappingModulDetailService();
             respon.errorMessage = service.VwTrxMappingModulDetailCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(VwTrxMappingModulDetail Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwTrxMappingModulDetailService();
             respon.errorMessage = service.VwTrxMappingModulDetailUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(VwTrxMappingModulDetail Post)
     {
         var respon = new Respon();
         try
         {
             var service = new VwTrxMappingModulDetailService();
             respon.errorMessage = service.VwTrxMappingModulDetailDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
