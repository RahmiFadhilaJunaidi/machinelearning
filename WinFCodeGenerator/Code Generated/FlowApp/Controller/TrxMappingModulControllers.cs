using System;
using Microsoft.AspNetCore.Mvc;
using LibrarySys.DA.Service.FlowApp;
using LibrarySys.DA.Models.FlowApp;

namespace LibrarySys.Controllers
{
 public class TrxMappingModulController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/FlowApp/TrxMappingModul.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new TrxMappingModulBase();
         try
         {
             var service = new TrxMappingModulService();
             result = service.TrxMappingModulGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.TrxMappingModulList });
     }


     [HttpPost]
     public IActionResult Create(TrxMappingModul Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxMappingModulService();
             respon.errorMessage = service.TrxMappingModulCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(TrxMappingModul Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxMappingModulService();
             respon.errorMessage = service.TrxMappingModulUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(TrxMappingModul Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxMappingModulService();
             respon.errorMessage = service.TrxMappingModulDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
