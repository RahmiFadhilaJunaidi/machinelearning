using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.FlowApp
{
     public partial class MstSystemBase : BaseModel
     {
         public MstSystem MstSystem { get; set; }
         public List<MstSystem> MstSystemList { get; set; }
     }
}
