using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.FlowApp
{
     public partial class VwMstObjectBase : BaseModel
     {
         public VwMstObject VwMstObject { get; set; }
         public List<VwMstObject> VwMstObjectList { get; set; }
     }
}
