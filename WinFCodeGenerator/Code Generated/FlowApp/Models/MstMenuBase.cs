using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.FlowApp
{
     public partial class MstMenuBase : BaseModel
     {
         public MstMenu MstMenu { get; set; }
         public List<MstMenu> MstMenuList { get; set; }
     }
}
