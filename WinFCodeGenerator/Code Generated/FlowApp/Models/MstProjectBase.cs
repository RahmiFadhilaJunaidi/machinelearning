using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.FlowApp
{
     public partial class MstProjectBase : BaseModel
     {
         public MstProject MstProject { get; set; }
         public List<MstProject> MstProjectList { get; set; }
     }
}
