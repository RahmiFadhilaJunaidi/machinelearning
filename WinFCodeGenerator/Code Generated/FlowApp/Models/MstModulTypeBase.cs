using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.FlowApp
{
     public partial class MstModulTypeBase : BaseModel
     {
         public MstModulType MstModulType { get; set; }
         public List<MstModulType> MstModulTypeList { get; set; }
     }
}
