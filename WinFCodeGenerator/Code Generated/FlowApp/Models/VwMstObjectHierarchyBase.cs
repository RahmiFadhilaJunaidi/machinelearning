using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.FlowApp
{
     public partial class VwMstObjectHierarchyBase : BaseModel
     {
         public VwMstObjectHierarchy VwMstObjectHierarchy { get; set; }
         public List<VwMstObjectHierarchy> VwMstObjectHierarchyList { get; set; }
     }
}
