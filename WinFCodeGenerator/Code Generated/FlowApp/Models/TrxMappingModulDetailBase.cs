using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.FlowApp
{
     public partial class TrxMappingModulDetailBase : BaseModel
     {
         public TrxMappingModulDetail TrxMappingModulDetail { get; set; }
         public List<TrxMappingModulDetail> TrxMappingModulDetailList { get; set; }
     }
}
