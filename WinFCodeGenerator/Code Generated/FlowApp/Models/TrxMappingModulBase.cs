using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.FlowApp
{
     public partial class TrxMappingModulBase : BaseModel
     {
         public TrxMappingModul TrxMappingModul { get; set; }
         public List<TrxMappingModul> TrxMappingModulList { get; set; }
     }
}
