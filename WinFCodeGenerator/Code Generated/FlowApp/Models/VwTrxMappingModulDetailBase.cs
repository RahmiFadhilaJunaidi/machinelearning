using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.FlowApp
{
     public partial class VwTrxMappingModulDetailBase : BaseModel
     {
         public VwTrxMappingModulDetail VwTrxMappingModulDetail { get; set; }
         public List<VwTrxMappingModulDetail> VwTrxMappingModulDetailList { get; set; }
     }
}
