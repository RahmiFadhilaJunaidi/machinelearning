using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.FlowApp
{
     public partial class MstObjectCategoryBase : BaseModel
     {
         public MstObjectCategory MstObjectCategory { get; set; }
         public List<MstObjectCategory> MstObjectCategoryList { get; set; }
     }
}
