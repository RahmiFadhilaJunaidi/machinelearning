using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.FlowApp
{
     public partial class MstApplicationVersionBase : BaseModel
     {
         public MstApplicationVersion MstApplicationVersion { get; set; }
         public List<MstApplicationVersion> MstApplicationVersionList { get; set; }
     }
}
