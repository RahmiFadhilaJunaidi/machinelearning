using System;
using System.Collections.Generic;

namespace LibrarySys.DA.Models.FlowApp
{
     public partial class MstObjectBase : BaseModel
     {
         public MstObject MstObject { get; set; }
         public List<MstObject> MstObjectList { get; set; }
     }
}
