﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WinFCodeGenerator
{
    public class CodeGenerated
    {
        #region declare variable
        static SqlConnection connection;
        static List<string> viewList = new List<string>();
        static List<TableModel> tableModels = new List<TableModel>();
        static List<TableModel> vwTableModels = new List<TableModel>();
        #endregion
        public CodeGenerated(SqlConnection con, string projectName, string module, string objectName, string dbContext, string objectPath)
        {
            connection = con;
            string[] listFiles;
            listFiles = Directory.GetFiles(objectPath, "*.cs");

            var fileNameList = new List<string>();
            for (int i = 0; i < listFiles.Length; i++)
            {
                fileNameList.Add(Path.GetFileName(listFiles[i]));
            }
            var fileList = new List<string>();
            var fileListMaster = new List<string>();
            fileNameList = fileNameList.Where(x => !x.Contains("Base")).ToList();
            if (!string.IsNullOrEmpty(objectName))
            {
                fileList = fileNameList.Where(x => x.ToLower().Contains(objectName.ToLower()) && !x.ToLower().Contains("vw")).ToList();
                fileListMaster = fileNameList.Where(x => x.ToLower().Contains(objectName.ToLower()) && x.ToLower().Contains("vw")).ToList();
                viewList = fileListMaster;
            }
            else
            {
                fileList = fileNameList.Where(x => !x.ToLower().Contains("vw")).ToList();
                fileListMaster = fileNameList.Where(x => x.ToLower().Contains("vw")).ToList();
                viewList = fileListMaster;
            }

            GenerateServices(projectName, module, fileList, dbContext);
            GenerateServiceMaster(projectName, module, fileListMaster, dbContext);
            GenerateObjectBaseModel(projectName, module, fileList);
            GenerateController(projectName, module, fileList);
            GenerateJavascript(projectName, module, fileList);
            GenerateHtml(projectName, module, fileList);
        }

        #region Generate Code By Type Code
        private static void GenerateServices(string projectName, string nameSpace, List<string> listFiles, string dbContext)
        {

            for (int i = 0; i < listFiles.Count; i++)
            {
                StringBuilder sb = new StringBuilder();
                string className = listFiles[i].Replace(".cs", "");
                sb.AppendLine("using System;");
                sb.AppendLine("using System.Collections.Generic;");
                sb.AppendLine("using System.Linq;");
                sb.AppendLine("using " + projectName + ".DA.Models." + nameSpace + ";");
                sb.AppendLine("");
                sb.AppendLine("namespace " + projectName + ".DA.Service." + nameSpace);
                sb.AppendLine(" {");
                sb.AppendLine(" public class " + className + "Service");
                sb.AppendLine(" {");
                sb.AppendLine("     BaseModel baseModel = new BaseModel();");
                sb.AppendLine("");
                /* Get List Data*/
                /*start*/
                if (viewList.Where(x => x.ToLower().Contains("vw" + className.ToLower())).ToList().Count > 0)
                {
                    string vwName = viewList.Where(x => x.ToLower().Contains("vw" + className.ToLower())).FirstOrDefault();
                    vwName = vwName.Replace(".cs", "");

                    sb.AppendLine("     public " + vwName + "Base " + vwName + "GetList()");
                    sb.AppendLine("     {");
                    sb.AppendLine("         var listBase = new " + vwName + "Base();");
                    sb.AppendLine("         try");
                    sb.AppendLine("         {");
                    sb.AppendLine("             using (var context = new " + dbContext + "())");
                    sb.AppendLine("             {");
                    sb.AppendLine("                 listBase." + vwName + "List = context." + vwName + ".ToList();");
                    sb.AppendLine("                 context.Dispose();");
                    sb.AppendLine("             }");
                    sb.AppendLine("             return listBase;");
                    sb.AppendLine("         }");
                    sb.AppendLine("         catch (Exception ex)");
                    sb.AppendLine("         {");
                    sb.AppendLine("             listBase.errorMessage = listBase.errorService+ ex.Message;");
                    sb.AppendLine("             listBase.errorType = 1;");
                    sb.AppendLine("             return listBase;");
                    sb.AppendLine("         }");
                    sb.AppendLine("     }");
                    sb.AppendLine("");
                }
                else
                {
                    sb.AppendLine("     public " + className + "Base " + className + "GetList()");
                    sb.AppendLine("     {");
                    sb.AppendLine("         var listBase = new " + className + "Base();");
                    sb.AppendLine("         try");
                    sb.AppendLine("         {");
                    sb.AppendLine("             using (var context = new " + dbContext + "())");
                    sb.AppendLine("             {");
                    sb.AppendLine("                 listBase." + className + "List = context." + className + ".ToList();");
                    sb.AppendLine("                 context.Dispose();");
                    sb.AppendLine("             }");
                    sb.AppendLine("             return listBase;");
                    sb.AppendLine("         }");
                    sb.AppendLine("         catch (Exception ex)");
                    sb.AppendLine("         {");
                    sb.AppendLine("             listBase.errorMessage = listBase.errorService+ ex.Message;");
                    sb.AppendLine("             listBase.errorType = 1;");
                    sb.AppendLine("             return listBase;");
                    sb.AppendLine("         }");
                    sb.AppendLine("     }");
                    sb.AppendLine("");
                    /*end*/
                }
                /**/
                /*delete*/
                sb.AppendLine("     public string " + className + "Delete(" + className + " Post)");
                sb.AppendLine("     {");
                sb.AppendLine("         try");
                sb.AppendLine("         {");
                sb.AppendLine("             using (var context = new " + dbContext + "())");
                sb.AppendLine("             {");
                sb.AppendLine("                 context." + className + ".Remove(Post);");
                sb.AppendLine("                 context.SaveChanges();");
                sb.AppendLine("                 context.Dispose();");
                sb.AppendLine("             }");
                sb.AppendLine(@"            return """";");
                sb.AppendLine("         }");
                sb.AppendLine("         catch (Exception ex)");
                sb.AppendLine("         {");
                sb.AppendLine("             return baseModel.errorService + ex.Message;");
                sb.AppendLine("         }");
                sb.AppendLine("     }");
                sb.AppendLine("");
                /*end*/

                /**/
                /*create*/
                sb.AppendLine("     public string " + className + "Create(" + className + " Post)");
                sb.AppendLine("     {");
                sb.AppendLine("         try");
                sb.AppendLine("         {");
                sb.AppendLine("             using (var context = new " + dbContext + "())");
                sb.AppendLine("             {");
                sb.AppendLine("                 context." + className + ".Add(Post);");
                sb.AppendLine("                 context.SaveChanges();");
                sb.AppendLine("                 context.Dispose();");
                sb.AppendLine("             }");
                sb.AppendLine(@"            return """";");
                sb.AppendLine("         }");
                sb.AppendLine("         catch (Exception ex)");
                sb.AppendLine("         {");
                sb.AppendLine("             return baseModel+ ex.Message;");
                sb.AppendLine("         }");
                sb.AppendLine("     }");
                sb.AppendLine("");
                /*start*/

                /*end*/
                sb.AppendLine("     public string " + className + "Update(" + className + " Post)");
                sb.AppendLine("     {");
                sb.AppendLine("         try");
                sb.AppendLine("         {");
                sb.AppendLine("             using (var context = new " + dbContext + "())");
                sb.AppendLine("             {");
                sb.AppendLine("                 context." + className + ".Update(Post);");
                sb.AppendLine("                 context.SaveChanges();");
                sb.AppendLine("                 context.Dispose();");
                sb.AppendLine("             }");
                sb.AppendLine(@"            return """";");
                sb.AppendLine("         }");
                sb.AppendLine("         catch (Exception ex)");
                sb.AppendLine("         {");
                sb.AppendLine("             return baseModel+ ex.Message;");
                sb.AppendLine("         }");
                sb.AppendLine("     }");
                sb.AppendLine("");
                /*end*/
                sb.AppendLine(" }");
                sb.AppendLine("}");

                GenerateFile(projectName, nameSpace, className + "Service", "service", sb);
            }

        }
        private static void GenerateServiceMaster(string projectName, string nameSpace, List<string> listFiles, string dbContext)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("using System;");
            sb.AppendLine("using System.Collections.Generic;");
            sb.AppendLine("using System.Linq;");
            sb.AppendLine("using DatabaseAccess;");
            sb.AppendLine("using " + projectName + ".DA.Context;");
            sb.AppendLine("using " + projectName + ".DA.Models." + nameSpace + ";");
            sb.AppendLine("");
            sb.AppendLine("namespace " + projectName + ".DA.Service." + nameSpace);
            sb.AppendLine("{");
            sb.AppendLine(" public class " + nameSpace + "MasterService");
            sb.AppendLine(" {");
            sb.AppendLine("     BaseModel baseModel = new BaseModel();");
            sb.AppendLine("");
            for (int i = 0; i < listFiles.Count; i++)
            {
                string className = listFiles[i].Replace(".cs", "");

                /* Get List Data*/
                /*start*/
                /*start*/
                /*start*/
                sb.AppendLine("     public  " + className + "Base " + className + "GetList()");
                sb.AppendLine("     {");
                sb.AppendLine("         var listBase = new " + className + "Base();");
                sb.AppendLine("         try");
                sb.AppendLine("         {");
                sb.AppendLine("             using (var context = new " + dbContext + "())");
                sb.AppendLine("             {");
                sb.AppendLine("                 listBase." + className + "List = context." + className + ".ToList();");
                sb.AppendLine("                 context.Dispose();");
                sb.AppendLine("             }");
                sb.AppendLine("             return listBase;");
                sb.AppendLine("         }");
                sb.AppendLine("         catch (Exception ex)");
                sb.AppendLine("         {");
                sb.AppendLine("             listBase.errorMessage = listBase.errorService+ ex.Message;");
                sb.AppendLine("             listBase.errorType = 1;");
                sb.AppendLine("         return listBase;");
                sb.AppendLine("         }");
                sb.AppendLine("     }");
                sb.AppendLine("");
                /*end*/
                /*end*/
            }
            sb.AppendLine(" }");
            sb.AppendLine("}");

            GenerateFile(projectName, nameSpace, nameSpace + "MasterService", "service", sb);
        }
        private static void GenerateObjectBaseModel(string projectName, string nameSpace, List<string> listFiles)
        {
            listFiles.AddRange(viewList);
            for (int i = 0; i < listFiles.Count; i++)
            {
                StringBuilder sb = new StringBuilder();
                string className = listFiles[i].Replace(".cs", "");
                sb.AppendLine("using System;");
                sb.AppendLine("using System.Collections.Generic;");
                sb.AppendLine("");
                sb.AppendLine("namespace " + projectName + ".DA.Models." + nameSpace);
                sb.AppendLine("{");
                sb.AppendLine("     public partial class " + className + "Base : BaseModel");
                sb.AppendLine("     {");
                sb.AppendLine("         public " + className + " " + className + " { get; set; }");
                sb.AppendLine("         public List<" + className + "> " + className + "List { get; set; }");
                sb.AppendLine("     }");
                sb.AppendLine("}");

                GenerateFile(projectName, nameSpace, className + "Base", "model", sb);
            }
        }
        private static void GenerateController(string projectName, string nameSpace, List<string> listFiles)
        {
            for (int i = 0; i < listFiles.Count; i++)
            {
                StringBuilder sb = new StringBuilder();
                string className = listFiles[i].Replace(".cs", "");
                sb.AppendLine("using System;");
                sb.AppendLine("using Microsoft.AspNetCore.Mvc;");
                sb.AppendLine("using " + projectName + ".DA.Service." + nameSpace + ";");
                sb.AppendLine("using " + projectName + ".DA.Models." + nameSpace + ";");
                sb.AppendLine("");
                sb.AppendLine("namespace " + projectName + ".Controllers");
                sb.AppendLine("{");
                sb.AppendLine(" public class " + className + "Controller : Controller");
                sb.AppendLine(" {");
                sb.AppendLine("     public IActionResult Index()");
                sb.AppendLine("     {");
                sb.AppendLine(@"         return View(""~/Views/" + nameSpace + @"/" + className + @".cshtml"");");
                sb.AppendLine("     }");
                sb.AppendLine("");

                /*get data list*/
                /*start*/
                if (viewList.Where(x => x.ToLower().Contains("vw" + className.ToLower())).ToList().Count > 0)
                {
                    string viewName = viewList.Where(x => x.ToLower().Contains("vw" + className.ToLower())).FirstOrDefault();
                    viewName = viewName.Replace(".cs", "");
                    sb.AppendLine(@"    [HttpGet]");
                    sb.AppendLine("     public IActionResult GetDataList()");
                    sb.AppendLine("     {");
                    sb.AppendLine("         var respon = new Respon();");
                    sb.AppendLine("         var result = new " + viewName + "Base();");
                    sb.AppendLine("         try");
                    sb.AppendLine("         {");
                    sb.AppendLine("             var service = new " + className + "Service();");
                    sb.AppendLine("             result = service." + viewName + "GetList();");
                    sb.AppendLine("             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;");
                    sb.AppendLine("         }");
                    sb.AppendLine("         catch (Exception ex)");
                    sb.AppendLine("         {");
                    sb.AppendLine("             respon.errorMessage = respon.errorController  + ex.Message;");
                    sb.AppendLine("             respon.errorType = 1;");
                    sb.AppendLine("         }");
                    sb.AppendLine("         return Ok(new { respon = respon, data = result." + viewName + "List });");
                    sb.AppendLine("     }");
                    sb.AppendLine("");
                    sb.AppendLine("");
                    /*end*/
                }
                else
                {
                    sb.AppendLine(@"    [HttpGet]");
                    sb.AppendLine("     public IActionResult GetDataList()");
                    sb.AppendLine("     {");
                    sb.AppendLine("         var respon = new Respon();");
                    sb.AppendLine("         var result = new " + className + "Base();");
                    sb.AppendLine("         try");
                    sb.AppendLine("         {");
                    sb.AppendLine("             var service = new " + className + "Service();");
                    sb.AppendLine("             result = service." + className + "GetList();");
                    sb.AppendLine("             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;");
                    sb.AppendLine("         }");
                    sb.AppendLine("         catch (Exception ex)");
                    sb.AppendLine("         {");
                    sb.AppendLine("             respon.errorMessage = respon.errorController  + ex.Message;");
                    sb.AppendLine("             respon.errorType = 1;");
                    sb.AppendLine("         }");
                    sb.AppendLine("         return Ok(new { respon = respon, data = result." + className + "List });");
                    sb.AppendLine("     }");
                    sb.AppendLine("");
                    sb.AppendLine("");
                    /*end*/
                }
                /*create*/
                /*start*/
                sb.AppendLine("     [HttpPost]");
                sb.AppendLine("     public IActionResult Create(" + className + " Post)");
                sb.AppendLine("     {");
                sb.AppendLine("         var respon = new Respon();");
                sb.AppendLine("         try");
                sb.AppendLine("         {");
                sb.AppendLine("             var service = new " + className + "Service();");
                sb.AppendLine("             respon.errorMessage = service." + className + "Create(Post);");
                sb.AppendLine("             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;");
                sb.AppendLine("         }");
                sb.AppendLine("         catch (Exception ex)");
                sb.AppendLine("         {");
                sb.AppendLine(@"            respon.errorMessage = respon.errorController  + ex.Message;");
                sb.AppendLine("             respon.errorType = 1;");
                sb.AppendLine("         }");
                sb.AppendLine("         return Ok(new { respon = respon });");
                sb.AppendLine("     }");
                sb.AppendLine("");
                sb.AppendLine("");
                /*end*/
                /*update*/
                /*start*/
                sb.AppendLine("     [HttpPost]");
                sb.AppendLine("     public IActionResult Update(" + className + " Post)");
                sb.AppendLine("     {");
                sb.AppendLine("         var respon = new Respon();");
                sb.AppendLine("         try");
                sb.AppendLine("         {");
                sb.AppendLine("             var service = new " + className + "Service();");
                sb.AppendLine("             respon.errorMessage = service." + className + "Update(Post);");
                sb.AppendLine("             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;");
                sb.AppendLine("         }");
                sb.AppendLine("         catch (Exception ex)");
                sb.AppendLine("         {");
                sb.AppendLine(@"            respon.errorMessage = respon.errorController  + ex.Message;");
                sb.AppendLine("             respon.errorType = 1;");
                sb.AppendLine("          }");
                sb.AppendLine("          return Ok(new { respon = respon });");
                sb.AppendLine("     }");
                sb.AppendLine("");
                sb.AppendLine("");
                /*end*/

                /*delete*/
                /*start*/
                sb.AppendLine("     [HttpGet]");
                sb.AppendLine("     public IActionResult Delete(" + className + " Post)");
                sb.AppendLine("     {");
                sb.AppendLine("         var respon = new Respon();");
                sb.AppendLine("         try");
                sb.AppendLine("         {");
                sb.AppendLine("             var service = new " + className + "Service();");
                sb.AppendLine("             respon.errorMessage = service." + className + "Delete(Post);");
                sb.AppendLine("             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;");
                sb.AppendLine("         }");
                sb.AppendLine("         catch (Exception ex)");
                sb.AppendLine("         {");
                sb.AppendLine(@"            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;");
                sb.AppendLine("             respon.errorType = 1;");
                sb.AppendLine("         }");
                sb.AppendLine("         return Ok(new { respon = respon });");
                sb.AppendLine("     }");
                /*end*/

                sb.AppendLine("  }");
                sb.AppendLine("}");

                GenerateFile(projectName, nameSpace, className + "Controllers", "controller", sb);
            }
        }
        private static void GenerateJavascript(string projectName, string nameSpace, List<string> listFiles)
        {
            for (int i = 0; i < listFiles.Count; i++)
            {
                #region Set Data
                string className = listFiles[i].Replace(".cs", "");

                CoumnOfTable coumnOfTable = new CoumnOfTable(className, connection);
                tableModels = coumnOfTable.tableModels;
                coumnOfTable = new CoumnOfTable("vw" + className, connection);
                vwTableModels = coumnOfTable.tableModels;
                #endregion


                StringBuilder sb = new StringBuilder();
                #region jquery document ready
                sb.AppendLine("var tbl" + className + "='#tbl" + className + "';");
                sb.AppendLine("jQuery(document).ready(function () {");
                sb.AppendLine(" Table.Init();");
                sb.AppendLine(" Table.LoadTable();");
                sb.AppendLine(" Helper.PanelHeaderShow();");
                sb.AppendLine(" Helper.InitButtons();");
                sb.AppendLine("");
                sb.AppendLine(" var form = document.querySelector('form#" + className + "Form');");
                sb.AppendLine(" form.addEventListener('submit', function (ev) {");
                sb.AppendLine("     ev.preventDefault();");
                sb.AppendLine("     if (Common.Form.HandleFormSubmit(form, FormValidation)) {");
                sb.AppendLine("         Form.Submit();");
                sb.AppendLine("     }");
                sb.AppendLine(" });");
                sb.AppendLine("});");
                sb.AppendLine("");
                #endregion

                #region Form
                sb.AppendLine("var Form = {");
                sb.AppendLine("    Submit: function () {");
                sb.AppendLine("     var data = {");
                foreach (var item in tableModels)
                {
                    string column = CamelToRegularWithoutSpace(item.ColumnName);
                    sb.AppendLine("                " + column + ": $('#" + column + "').val(),");
                }
                sb.AppendLine("                };");
                sb.AppendLine("     var result = Common.Form.Save('/" + className + "/Create', data);");
                sb.AppendLine("     if (Common.CheckError.Object(result)) {");
                sb.AppendLine("        Common.Alert.Success('Data  has been saved.');");
                sb.AppendLine("        Helper.PanelHeaderShow();");
                sb.AppendLine("        Table.LoadTable();");
                sb.AppendLine("      } else {");
                sb.AppendLine("        Common.Alert.Error(result.respon.errorMessage);");
                sb.AppendLine("      }");
                sb.AppendLine("    },");
                sb.AppendLine("    Delete: function (param) {");
                sb.AppendLine("     var result = Common.Form.Delete('/" + className + "/Delete', param);");
                sb.AppendLine("      if (Common.CheckError.Object(result)) {");
                sb.AppendLine("         Common.Alert.Success('Data  has been deleted.');");
                sb.AppendLine("         Table.LoadTable();");
                sb.AppendLine("       } else {");
                sb.AppendLine("         Common.Alert.Error(result.respon.errorMessage);");
                sb.AppendLine("       }");
                sb.AppendLine("    },");
                sb.AppendLine("    ClearForm: function () {");
                foreach (var item in tableModels)
                {
                    sb.AppendLine("      $('#" + CamelToRegularWithoutSpace(item.ColumnName) + "').val('');");

                }
                sb.AppendLine("    },");
                sb.AppendLine("}");
                sb.AppendLine("");
                #endregion

                #region Form Validation
                sb.AppendLine("var FormValidation = {");
                foreach (var item in tableModels.Where(x => x.ColumnNullable == "no"))
                {
                    sb.AppendLine("    " + CamelToRegularWithoutSpace(item.ColumnName) + ": {");
                    sb.AppendLine("     presence: true,");
                    sb.AppendLine("     },");
                }
                sb.AppendLine("}");
                sb.AppendLine("");
                #endregion

                #region Helper
                sb.AppendLine("var Helper = {");
                sb.AppendLine("     PanelHeaderShow: function () {");
                sb.AppendLine("       $('.pnlHeader').css('visibility', 'visible');");
                sb.AppendLine("       $('.pnlDetail').css('visibility', 'hidden');");
                sb.AppendLine("       $('.pnlHeader').show();");
                sb.AppendLine("       $('.pnlDetail').hide();");
                sb.AppendLine("     },");
                sb.AppendLine("     PanelDetailShow: function () {");
                sb.AppendLine("       $('.pnlHeader').css('visibility', 'hidden');");
                sb.AppendLine("       $('.pnlDetail').css('visibility', 'visible');");
                sb.AppendLine("       $('.pnlHeader').hide();");
                sb.AppendLine("       $('.pnlDetail').show();");
                sb.AppendLine("     },");
                sb.AppendLine("     InitButtons: function () {");
                sb.AppendLine("       $('.btnCancel').unbind().click(function () {");
                sb.AppendLine("          Helper.PanelHeaderShow();");
                sb.AppendLine("       });");
                sb.AppendLine("       $('.btnAddData').unbind().click(function () {");
                sb.AppendLine("           Form.ClearForm();");
                sb.AppendLine("           Helper.PanelDetailShow();");
                sb.AppendLine("        });");
                sb.AppendLine("     },");
                sb.AppendLine("}");
                sb.AppendLine("");
                #endregion

                #region Table
                sb.AppendLine("var Table = {");
                sb.AppendLine("       Init: function () {");
                sb.AppendLine("          Common.Table.InitClient(tbl" + className + ");");
                sb.AppendLine("       },");
                sb.AppendLine("       LoadData: function () {");
                sb.AppendLine("          return Common.GetData.Get('/" + className + "/GetDataList');");
                sb.AppendLine("       },");
                sb.AppendLine("       LoadTable: function () {");
                sb.AppendLine("          var data = Table.LoadData();");
                sb.AppendLine("          if (Common.CheckError.Object(data)){");
                sb.AppendLine("              var columns = [");
                sb.AppendLine("                   {");
                sb.AppendLine("                       render: function () {");
                sb.AppendLine(@"                         var str = '<div class=""btn-group"">';");
                sb.AppendLine(@"                         str += '<button type=""button"" class=""btn btn-primary btn-xl btn-icon btnEdit""  data-toggle=""tooltip"" data-placement=""top"" title=""edit row""><i class=""icofont icofont-pencil-alt-5""></i></button> &nbsp;&nbsp;';");
                sb.AppendLine(@"                         str += '<button type=""button"" class=""btn btn-danger btn-xl btn-icon btnRemove""  data-toggle=""tooltip"" data-placement=""top"" title=""remove row""><i class=""icofont icofont-trash""></i></button>';");
                sb.AppendLine(@"                         str += '</div>';");
                sb.AppendLine(@"                         return str;");
                sb.AppendLine("                       }},");

                if (vwTableModels.Count > 0)
                {
                    foreach (var item in vwTableModels)
                    {
                        if (item.ColumnType.ToLower().Contains("date"))
                        {
                            sb.AppendLine("                    {'data':'" + CamelToRegularWithoutSpaceLowerFirst(item.ColumnName) + "', render: function (data){ return Common.Format.Date(data);}},");
                        }
                        else
                        {
                            sb.AppendLine("                    {'data':'" + CamelToRegularWithoutSpaceLowerFirst(item.ColumnName) + "'},");
                        }

                    }
                }
                else
                {
                    foreach (var item in tableModels)
                    {
                        if (item.ColumnType.ToLower().Contains("date"))
                        {
                            sb.AppendLine("                    {'data':'" + CamelToRegularWithoutSpaceLowerFirst(item.ColumnName) + "', render: function (data){ return Common.Format.Date(data);}},");
                        }
                        else
                        {
                            sb.AppendLine("                    {'data':'" + CamelToRegularWithoutSpaceLowerFirst(item.ColumnName) + "'},");
                        }
                    }
                }

                sb.AppendLine("                   ];");
                sb.AppendLine("              var columnDefs = [];");
                sb.AppendLine("              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];");
                sb.AppendLine("              Common.Table.LoadTableClient(tbl" + className + ", data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);");
                sb.AppendLine("              $(tbl" + className + ").unbind();");
                sb.AppendLine("              $(tbl" + className + ").on('click', '.btnRemove', function (e) {");
                sb.AppendLine("                  var tb = $(tbl" + className + ").DataTable();");
                sb.AppendLine("                  var row = tb.row($(this).parents('tr')).data();");
                sb.AppendLine("                  var param={");

                foreach (var item in tableModels.Where(x => x.IsPK == true))
                {
                    sb.AppendLine("                        " + CamelToRegularWithoutSpace(item.ColumnName) + " : row." + CamelToRegularWithoutSpaceLowerFirst(item.ColumnName) + ",");
                }

                sb.AppendLine("                             };");
                sb.AppendLine("                  swal({");
                sb.AppendLine("                      title: 'Are you sure ? ',");
                sb.AppendLine("                      text: '',");
                sb.AppendLine("                      type: 'warning',");
                sb.AppendLine("                      showCancelButton: true,");
                sb.AppendLine("                      confirmButtonClass: 'btn-danger',");
                sb.AppendLine("                      confirmButtonText: 'Yes, delete it!',");
                sb.AppendLine("                      closeOnConfirm: false");
                sb.AppendLine("                      },");
                sb.AppendLine("                      function () {");
                sb.AppendLine("                          Form.Delete(param);");
                sb.AppendLine("                      });");
                sb.AppendLine("              });");
                sb.AppendLine("              $(tbl" + className + ").on('click', '.btnEdit', function (e) {");
                sb.AppendLine("                  var tb = $(tbl" + className + ").DataTable();");
                sb.AppendLine("                  var row = tb.row($(this).parents('tr')).data();");
                foreach (var item in tableModels)
                {
                    sb.AppendLine("                  $('#" + CamelToRegularWithoutSpace(item.ColumnName) + "').val(row." + CamelToRegularWithoutSpaceLowerFirst(item.ColumnName) + ");");
                }
                sb.AppendLine("                  Helper.PanelDetailShow();");
                sb.AppendLine("              });");
                sb.AppendLine("          } else {");
                sb.AppendLine("               Common.Alert.Error(data.respon.errorMessage);");
                sb.AppendLine("          }");
                sb.AppendLine("       }");
                sb.AppendLine("}");
                #endregion

                GenerateFile(projectName, nameSpace, className, "js", sb);
            }
        }
        private static void GenerateHtml(string projectName, string nameSpace, List<string> listFiles)
        {
            for (int i = 0; i < listFiles.Count; i++)
            {
                #region Set Data
                string className = listFiles[i].Replace(".cs", "");
                CoumnOfTable coumnOfTable = new CoumnOfTable(className, connection);
                tableModels = coumnOfTable.tableModels;
                coumnOfTable = new CoumnOfTable("vw" + className, connection);
                vwTableModels = coumnOfTable.tableModels;
                #endregion

                StringBuilder sb = new StringBuilder();
                #region Panel Header
                sb.AppendLine(@"<script src=""~/js/Common.js""></script>");
                sb.AppendLine(@"<script src=""~/js/" + nameSpace + @"/" + className + @".js""></script>");
                sb.AppendLine(@"<div class=""card pnlHeader"">");
                sb.AppendLine(@"    <div class=""card-header"">");
                sb.AppendLine(@"        <div class=""data-table-main icon-list-demo"">");
                sb.AppendLine(@"            <div class=""row"">");
                sb.AppendLine(@"                <div class=""col-sm-6 col-md-6 col-lg-6 outer-ellipsis"">");
                sb.AppendLine(@"                <i class=""icofont icofont-list""></i><h5>Master .....</h5>");
                sb.AppendLine(@"               </div>");
                sb.AppendLine(@"               <div class=""col-md-6 text-right"">");
                sb.AppendLine(@"                    <button class=""btn btn-primary btn-round btnAddData"">Add More</button>");
                sb.AppendLine(@"               </div>");
                sb.AppendLine(@"            </div>");
                sb.AppendLine(@"        </div>");
                /*end data-table-main*/
                sb.AppendLine(@"    </div>");
                /*end card-header*/
                sb.AppendLine(@"    <div class=""card-block"">");
                sb.AppendLine(@"        <div class=""dt-responsive table-responsive"">");
                sb.AppendLine(@"            <table id=""tbl" + className + @""" class=""table table-striped table-hover table-bordered nowrap"">");
                sb.AppendLine(@"                <thead>");
                sb.AppendLine(@"                    <tr>");
                sb.AppendLine(@"                        <th>Action</th>");

                if (vwTableModels.Count > 0)
                {
                    foreach (var item in vwTableModels)
                    {
                        sb.AppendLine(@"                        <th>" + CamelToRegularWithSpace(item.ColumnName) + "</th>");
                    }
                }
                else
                {
                    foreach (var item in tableModels)
                    {
                        sb.AppendLine(@"                        <th>" + CamelToRegularWithSpace(item.ColumnName) + "</th>");
                    }
                }

                sb.AppendLine(@"                    </tr>");
                sb.AppendLine(@"                </thead>");
                sb.AppendLine(@"            </table>");
                sb.AppendLine(@"        </div>");
                sb.AppendLine(@"    </div>");
                /*end card-block*/
                sb.AppendLine(@"</div>");
                /*end card pnlHeader*/
                sb.AppendLine("");
                #endregion

                #region Panel Detail
                sb.AppendLine(@"<div class=""card pnlDetail"">");
                sb.AppendLine(@"    <div class=""card-header"">");
                sb.AppendLine(@"        <div class=""data-table-main icon-list-demo"">");
                sb.AppendLine(@"            <div class=""row"">");
                sb.AppendLine(@"                <div class=""col-sm-6 col-md-6 col-lg-6 outer-ellipsis"">");
                sb.AppendLine(@"                    <i class=""icofont icofont-list""></i><h5>Form Input .....</h5>");
                sb.AppendLine(@"               </div>");
                sb.AppendLine(@"            </div>");
                sb.AppendLine(@"        </div>");
                sb.AppendLine(@"    </div>");
                /*end card-header*/
                sb.AppendLine(@"    <div class=""card-block"">");
                sb.AppendLine(@"        <form id=""" + className + @"Form"" method=""post"" novalidate="""">");

                foreach (var item in tableModels)
                {
                    string columnWithoutSpace = CamelToRegularWithoutSpace(item.ColumnName);
                    string columnWitSpace = CamelToRegularWithSpace(item.ColumnName);
                    sb.AppendLine(@"            <div class=""form-group row"">");
                    sb.AppendLine(@"                <label class=""col-sm-2 col-form-label"">" + columnWitSpace + "</label>");
                    sb.AppendLine(@"                <div class=""col-sm-10"">");
                    sb.AppendLine(@"                    <input type=""text"" class=""form-control""  name=""" + columnWithoutSpace + @""" id=""" + columnWithoutSpace + @""" placeholder=""Text Input " + columnWitSpace + @""">");
                    sb.AppendLine(@"                    <span class=""messages""></span>");
                    sb.AppendLine(@"                </div>");
                    sb.AppendLine(@"            </div>");
                }

                sb.AppendLine(@"            <div class=""form-group row"">");
                sb.AppendLine(@"                <label class=""col-sm-2 col-form-label""></label>");
                sb.AppendLine(@"                <div class=""col-sm-10"">");
                sb.AppendLine(@"                    <button type=""submit"" class=""btn btn-primary m-b-0 btnSubmit""><i class=""icofont icofont-save""></i> Submit</button> &nbsp;&nbsp;&nbsp;");
                sb.AppendLine(@"                    <button type=""button"" class=""btn btn-warning m-b-0 btnCancel""><i class=""icofont icofont-close-line-circled""></i> Cancel</button>");
                sb.AppendLine(@"                </div>");
                sb.AppendLine(@"            </div>");
                sb.AppendLine(@"        </form>");
                sb.AppendLine(@"    </div>");
                /*end card-block*/
                sb.AppendLine(@"</div>");
                /*end card pnlDetail*/
                #endregion

                GenerateFile(projectName, nameSpace, className, "html", sb);
            }
        }
        private static void GenerateFile(string projectName, string nameSpace, string fileName, string objectType, StringBuilder sb)
        {
            string curDir = Directory.GetCurrentDirectory();
            curDir = curDir.Replace(@"bin\Debug", @"Code Generated\" + nameSpace + @"\");
            curDir = curDir.Replace(@"bin\Release", @"Code Generated\" + nameSpace + @"\");
            bool objectTypeValid = true;
            string extFile = ".cs";
            if (objectType == "controller")
            {
                curDir += @"\Controller\";

            }
            else if (objectType == "service")
            {
                curDir += @"\Service\";
            }
            else if (objectType == "model")
            {
                curDir += @"\Models\";

            }
            else if (objectType == "js")
            {
                curDir += @"\js\";
                extFile = ".js";
            }
            else if (objectType == "html")
            {
                curDir += @"\Views\";
                extFile = ".cshtml";
            }
            else
            {
                objectTypeValid = false;
            }

            if (objectTypeValid)
            {
                if (!Directory.Exists(curDir))
                    Directory.CreateDirectory(curDir);

                using (StreamWriter sw = File.CreateText(curDir + fileName + extFile))
                {
                    sw.Write(sb);
                    sw.Dispose();
                }
            }
        }
        #endregion

        #region manage text
        public static string CamelToRegularWithSpace(string text)
        {
            string[] split = text.Split(',');
            //string[] result = new string[] { };

            List<string> result = new List<string>(); ;
            foreach (var item in split)
            {
                string[] splitText = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Regex.Replace(item, "(\\B[A-Z])", " $1")).Split(' ');
                string value = "";
                for (int j = 0; j < splitText.Length; j++)
                {
                    if (splitText[j].Length == 1)
                    {
                        value += splitText[j];
                    }
                    else
                    {
                        if (j == 0)
                            value += splitText[j] + "";
                        else
                            value += " " + splitText[j] + "";
                    }

                }
                result.Add(value);

            }
            string aaa = string.Join(",", result);
            return aaa;
        }
        public static string CamelToRegularWithoutSpace(string text)
        {
            string[] split = text.Split(',');
            //string[] result = new string[] { };
            int i = 0;
            List<string> result = new List<string>(); ;
            foreach (var item in split)
            {
                string[] splitText = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Regex.Replace(item, "(\\B[A-Z])", " $1")).Split(' ');
                string value = "";
                foreach (var a in splitText)
                {
                    if (a.Length == 1)
                        value += a;
                    else
                        value += "" + a + "";
                }
                result.Add(value);
                i++;
            }
            string aaa = string.Join(",", result);
            return aaa;
        }

        public static string CamelToRegularWithoutSpaceLowerFirst(string text)
        {
            string[] split = text.Split(',');
            //string[] result = new string[] { };

            List<string> result = new List<string>(); ;
            foreach (var item in split)
            {
                string[] splitText = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Regex.Replace(item, "(\\B[A-Z])", " $1")).Split(' ');
                string value = "";
                for (int j = 0; j < splitText.Length; j++)
                {
                    if (splitText[j].Length == 1)
                    {
                        value += splitText[j].ToLower();
                    }
                    else
                    {
                        if (j == 0)
                            value += splitText[j].ToLower() + "";
                        else
                            value += "" + splitText[j] + "";
                    }

                }
                result.Add(value);
            }
            string aaa = string.Join(",", result);

            return aaa;
        }
        #endregion
    }

    #region Get data table from database
    public class TableModel
    {
        public string ColumnName { get; set; }
        public string ColumnType { get; set; }
        public string ColumnNullable { get; set; }
        public bool IsPK { get; set; }
    }
    public class CoumnOfTable
    {
        public List<TableModel> tableModels = new List<TableModel>();
        public CoumnOfTable(string tableName, SqlConnection con)
        {
            tableModels = GetColumns(tableName, con);
        }

        private List<TableModel> GetColumns(string tableName, SqlConnection con)
        {
            DataTable dataTable = new DataTable();
            List<TableModel> tableModels = new List<TableModel>();


            string queryString = "Exec sp_help '" + tableName + "'";
            SqlCommand command = new SqlCommand(queryString, con);
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet dataSet = new DataSet();

            try
            {
                da.Fill(dataSet);
                if (dataSet.Tables[1].Rows.Count > 0)
                {
                    List<string> pkList = new List<string>();
                    if (tableName.Substring(0, 2).ToString().ToLower() != "vw")
                    {
                        foreach (DataRow pk in dataSet.Tables[6].Rows)
                        {
                            if (pk["constraint_type"].ToString().Contains("PRIMARY KEY"))
                            {
                                string pkID = pk["constraint_keys"].ToString();
                                pkList = pkID.Split(',').ToList();
                            }
                        }
                    }
                    foreach (DataRow row in dataSet.Tables[1].Rows)
                    {
                        string colName = row["Column_name"].ToString();
                        string pkID = pkList.Where(x => x.TrimStart().TrimEnd() == colName).FirstOrDefault();
                        tableModels.Add(new TableModel
                        {
                            ColumnName = colName,
                            ColumnType = row["type"].ToString(),
                            ColumnNullable = row["Nullable"].ToString(),
                            IsPK = pkID != null ? true : false
                        });
                    }
                    da.Dispose();
                    dataSet.Dispose();
                    dataTable.Dispose();
                }
                return tableModels;
            }
            catch (Exception)
            {
                return tableModels;
            }
            finally
            {
                command.Connection.Close();
                da.Dispose();
                dataSet.Dispose();
            }
        }
    }
    #endregion
}
