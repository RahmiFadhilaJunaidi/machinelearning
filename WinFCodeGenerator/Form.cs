﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WinFCodeGenerator
{
    public partial class Form : System.Windows.Forms.Form
    {
        protected SqlConnection sqlCon;
        public Form()
        {
            InitializeComponent();
            BindData();
        }
        private void BindData()
        {
            string currentDir = Directory.GetCurrentDirectory();
            currentDir = currentDir.Replace(@"bin\Debug", @"Data\");
            currentDir = currentDir.Replace(@"bin\Release", @"Data\");
            currentDir += "DatabaseConfig.json";
            string Json = File.ReadAllText(currentDir).ToString();

            JObject jObject = JObject.Parse(Json);
            /* bind server list*/
            JArray array = (JArray)jObject["Server"];
            List<string> serverList = new List<string>();
            foreach (var item in array)
            {
                serverList.Add(item["Name"].ToString());
            }
            foreach (var option in serverList)
            {
                cbServerName.Items.Add(option);
            }
            /* bind server list*/

            /* bind database list*/
            serverList = new List<string>();
            array = (JArray)jObject["Database"];
            foreach (var item in array)
            {
                serverList.Add(item["Name"].ToString());
            }
            foreach (var option in serverList)
            {
                cbDatabaseName.Items.Add(option);
            }
            /* bind server list*/
        }
        
        #region Button settings
        private void btnCheckConnection_Click(object sender, EventArgs e)
        {

            if (cbServerName.SelectedItem == null || cbDatabaseName.SelectedItem == null)
            {
                MessageBox.Show("Please select dropdown list required(*).");
            }
            else
            {
                if (!cbIsWinAuth.Checked && (string.IsNullOrEmpty(tbUserID.Text.TrimStart().TrimEnd()) || string.IsNullOrEmpty(tbPassword.Text.TrimStart().TrimEnd())))
                {
                    MessageBox.Show("Please fill User ID and Password!");
                }
                else
                {
                    string conStr = "";
                    if (cbIsWinAuth.Checked)
                        conStr = "Data Source=" + cbServerName.SelectedItem + ";Initial Catalog=" + cbDatabaseName.SelectedItem + ";Trusted_Connection=True";
                    else
                        conStr = "Data Source=" + cbServerName.SelectedItem + ";Initial Catalog=" + cbDatabaseName.SelectedItem + ";uid=" + tbUserID.Text.TrimStart().TrimEnd() + ";pwd=" + tbPassword.Text.TrimStart().TrimEnd() + ";Pooling=true";
                    try
                    {
                        sqlCon = new SqlConnection(conStr);
                        sqlCon.Open();
                        MessageBox.Show("Connection Success.");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Connection Failed: " + ex.Message);
                    }
                }
            }
        }
        private void btnGenerateCode_Click(object sender, EventArgs e)
        {
            if (sqlCon == null)
            {
                MessageBox.Show("Please check connection first!");
            }
            else
            {
                string projectName, dbContext, module, objectName, objectPath;
                projectName = tbProjectName.Text.TrimStart().TrimEnd();
                dbContext = tbDBContext.Text.TrimStart().TrimEnd();
                module = tbModule.Text.TrimStart().TrimEnd();
                objectName = tbObjectName.Text.TrimStart().TrimEnd();
                objectPath = tbObjectPath.Text.TrimStart().TrimEnd();

                /* validation code generate form */
                if (string.IsNullOrEmpty(projectName) || string.IsNullOrEmpty(dbContext) || string.IsNullOrEmpty(module) || string.IsNullOrEmpty(objectPath))
                {
                    MessageBox.Show("Please fill textbox required(*)");
                }
                else
                {
                    string result = "Generate is complate.";
                    try
                    {
                        CodeGenerated codeGenerated = new CodeGenerated(sqlCon, projectName, module, objectName, dbContext, objectPath);
                    }
                    catch (Exception ex)
                    {

                        result = "Generate is failur. Error : " + ex.Message;
                    }
                    finally
                    {
                        MessageBox.Show(result);
                    }
                }
            }
        }
        private void cbIsWinAuth_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbIsWinAuth.Checked)
            {
                label2.Text = "User ID*";
                label3.Text = "Password*";
            }
            else
            {
                label2.Text = "User ID";
                label3.Text = "Password";
            }

        }
        #endregion
    }
}
