﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;

namespace  DatabaseAccess
{
    public class Repositories : IRepositories
    {
        private string procedureName;
        private string connectionName;
        public Repositories(string procName, string conName)
        {
            procedureName = procName;
            connectionName = conName;
        }

        #region Create Transaction
        public async Task<ResponModel> CreateTransactionByProcAsync(ParameterModel param)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            var respon = new ResponModel();
            try
            {
                respon = await repo.CreateTransactionByProcAsync(procedureName, param);
            }
            catch (Exception ex)
            {

                respon.errorMessage = ex.Message;
                respon.errorType = 1;
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public ResponModel CreateTransactionByProc(ParameterModel param)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            var respon = new ResponModel();
            try
            {
                respon = repo.CreateTransactionByProc(procedureName, param);
            }
            catch (Exception ex)
            {

                respon.errorMessage = ex.Message;
                respon.errorType = 1;
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public ResponModel CreateTransactionByProcToSingle(ParameterModel param)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            var respon = new ResponModel();
            try
            {
                respon = repo.CreateTransactionByProcToSingle(procedureName, param);
            }
            catch (Exception ex)
            {

                respon.errorMessage = ex.Message;
                respon.errorType = 1;
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public void CreateTransactionByQuery(string query)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            try
            {
                CreateTransactionByQuery(query);
            }
            catch (Exception)
            {
            }
            finally
            {
                context.Dispose();
            }
        }
        #endregion


        #region GetData
        public async Task<List<Dictionary<string, object>>> GetDataByProcDicAsync(ParameterModel param)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            var respon = new List<Dictionary<string, object>>();
            try
            {
                respon = await repo.GetDataByProcDicAsync(procedureName, param);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> obj = new Dictionary<string, object>();
                obj.Add(AppVars.ErrorMessage, ex.Message);
                respon.Add(obj);
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public List<Dictionary<string, object>> GetDataByProcDic(ParameterModel param)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            var respon = new List<Dictionary<string, object>>();
            try
            {
                respon = repo.GetDataByProcDic(procedureName, param);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> obj = new Dictionary<string, object>();
                obj.Add(AppVars.ErrorMessage, ex.Message);
                respon.Add(obj);
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public async Task<ResponModel> GetDataByProcAsync(ParameterModel param)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            var respon = new ResponModel();
            try
            {
                respon = await repo.GetDataByProcAsync(procedureName, param);
            }
            catch (Exception ex)
            {
                respon.errorMessage = ex.Message;
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public ResponModel GetDataByProc(ParameterModel param)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            var respon = new ResponModel();
            try
            {
                respon = repo.GetDataByProc(procedureName, param);
            }
            catch (Exception ex)
            {
                respon.errorMessage = ex.Message;
                respon.errorType = 1;
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public List<Dictionary<string, object>> GetDataByProcToDynamicList(ParameterModel param)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            var respon = new List<Dictionary<string, object>>();
            try
            {
                respon = repo.GetDataByProcToDynamicList(procedureName, param);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> obj = new Dictionary<string, object>();
                obj.Add(AppVars.ErrorMessage, ex.Message);
                respon.Add(obj);
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }

        public ResponModel GetDataByProcToDataTable(ParameterModel param)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            var respon = new ResponModel();
            try
            {
                respon.dt = repo.GetDataByProcToDataTable(procedureName, param);
                respon.errorType = 0;
            }
            catch (Exception ex)
            {
                respon.errorMessage = ex.Message;
                respon.errorType = 1;
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public async Task<ResponModel> GetDataByQuerModelAsync(string query)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            var respon = new ResponModel();
            try
            {
                respon = await repo.GetDataByQuerModelAsync(query);
            }
            catch (Exception ex)
            {
                respon.errorMessage = ex.Message;
                respon.errorType = 1;
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public ResponModel GetDataByQuerModel(string query)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            var respon = new ResponModel();
            try
            {
                respon = repo.GetDataByQuerModel(query);
            }
            catch (Exception ex)
            {
                respon.errorMessage = ex.Message;
                respon.errorType = 1;
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public async Task<string> GetDataByQueryAsync(string query)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            string respon = "";
            try
            {
                respon = await repo.GetDataByQueryAsync(query);
            }
            catch (Exception)
            {
                respon = "";
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public string GetDataByQuery(string query)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            string respon = "";
            try
            {
                respon = repo.GetDataByQuery(query);
            }
            catch (Exception)
            {
                respon = "";
            }

            finally
            {
                context.Dispose();
            }
            return respon;
        }

        #endregion


        #region  Get Count Data
        public async Task<int> CountDataByProcAsync(ParameterModel param)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            int respon = 0;
            try
            {
                respon = await repo.CountDataByProcAsync(procedureName, param);
            }
            catch (Exception)
            {
                respon = 0;
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public int CountDataByProc(ParameterModel param)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            int respon = 0;
            try
            {
                respon = repo.CountDataByProc(procedureName, param);
            }
            catch (Exception)
            {
                respon = 0;
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public async Task<int> CountDataByQueryAsync(string query)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            int respon = 0;
            try
            {
                respon = await repo.CountDataByQueryAsync(query);
            }
            catch (Exception)
            {
                respon = 0;
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        public int CountDataByQuery(string query)
        {
            var context = new DbContext(Helper.GetConnection(connectionName));
            var repo = new Repository(context);
            int respon = 0;
            try
            {
                respon = repo.CountDataByQuery(query);
            }
            catch (Exception)
            {
                respon = 0;
            }
            finally
            {
                context.Dispose();
            }
            return respon;
        }
        #endregion


    }
}

