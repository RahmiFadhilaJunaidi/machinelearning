﻿using System;
using System.Collections.Generic;
using System.Data;

namespace  DatabaseAccess
{
    public abstract class BaseRepository<T> where T : new()
    {
        DbContext _context;
        public BaseRepository(DbContext context)
        {
            _context = context;
        }

        protected DbContext Context
        {
            get
            {
                return this._context;
            }
        }

        protected int CountTransaction(IDbCommand command)
        {
            using (var record = command.ExecuteReader())
            {
                int result = 0;
                while (record.Read())
                {
                    result = (int)record[0];
                }
                return result;
            }
        }
        protected IList<T> ReadTransaction(IDbCommand command)
        {
            using (var record = command.ExecuteReader())
            {
                List<T> items = new List<T>();
                while (record.Read())
                {
                    items.Add(Map<T>(record));
                }
                return items;
            }
        }

        protected T Map<T>(IDataRecord record)
        {
            var objT = Activator.CreateInstance<T>();
            foreach (var property in typeof(T).GetProperties())
            {
                if (record.HasColumn(property.Name) && !record.IsDBNull(record.GetOrdinal(property.Name)))
                    property.SetValue(objT, record[property.Name]);
            }
            return objT;

        }

        protected int WriteTransaction(IDbCommand command)
        {
            int returnVal = 0;

            using (var record = command.ExecuteReader())
            {
                while (record.Read())
                {
                    returnVal = Convert.ToInt32(record[0]);
                }
            }

            return returnVal;
        }
    }
}
