﻿using System.Data;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace  DatabaseAccess
{

    public class Repository : BaseRepository<ResponModel>
    {
        #region Contructor
        private DbContext _context;
        public Repository(DbContext context) : base(context)
        {
            _context = context;
        }
        #endregion

        #region Create Transaction
        /*public method*/
        public async Task<ResponModel> CreateTransactionByProcAsync(string ProcedureName, ParameterModel param)
        {
            return await pCreateTransactionByProcAsync(ProcedureName, param);
        }
        public ResponModel CreateTransactionByProc(string ProcedureName, ParameterModel param)
        {
            return pCreateTransactionByProc(ProcedureName, param);
        }
        public ResponModel CreateTransactionByProcToSingle(string ProcedureName, ParameterModel param)
        {
            return pCreateTransactionByProcToSingle(ProcedureName, param);
        }
        public void CreateTransactionByQuery(string query)
        {
            pCreateTransactionByQuery(query);
        }

        /*private method*/
        private async Task<ResponModel> pCreateTransactionByProcAsync(string ProcedureName, ParameterModel param)
        {
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = ProcedureName;
                command.Parameters.Clear();
                for (int i = 0; i < param.parameter.Length; i++)
                {
                    command.Parameters.Add(command.CreateParameter(param.parameter[i], param.value[i]));
                }
                ResponModel.dt = command.ExecuteReader().ToDataTable();
            }
            return ResponModel;
        }
        private ResponModel pCreateTransactionByProc(string ProcedureName, ParameterModel param)
        {
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = ProcedureName;
                command.Parameters.Clear();
                for (int i = 0; i < param.parameter.Length; i++)
                {
                    command.Parameters.Add(command.CreateParameter(param.parameter[i], param.value[i]));
                }
                ResponModel.dt = command.ExecuteReader().ToDataTable();
            }
            return ResponModel;
        }
        private ResponModel pCreateTransactionByProcToSingle(string ProcedureName, ParameterModel param)
        {
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = ProcedureName;
                command.Parameters.Clear();
                for (int i = 0; i < param.parameter.Length; i++)
                {
                    command.Parameters.Add(command.CreateParameter(param.parameter[i], param.value[i]));
                }
                ResponModel = command.ExecuteReader().GetItem<ResponModel>();
            }
            return ResponModel;
        }
        private void pCreateTransactionByQuery(string query)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.Text;
                command.CommandText = query;
                this.WriteTransaction(command);
            }
        }


        #endregion


        #region GetData
        /* public method*/
        public async Task<List<Dictionary<string, object>>> GetDataByProcDicAsync(string ProcedureName, ParameterModel param)
        {
            return await pGetDataByProcDicAsync(ProcedureName, param);
        }
        public List<Dictionary<string, object>> GetDataByProcDic(string ProcedureName, ParameterModel param)
        {
            return pGetDataByProcDic(ProcedureName, param);
        }
        public async Task<ResponModel> GetDataByProcAsync(string ProcedureName, ParameterModel param)
        {
            return await pGetDataByProcAsync(ProcedureName, param);
        }
        public ResponModel GetDataByProc(string ProcedureName, ParameterModel param)
        {
            return pGetDataByProc(ProcedureName, param);
        }
        public List<Dictionary<string, object>> GetDataByProcToDynamicList(string ProcedureName, ParameterModel param)
        {
            return pGetDataByProcToDynamicList(ProcedureName, param);
        }
        public DataTable GetDataByProcToDataTable(string ProcedureName, ParameterModel param)
        {
            return pGetDataByProcToDataTable(ProcedureName, param);
        }
        public List<Dictionary<string, object>> GetDataByProcToDynamicList(string query)
        {
            return pGetDataByProcToDynamicList(query);
        }
        public async Task<ResponModel> GetDataByQuerModelAsync(string query)
        {
            return await pGetDataByQuerModelAsync(query);
        }
        public ResponModel GetDataByQuerModel(string query)
        {
            return pGetDataByQuerModel(query);
        }
        public async Task<string> GetDataByQueryAsync(string query)
        {
            return await pGetDataByQueryAsync(query);
        }
        public string GetDataByQuery(string query)
        {
            return pGetDataByQuery(query);
        }

        /*private method*/
        private async Task<List<Dictionary<string, object>>> pGetDataByProcDicAsync(string ProcedureName, ParameterModel param)
        {
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = ProcedureName;
                command.Parameters.Clear();
                for (int i = 0; i < param.parameter.Length; i++)
                {
                    command.Parameters.Add(command.CreateParameter(param.parameter[i], param.value[i]));
                }

                return command.ExecuteReader().ToDictionaryList();
            }
        }
        private List<Dictionary<string, object>> pGetDataByProcDic(string ProcedureName, ParameterModel param)
        {
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = ProcedureName;
                command.Parameters.Clear();
                for (int i = 0; i < param.parameter.Length; i++)
                {
                    command.Parameters.Add(command.CreateParameter(param.parameter[i], param.value[i]));
                }
                return command.ExecuteReader().ToDictionaryList();
            }
        }
        private async Task<ResponModel> pGetDataByProcAsync(string ProcedureName, ParameterModel param)
        {
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = ProcedureName;
                command.Parameters.Clear();
                for (int i = 0; i < param.parameter.Length; i++)
                {
                    command.Parameters.Add(command.CreateParameter(param.parameter[i], param.value[i]));
                }
                ResponModel.dt = command.ExecuteReader().ToDataTable();
            }
            return ResponModel;
        }
        private ResponModel pGetDataByProc(string ProcedureName, ParameterModel param)
        {
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = ProcedureName;
                command.Parameters.Clear();
                for (int i = 0; i < param.parameter.Length; i++)
                {
                    command.Parameters.Add(command.CreateParameter(param.parameter[i], param.value[i]));
                }
                ResponModel.dt = command.ExecuteReader().ToDataTable();
            }
            return ResponModel;
        }
        private List<Dictionary<string, object>> pGetDataByProcToDynamicList(string ProcedureName, ParameterModel param)
        {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = ProcedureName;
                command.Parameters.Clear();
                for (int i = 0; i < param.parameter.Length; i++)
                {
                    command.Parameters.Add(command.CreateParameter(param.parameter[i], param.value[i]));
                }
                result = command.ExecuteReader().ToDictionaryList();
            }
            return result;
        }
        private DataTable pGetDataByProcToDataTable(string ProcedureName, ParameterModel param)
        {
            DataTable result = new DataTable();
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = ProcedureName;
                command.Parameters.Clear();
                for (int i = 0; i < param.parameter.Length; i++)
                {
                    command.Parameters.Add(command.CreateParameter(param.parameter[i], param.value[i]));
                }
                result = command.ExecuteReader().ToDataTable();
            }
            return result;
        }
        private List<Dictionary<string, object>> pGetDataByProcToDynamicList(string query)
        {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.Text;
                command.CommandText = query;
                command.Parameters.Clear();
                result = command.ExecuteReader().ToDictionaryList();
            }
            return result;
        }
        private async Task<ResponModel> pGetDataByQuerModelAsync(string query)
        {
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {

                command.CommandType = CommandType.Text;
                command.CommandText = query;
                ResponModel.dt = command.ExecuteReader().ToDataTable();
            }
            return ResponModel;
        }
        private ResponModel pGetDataByQuerModel(string query)
        {
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {

                command.CommandType = CommandType.Text;
                command.CommandText = query;
                ResponModel.dt = command.ExecuteReader().ToDataTable();
            }
            return ResponModel;
        }
        private async Task<string> pGetDataByQueryAsync(string query)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.Text;
                command.CommandText = query;

                return command.ExecuteReader().ToListString();
            }

        }
        private string pGetDataByQuery(string query)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.Text;
                command.CommandText = query;

                return command.ExecuteReader().ToRowString();
            }

        }
        #endregion

        #region  Get Count Data
        /* public method*/
        public async Task<int> CountDataByProcAsync(string procedureName, ParameterModel param)
        {
            return await pCountDataByProcAsync(procedureName, param);
        }
        public int CountDataByProc(string procedureName, ParameterModel param)
        {
            return pCountDataByProc(procedureName, param);
        }
        public async Task<int> CountDataByQueryAsync(string query)
        {
            return await pCountDataByQueryAsync(query);
        }
        public int CountDataByQuery(string query)
        {
            return pCountDataByQuery(query);
        }


        /* private method*/
        private async Task<int> pCountDataByProcAsync(string procedureName, ParameterModel param)
        {

            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = procedureName;
                command.Parameters.Clear();
                for (int i = 0; i < param.parameter.Length; i++)
                {
                    command.Parameters.Add(command.CreateParameter(param.parameter[i], param.value[i]));
                }
                return this.CountTransaction(command);
            }

        }
        private int pCountDataByProc(string procedureName, ParameterModel param)
        {

            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = procedureName;
                command.Parameters.Clear();
                for (int i = 0; i < param.parameter.Length; i++)
                {
                    command.Parameters.Add(command.CreateParameter(param.parameter[i], param.value[i]));
                }
                return this.CountTransaction(command);
            }

        }
        private async Task<int> pCountDataByQueryAsync(string query)
        {
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {

                command.CommandType = CommandType.Text;
                command.CommandText = query;
                return this.CountTransaction(command);
            }
        }
        private int pCountDataByQuery(string query)
        {
            ResponModel ResponModel = new ResponModel();
            using (var command = _context.CreateCommand())
            {

                command.CommandType = CommandType.Text;
                command.CommandText = query;
                return this.CountTransaction(command);
            }
        }

        #endregion
    }
}
