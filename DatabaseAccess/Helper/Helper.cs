﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace  DatabaseAccess
{
    public class Helper
    {
        public static string XmlSerializer<T>(T dataToSerialize)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            StringWriter sw = new StringWriter();
            using (XmlWriter writer = XmlWriter.Create(sw, settings))
            {
                var xmlns = new XmlSerializerNamespaces();
                xmlns.Add(string.Empty, string.Empty);

                xsSubmit.Serialize(writer, dataToSerialize, xmlns);
                return sw.ToString();
            }
        }
        internal static IDbConnectionFactory GetConnection(string connectinName)
        {
            return new DbConnectionFactory(connectinName);
        }
        internal static IDbConnectionFactory GetConnection2(object connectinName)
        {
            throw new NotImplementedException();
        }
        public static T XMLDeserializer<T>(string xmlText)
        {
            var stringReader = new System.IO.StringReader(xmlText);
            var serializer = new XmlSerializer(typeof(T));
            return (T)serializer.Deserialize(stringReader);
        }
    }
}
