﻿
namespace DatabaseAccess
{
    public static class AppVars
    {
        /* Error Type */
        public static string ErrorExecQuery { get { return "Please check connection or functios"; } }
        public static string ErrorMessage { get { return "ErrorMessage"; } }
        public static string ErrorType { get { return "ErrorType"; } }

        /* Type of parameters */
        public static string vType { get { return "@vType"; } }
        public static string vWhereCaluse { get { return "@vWhereClause"; } }
        public static string vOrderBy { get { return "@vOrderBy"; } }
        public static string vRowSkip { get { return "@vRowSkip"; } }
        public static string vPageSize { get { return "@vPageSize"; } }
        public static string vXml { get { return "@vXml"; } }
    }
}
