﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Dynamic;
using System.Text.RegularExpressions;
using System.Globalization;
using Newtonsoft.Json;

namespace  DatabaseAccess
{
    public static class DataReaderExtensions
    {
        #region Dynamic Return
        public static List<dynamic> ToDynamicList(this IDataReader dr)
        {
            DataTable dt = new DataTable();
            var dynamicDt = new List<dynamic>();

            try
            {
                dt.Load(dr);
                foreach (DataRow row in dt.Rows)
                {
                    dynamic dyn = new ExpandoObject();
                    dynamicDt.Add(dyn);
                    foreach (DataColumn column in dt.Columns)
                    {
                        var dic = (IDictionary<string, object>)dyn;
                        dic[column.ColumnName] = row[column];
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return dynamicDt;
        }
        public static List<dynamic> ToDynamicList(this DataTable dt)
        {
            var dynamicDt = new List<dynamic>();
            foreach (DataRow row in dt.Rows)
            {
                dynamic dyn = new ExpandoObject();
                dynamicDt.Add(dyn);
                foreach (DataColumn column in dt.Columns)
                {
                    var dic = (IDictionary<string, object>)dyn;
                    dic[column.ColumnName] = row[column];
                }
            }
            return dynamicDt;
        }
        #endregion

        #region List dictionary return
        public static List<Dictionary<string, object>> ToDictionaryList(this IDataReader dr)
        {
            DataTable dt = new DataTable();
            List<Dictionary<string, object>> dynamicDt = new List<Dictionary<string, object>>();
            dt.Load(dr);
            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> data = new Dictionary<string, object>();
                foreach (DataColumn column in dt.Columns)
                {
                    data.Add(column.ColumnName, row[column.ColumnName].ToString());
                }
                dynamicDt.Add(data);
            }
            return dynamicDt;
        }
        public static Dictionary<string, object> ToDictionary(this IDataReader dr)
        {
            DataTable dt = new DataTable();
            var data = new Dictionary<string, object>();
            try
            {
                dt.Load(dr);
                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn column in dt.Columns)
                    {
                        data.Add(column.ColumnName, row[column.ColumnName].ToString());
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return data;

        }
        #endregion

        #region Datatable
        public static DataTable ToDataTable(this IDataReader dr)
        {
            DataTable dt = new DataTable();
            dt.Load(dr);
            return dt;
        }

        public static List<T> DataTableToObjectList<T>(DataTable objectData)
        {
            List<T> data = new List<T>();
            data = JsonConvert.DeserializeObject<List<T>>(JsonConvert.SerializeObject(objectData));
            return data;
        }

        public static T DictionaryToObject<T>(Dictionary<string, object> objectData)
        {
            T data;
            data = JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(objectData));
            return data;
        }

        #endregion

        #region String Return
        public static string ToRowString(this IDataReader dr)
        {
            DataTable dt = new DataTable();
            dt.Load(dr);
            return dt.Rows[0][0].ToString();
        }
        public static string ToListString(this IDataReader dr)
        {
            DataTable dt = new DataTable();
            string result = "";

            try
            {
                dt.Load(dr);
                List<string> list = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i]["result"].ToString());
                }
                result = string.Join(",", list);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        #endregion

        #region Generic return
        public static List<T> ToGenericList<T>(this IDataReader dr)
        {
            List<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (!object.Equals(dr[prop.Name], DBNull.Value))
                    {
                        prop.SetValue(obj, dr[prop.Name], null);
                    }
                }
                list.Add(obj);
            }
            return list;
        }
        public static List<T> ToGenericList<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static List<T> ToGenericList<T>(List<Dictionary<string, object>> objectData)
        {
            List<T> data = new List<T>();
            foreach (Dictionary<string, object> objDic in objectData)
            {
                T item = GetItemObject<T>(objDic);
                data.Add(item);
            }
            return data;
        }
        public static T ToGeneric<T>(DataTable dt)
        {
            T data = Activator.CreateInstance<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data = item;
            }
            return data;
        }
        #endregion

        #region Common methode
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name.ToLower() == column.ColumnName.ToLower())
                    {
                        var data = new object();
                        if (dr[column.ColumnName] == DBNull.Value)
                            data = null;
                        else
                            data = dr[column.ColumnName];

                        pro.SetValue(obj, data, null);
                        goto nextToDo;
                    }
                }
            nextToDo:
                continue;
            }

            return obj;
        }
        public static T GetItem<T>(this IDataReader dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();
            foreach (DataColumn column in dr.ToDataTable().Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name.ToLower() == column.ColumnName.ToLower())
                    {
                        var data = new object();
                        if (dr[column.ColumnName] == DBNull.Value)
                            data = null;
                        else
                            data = dr[column.ColumnName];

                        pro.SetValue(obj, data, null);
                        goto nextToDo;
                    }
                }
            nextToDo:
                continue;
            }

            return obj;
        }
        public static T GetItemObject<T>(Dictionary<string, object> objData)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();
            foreach (var item in objData)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    pro.SetValue(obj, item.Value);
                }
            }
            return obj;
        }
        public static string CamelToRegular(string text)
        {
            string[] split = text.Split(',');
            int i = 0;
            List<string> result = new List<string>(); ;
            foreach (var item in split)
            {
                string[] splitText = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Regex.Replace(item, "(\\B[A-Z])", " $1")).Split(' ');
                string value = "";
                foreach (var a in splitText)
                {
                    if (a.Length == 1)
                        value += a;
                    else
                        value += " " + a + " ";
                }
                result.Add(value);
                i++;
            }
            string aaa = string.Join(",", result);

            return aaa;
        }
        #endregion

    }
}
