﻿using System.Data;

namespace  DatabaseAccess
{
    public interface IDbConnectionFactory
    {
        IDbConnection Create();
    }
}
