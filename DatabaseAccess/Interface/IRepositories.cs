﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace  DatabaseAccess
{
    public interface IRepositories
    {
        #region Create Transaction
        ResponModel CreateTransactionByProc(ParameterModel param);
        ResponModel CreateTransactionByProcToSingle(ParameterModel param);
        Task<ResponModel> CreateTransactionByProcAsync(ParameterModel param);
        void CreateTransactionByQuery(string query);
        #endregion

        #region GetData
        Task<List<Dictionary<string, object>>> GetDataByProcDicAsync(ParameterModel param);
        List<Dictionary<string, object>> GetDataByProcDic(ParameterModel param);
        Task<ResponModel> GetDataByProcAsync(ParameterModel param);
        ResponModel GetDataByProc(ParameterModel param);
        List<Dictionary<string, object>> GetDataByProcToDynamicList(ParameterModel param);
        ResponModel GetDataByProcToDataTable(ParameterModel param);
        Task<ResponModel> GetDataByQuerModelAsync(string query);
        ResponModel GetDataByQuerModel(string query);
        Task<string> GetDataByQueryAsync(string query);
        string GetDataByQuery(string query);
        #endregion

        #region Get Count Data
        Task<int> CountDataByProcAsync(ParameterModel param);
        int CountDataByProc(ParameterModel param);
        Task<int> CountDataByQueryAsync(string query);
        int CountDataByQuery(string query);
        #endregion
    }
}
