﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace DatabaseAccess
{

    public class AppConfig
    {
        public readonly string _connectionString = string.Empty;
        public readonly string _providerName = string.Empty;
        
          public AppConfig(string connectionName)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            _connectionString = configuration.GetConnectionString(connectionName);
            _providerName = configuration.GetConnectionString("SQLProvider");
        }

        public string ConnectionString
        {
            get => _connectionString;
        }
        public string ProviderName
        {
            get => _providerName;
        }

    }
}
