﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace  DatabaseAccess
{
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private readonly DbProviderFactory _provider;
        private readonly string _connectionString;
        private readonly string _name;


        public DbConnectionFactory(string connectionName)
        {
            if (connectionName == null) throw new ArgumentNullException("connectionName");

             // var conStr = ConfigurationManager.ConnectionStrings[connectionName];


            AppConfig AppConfig = new AppConfig(connectionName);

            _name = "System.Data.SqlClient"; //AppConfig.ProviderName;

            if (AppConfig.ConnectionString == null)
                throw new ArgumentNullException(string.Format("Failed to find connection string named '{0}' in app/web.config.", connectionName));

            //DbProviderFactories.RegisterFactory(_name, SqlClientFactory.Instance);
            DbProviderFactories.RegisterFactory(_name, SqlClientFactory.Instance);
            _provider = DbProviderFactories.GetFactory(_name);

            _connectionString = AppConfig.ConnectionString;
        }

        public IDbConnection Create()
        {
            var connection = _provider.CreateConnection();
            if (connection == null)
                throw new ArgumentNullException(string.Format("Failed to create a connection using the connection string named '{0}' in app/web.config.", _name));

            connection.ConnectionString = _connectionString;
            connection.Open();
            return connection;
        }
    }
}
