﻿
namespace DatabaseAccess
{
    public class BaseClass
    {
        public int ErrorType { get; set; }
        public string ErrorMessage { get; set; }
    }
}
