﻿using System.Collections.Generic;

namespace  DatabaseAccess
{
    public class ParameterModel
    {
        public int iD { get; set; }
        public string[] parameter { get; set; }
        public string[] value { get; set; }
        public string xlm { get; set; }
        public string typeAction { get; set; }
        public string orderBy { get; set; }
        public string rowSkip { get; set; }
        public string pageSize { get; set; }
        public string loginToken { get; set; }
        public Dictionary<string,object> dictionary { get; set; }
    }
}
