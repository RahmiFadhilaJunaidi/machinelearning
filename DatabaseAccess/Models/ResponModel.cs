﻿using System.Data;
using System.Collections.Generic;
namespace  DatabaseAccess
{
    public class ResponModel
    {
        public DataTable dt { get; set; }
        public List<Dictionary<string, object>> dynamicData { get; set; }
        public string errorMessage { get; set; }
        public int iD { get; set; }
        public int countData { get; set; }
        public int errorType { get; set; }
    }
}
