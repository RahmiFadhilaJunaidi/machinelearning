﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibrarySys.DA.Models
{
    public class DropDownListModel
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}
