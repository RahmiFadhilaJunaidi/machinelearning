﻿using LibrarySys.DA;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibrarySysDA.Models
{
    public class JsonMenuModelBase : BaseModel
    {
        public JsonMenuModel JsonMenuModel = new JsonMenuModel();
        public List<JsonMenuModel> jsonMenuModels = new List<JsonMenuModel>();
    }
    public class JsonMenuModel
    {
        public string id { get; set; }
        public string parent { get; set; }
        public string text { get; set; }
        public string objCatId { get; set; }
        public State state = new State();
    }
    public class State
    {
        public bool opened { get; set; }
        public string type { get; set; }
    }
}
