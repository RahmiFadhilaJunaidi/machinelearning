﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibrarySys.DA.Models
{
    public class DropDownListModelBase : BaseModel
    {
        public DropDownListModel dropDownListModel = new DropDownListModel();
        public List<DropDownListModel> dropDownListModelList = new List<DropDownListModel>();
    }
}
