﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibrarySys.DA
{
    public class BaseModel
    {
        public int errorType { get; set; }
        public string errorMessage { get; set; }
        public string errorService { get; set; }

        public int waring { get { return 2; } }
        public int error { get { return 1; } }
        public string errorSys { get { return "Error :"; } }

        public BaseModel()
        {
            this.errorType = 0;
            this.errorMessage = string.Empty;
            this.errorService = "error service : ";
        }
    }
}
