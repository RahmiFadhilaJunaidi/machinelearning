﻿using DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.MachineLearningMinatBakat
{
	public class logIncorrectPasswordAttempt : BaseClass
	{
		public logIncorrectPasswordAttempt()
		{
			this.ErrorType = 0;
			this.ErrorMessage = null;
		}
		public logIncorrectPasswordAttempt(int errorType, string errorMessage)
		{
			this.ErrorType = errorType;
			this.ErrorMessage = errorMessage;
		}
		public long logIncorrectPasswordAttemptID { get; set; }
		public string UserID { get; set; }
		public int AttemptCount { get; set; }
		public DateTime CreatedDate { get; set; }
	}
}
