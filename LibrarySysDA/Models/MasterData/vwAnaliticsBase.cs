﻿using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;
using System.Text;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
    public partial class vwAnaliticsBase : BaseModel
    {
        public vwAnalitics vwAnalitics { get; set; }
        public List<vwAnalitics> vwAnaliticsList { get; set; }
    }
}
