﻿using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibrarySysDA.Models.MasterData
{
    public partial class trxPrediksiTestBase : BaseModel
    {
        public trxPrediksiTest trxPrediksiTest { get; set; }
        public List<trxPrediksiTest> trxPrediksiTestList { get; set; }
    }
}
