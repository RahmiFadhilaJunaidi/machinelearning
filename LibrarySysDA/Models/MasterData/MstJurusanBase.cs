using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
     public partial class MstJurusanBase : BaseModel
     {
         public MstJurusan MstJurusan { get; set; }
         public List<MstJurusan> MstJurusanList { get; set; }
     }
}
