﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.MachineLearningMinatBakat
{
    public partial class vwDataSet
    {
        public string nama { get; set; }
        public int SangatTidakSetuju { get; set; }
        public int TidakSetuju { get; set; }
        public int KurangTidakSetuju { get; set; }
        public int Setuju { get; set; }
        public string KodeCategory{ get; set; }

    }
}
