﻿using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;
using System.Text;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
    public partial class vwAccuracyBase : BaseModel
    {
        public vwAccuracy vwAccuracy { get; set; }
        public List<vwAccuracy> vwAccuracyList { get; set; }
    }
}
