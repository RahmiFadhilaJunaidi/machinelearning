using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
     public partial class mstKampusBase : BaseModel
     {
         public mstKampus mstKampus { get; set; }
         public List<mstKampus> mstKampusList { get; set; }
     }
}
