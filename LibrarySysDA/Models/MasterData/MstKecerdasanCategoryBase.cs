using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;

namespace MechineLearningMinatBakat.DA.Models.MasterData
{
     public partial class MstKecerdasanCategoryBase : BaseModel
     {
         public MstKecerdasanCategory MstKecerdasanCategory { get; set; }
         public List<MstKecerdasanCategory> MstKecerdasanCategoryList { get; set; }
     }
}
