﻿using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibrarySysDA.Models.MasterData
{
    public class vwDetailChartCategoryUtilizedBase : BaseModel
    {
        public vwDetailChartCategoryUtilized VwDetailChartCategoryUtilized { get; set; }
        public List<vwDetailChartCategoryUtilized> VwDetailChartCategoryUtilizedList { get; set; }
    }
}
