using LibrarySys.DA;
using LibrarySys.DA.DBContext;
using System;
using System.Collections.Generic;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
     public partial class vwMappingKecerdasanBase : BaseModel
     {
         public vwMappingKecerdasan vwMappingKecerdasan { get; set; }
         public List<vwMappingKecerdasan> vwMappingKecerdasanList { get; set; }
     }
}
