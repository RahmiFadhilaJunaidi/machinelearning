﻿using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;
using System.Text;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
    public class vwDatasetBase : BaseModel
    {
        public vwDataSet vwDataset { get; set; }
        public List<vwDataSet> vwDatasetList { get; set; }
    }
}
