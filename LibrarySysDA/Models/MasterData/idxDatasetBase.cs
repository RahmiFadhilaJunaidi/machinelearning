﻿using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;
using System.Text;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
   public class idxDatasetBase : BaseModel
    {
        public idxDataset IdxDataset { get; set; }
        public List<idxDataset> IdxDatasetList { get; set; }
    }
}
