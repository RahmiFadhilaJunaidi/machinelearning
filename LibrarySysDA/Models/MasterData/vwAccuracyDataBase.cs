﻿using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibrarySysDA.Models.MasterData
{
   public class vwAccuracyDataBase : BaseModel
    {
        public vwAccuracyData vwAccuracyData { get; set; }
        public List<vwAccuracyData> vwAccuracyDataList { get; set; }
    }
}
