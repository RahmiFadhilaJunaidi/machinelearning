using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
     public partial class MstUserBase : BaseModel
     {
         public mstUser MstUser { get; set; }
         public List<mstUser> MstUserList { get; set; }
        public List<vwUser> VwUserList { get; set; }
    }
}
