﻿using DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningMinatBakat
{
    public class vmUserLogin : BaseClass
    {
        public vmUserLogin()
        {
            this.ErrorType = 0;
            this.ErrorMessage = "";
        }
        public vmUserLogin(int errorType, string errorMessage)
        {
            this.ErrorType = errorType;
            this.ErrorMessage = errorMessage;
        }
        public string UserID { get; set; }
        public string UserFullName { get; set; }
        public string UserToken { get; set; }
        public string UserRole { get; set; }
        public int RoleID { get; set; }
        public string UserEmail { get; set; }
        public string Avatar { get; set; }
    }
}
