using LibrarySys.DA;
using LibrarySys.DA.DBContext;
using System;
using System.Collections.Generic;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
     public partial class MstJurusanCategoryBase : BaseModel
     {
         public MstJurusanCategory MstJurusanCategory { get; set; }
         public List<MstJurusanCategory> MstJurusanCategoryList { get; set; }
     }
}
