﻿using DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.MachineLearningMinatBakat
{
    public class vmLogin : BaseClass
    {
        public vmLogin()
        {
            this.ErrorType = 0;
            this.ErrorMessage = "";
        }
        public vmLogin(int errorType, string errorMessage)
        {
            this.ErrorType = errorType;
            this.ErrorMessage = errorMessage;
        }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string IP { get; set; }
    }
}
