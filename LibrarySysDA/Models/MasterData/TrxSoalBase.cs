using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;

namespace MachineLearningMinatBakat.DA.Models.MasterData
{
     public partial class TrxSoalBase : BaseModel
     {
         public TrxSoal TrxSoal { get; set; }
         public List<TrxSoal> TrxSoalList { get; set; }
     }
}
