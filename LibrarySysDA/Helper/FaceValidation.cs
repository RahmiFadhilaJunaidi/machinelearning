﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.Util;
using System.IO;
using System.Diagnostics;


namespace LibrarySys.DA
{
    public class FaceValidation
    {
        //public static Image<Bgr, Byte> currentFrame;
        //public static Capture grabber;
        ////public static HaarCascade face;
        //public static HaarCascade eye;
        //public static MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_COMPLEX, 0.9d, 0.8d);
        //public static Image<Gray, byte> result, TrainedFace = null;
        //public static Image<Gray, byte> gray = null;
        ////public static List<Image<Gray, byte>> trainingImages = new List<Image<Gray, byte>>();
        //public static List<string> labels = new List<string>();
        //public static List<string> NamePersons = new List<string>();
        //public static int ContTrain, NumLabels, t;
        //public static string name, names = null;
        //public static Image<Bgr, byte> image;
        //public static string errorMessage;

        public FaceValidation(string imgSource)
        {
            //  face = new HaarCascade("haarcascade_frontalface_default.xml");

            try
            {
                //Load of previus trainned faces and labels for each image
                string curdir = @"C:\File Program\Dot Net Core\LibrarySystem\LibrarySys\LibrarySysDA\bin\Debug\netcoreapp3.1\TrainedFaces"; //Directory.GetCurrentDirectory();
                string Labelsinfo = File.ReadAllText(curdir+@"\TrainedLabels.txt");
                string[] Labels = Labelsinfo.Split('%');
                int totalImages = int.Parse(Labels[0]);
                string LoadFaces;
                List<Image<Gray, byte>> trainingImages = new List<Image<Gray, byte>>();
                FaceDetection(imgSource, totalImages, trainingImages);
                for (int tf = 1; tf < totalImages + 1; tf++)
                {
                    LoadFaces = "face" + tf + ".bmp";
                    trainingImages.Add(new Image<Gray, byte>(curdir + @"\" + LoadFaces));
                }
                FaceDetection(imgSource, totalImages, trainingImages);
            }
            catch (Exception e)
            {

            }

        }

        private static string FaceDetection(string imageSource, int numLable, List<Image<Gray, byte>> trainingImages)
        {
            string resultProcess = "";
            HaarCascade face = new HaarCascade("haarcascade_frontalface_default.xml");
            string imgSource = imageSource;
            string base64 = "; base64,";
            string firstIdx = "data:image/";
            string fileName = string.Format("{0:ddMMyyyyHHMMss}", DateTime.Now) + ".TestImage";

            int idxBase64Str = imgSource.IndexOf(base64);

            string fileExt = "." + imgSource.Substring((imgSource.IndexOf(firstIdx) + firstIdx.Length), idxBase64Str - (imgSource.IndexOf(firstIdx) + firstIdx.Length));

            imgSource = imgSource.Remove(0, (idxBase64Str + base64.Length));

            string base64String = imgSource;


            byte[] imageByte = Convert.FromBase64String(base64String);


            Image<Bgr, Byte> image = new Image<Bgr, Byte>(170, 130); //specify the width and height here
            image.Bytes = imageByte; //your byte array
            //currentFrame = grabber.QueryFrame().Resize(460, 350, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
            Image<Bgr, Byte> currentFrame;
            currentFrame = image.Resize(460, 350, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

            //Convert it to Grayscale
            Image<Gray, byte> gray;
            gray = currentFrame.Convert<Gray, Byte>();

            //Face Detector
            MCvAvgComp[][] facesDetected = gray.DetectHaarCascade(
          face,
          1.2,
          2, // 10
          Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.DO_CANNY_PRUNING,
          new Size(20, 20));

            Image<Gray, byte> result;
            int t=0;
            string name="";
            string names = "";
            List<string> NamePersons = new List<string>();
            //Action for each element detected
            foreach (MCvAvgComp f in facesDetected[0])
            {
                t = t + 1;
                result = currentFrame.Copy(f.rect).Convert<Gray, byte>().Resize(100, 100, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                //draw the face detected in the 0th (gray) channel with blue color
                currentFrame.Draw(f.rect, new Bgr(Color.YellowGreen), 3);
                List<string> labels = new List<string>();
                MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_COMPLEX, 0.9d, 0.8d);
                if (trainingImages.ToArray().Length != 0)
                {
                    //TermCriteria for face recognition with numbers of trained images like maxIteration
                    MCvTermCriteria termCrit = new MCvTermCriteria(numLable, 0.001);

                    //Eigen face recognizer
                    EigenObjectRecognizer recognizer = new EigenObjectRecognizer(
                       trainingImages.ToArray(),
                       labels.ToArray(),
                       5000, //3000
                       ref termCrit);

                    name = recognizer.Recognize(result);

                    //Draw the label for each face detected and recognized
                    currentFrame.Draw(name, ref font, new Point(f.rect.X - 10, f.rect.Y - 10), new Bgr(Color.Red));


                }

                NamePersons[t - 1] = name;
                
            }
            t = 0;

            //Names concatenation of persons recognized
            for (int nnn = 0; nnn < facesDetected[0].Length; nnn++)
            {
                names = names + NamePersons[nnn] + ", ";
            }
            return resultProcess;
        }
    }
}
