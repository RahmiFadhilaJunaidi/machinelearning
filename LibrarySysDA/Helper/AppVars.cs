﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace LibrarySys.DA
{
    public static class AppVars
    {
        public static string MLMinatBakat { get { return _MLMinatBakat(); } }
        
        private static string _MLMinatBakat()
        {
            return ConfigValue("LibrarySysDB");
        }
       
        private static string ConfigValue(string configName)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            return configuration.GetConnectionString(configName);
        }
    }
}
