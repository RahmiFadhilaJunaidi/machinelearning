﻿using DatabaseAccess;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace MachineLearningMinatBakat.DA.Service
{
    public class Helper
    {
        /// <summary>
        /// Enum Error Type
        /// </summary>
        public enum ErrorType : int
        {
            None = 0,
            Validation = 1,
            Error = 2
        };

        public enum TaskMilestone : int
        {
            Requirement = 2,
            Development = 5,
            SIT = 8,
            UAT = 11,
            PascaUAT = 13
        };

        /// <summary>
        /// Get Connection String
        /// </summary>
        /// <returns></returns>
        public static IDbConnectionFactory GetConnection()
        {
            return new DbConnectionFactory("TeamMate");
        }
        /// <summary>
        /// Get Connection String
        /// </summary>
        /// <param name="strConnection"></param>
        /// <returns></returns>
        public static IDbConnectionFactory GetConnection(string strConnection)
        {
            return new DbConnectionFactory(strConnection);
        }
        /// <summary>
        /// Get DateTime Now
        /// </summary>
        /// <returns></returns>
        public static DateTime GetDateTimeNow()
        {
            string strDtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffff");
            return DateTime.Parse(strDtNow);
        }

        /// <summary>
        /// Get Idle Time Application in Minutes
        /// </summary>
        /// <returns></returns>
        public static int GetIdletime()
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings["IdleTime"]);
        }

        /// <summary>
        /// Get Password Expired in Days
        /// </summary>
        /// <returns></returns>
        public static int GetPasswordExpired()
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings["PasswordExpired"]);
        }

        /// <summary>
        /// Hash string using MD5CryptoServiceProvider
        /// </summary>
        /// <param name="strPassword"></param>
        /// <returns></returns>
        public static string HashingMD5(string strInput)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            Byte[] bytes = md5.ComputeHash(new System.Text.UTF8Encoding().GetBytes(strInput));
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (Byte item in bytes)
            {
                sb.Append(item.ToString("x2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Log Error
        /// </summary>
        /// <param name="strError"></param>
        /// <param name="strService"></param>
        /// <param name="strMethod"></param>
        /// <returns></returns>
        //public static string LogError(string strError, string strService, string strMethod, string strUserID)
        //{
        //    var context = new DbContext(Helper.GetConnection());
        //    var errorRepo = new logErrorRepository(context);

        //    logError error = new logError();
        //    error.ErrorMessages = strError;
        //    error.Service = strService;
        //    error.Method = strMethod;
        //    error.CreatedDate = Helper.GetDateTimeNow();
        //    error.CreatedBy = strUserID;
        //    errorRepo.Create(error);

        //    return "Error on System. Please Call IT Help Desk!";
        //}

        /// <summary>
        /// Token TBG Sys
        /// </summary>
        /// <returns></returns>
        /// 

        public static void CreateNewFolder(string strPath)
        {
            if (!Directory.Exists(strPath))
            {
                DirectoryInfo di = Directory.CreateDirectory(strPath);
            }
        }

        public static string GetTokenTBGSys()
        {
            var Token = ConfigurationManager.AppSettings["TokenTBGSys"];
            return Token;
        }

        public static string GetTimeStamp()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmss");
        }

        public static string GetNamaBulan(int month)
        {
            string namaBulan = "";
            if (month == 1)
                namaBulan = "Januari";
            else if (month == 2)
                namaBulan = "Februari";
            else if (month == 3)
                namaBulan = "Maret";
            else if (month == 4)
                namaBulan = "April";
            else if (month == 5)
                namaBulan = "Mei";
            else if (month == 6)
                namaBulan = "Juni";
            else if (month == 7)
                namaBulan = "Juli";
            else if (month == 8)
                namaBulan = "Agustus";
            else if (month == 9)
                namaBulan = "September";
            else if (month == 10)
                namaBulan = "Oktober";
            else if (month == 11)
                namaBulan = "November";
            else if (month == 12)
                namaBulan = "Desember";
            return namaBulan;
        }

        //public static string UploadFile(string strFolderName, HttpPostedFile postedFile)
        //{
        //    try
        //    {
        //        string timeStamp = GetTimeStamp();
        //        string targetPath = HttpContext.Current.Server.MapPath(Helper.GetDocPath() + "\\" + strFolderName + "\\");
        //        CreateNewFolder(targetPath);
        //        targetPath = targetPath + timeStamp + "." + postedFile.FileName;
        //        postedFile.SaveAs(targetPath);
        //        return "\\" + strFolderName + "\\" + timeStamp + "." + postedFile.FileName;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static string UploadedAvatar(string strFolderName, string UserID, HttpPostedFile postedFile)
        //{
        //    try
        //    {
        //        string targetPath = HttpContext.Current.Server.MapPath(Helper.GetAvaPath() + "\\" + strFolderName + "\\");
        //        string extension = Path.GetExtension(postedFile.FileName);
        //        CreateNewFolder(targetPath);
        //        targetPath = targetPath + UserID + extension;
        //        postedFile.SaveAs(targetPath);
        //        return "/content/images/" + strFolderName + "/" + UserID + extension;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static void SendEmail(string strTo, string strCC, string strBCC, string strSubject, string strBody, string strReplyTo, string urlAttachment)
        //{
        //    var context = new DbContext(Helper.GetConnection());

        //    try
        //    {

        //        //Sending email
        //        using (var command = context.CreateCommand())
        //        {
        //            command.CommandType = CommandType.StoredProcedure;
        //            command.CommandText = "msdb.dbo.sp_send_dbmail";

        //            command.Parameters.Add(command.CreateParameter("@profile_name", ConfigurationManager.AppSettings["EmailProfile"].ToString()));
        //            command.Parameters.Add(command.CreateParameter("@recipients", strTo));
        //            command.Parameters.Add(command.CreateParameter("@copy_recipients", strCC));
        //            command.Parameters.Add(command.CreateParameter("@blind_copy_recipients", strBCC));
        //            command.Parameters.Add(command.CreateParameter("@subject", strSubject.Trim()));
        //            command.Parameters.Add(command.CreateParameter("@body", strBody));
        //            command.Parameters.Add(command.CreateParameter("@body_format", "HTML"));
        //            command.Parameters.Add(command.CreateParameter("@reply_to", strReplyTo));
        //            command.Parameters.Add(command.CreateParameter("@file_attachments", urlAttachment));

        //            command.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        context.Dispose();
        //    }
        //}
    }
}
