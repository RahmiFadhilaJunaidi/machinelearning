using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;

namespace MachineLearningMinatBakat.DA.Service.MasterData
 {
 public class mstKampusService
 {
     BaseModel baseModel = new BaseModel();

     public mstKampusBase mstKampusGetList()
     {
         var listBase = new mstKampusBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.mstKampusList = context.mstKampus.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string mstKampusDelete(mstKampus Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.mstKampus.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string mstKampusCreate(mstKampus Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.mstKampus.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string mstKampusUpdate(mstKampus Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.mstKampus.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
