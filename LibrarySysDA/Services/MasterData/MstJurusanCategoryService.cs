using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA;
using LibrarySys.DA.DBContext;
using LibrarySys.DA.Models;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Microsoft.EntityFrameworkCore;

namespace MachineLearningMinatBakat.DA.Service.MasterData
 {
 public class MstJurusanCategoryService
 {
     BaseModel baseModel = new BaseModel();

     public MstJurusanCategoryBase MstJurusanCategoryGetList()
     {
         var listBase = new MstJurusanCategoryBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.MstJurusanCategoryList = context.MstJurusanCategory.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstJurusanCategoryDelete(MstJurusanCategory Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstJurusanCategory.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

        public string GenerateKodeCategoryJurusan()
        {

            var prefix = "C";
            MstJurusanCategory genCat = new MstJurusanCategory();
            //var context = new DBContext();
            using (var context = new DBContext())
            {
                var data = context.MstJurusanCategory.OrderByDescending(p => p.createdDate).FirstOrDefault();
                if (data != null)
                {
                    var number = data.kodeJurusanCategory.Substring(1, 4);
                    var countNumber = Convert.ToInt32(number) + 1;
                    var leadingZero = Convert.ToString(countNumber).PadLeft(4, '0');
                    genCat.kodeJurusanCategory = prefix + leadingZero;
                }
                else
                {
                    genCat.kodeJurusanCategory = prefix + "1".PadLeft(4, '0');
                }
            }
            return genCat.kodeJurusanCategory;
        }
        public string MstJurusanCategoryCreate(MstJurusanCategory Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                    Post.kodeJurusanCategory = GenerateKodeCategoryJurusan();
                 Post.createdDate = DateTime.Now;
                 context.MstJurusanCategory.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

    public string MstJurusanCategoryUpdate(MstJurusanCategory Post)
     {

            var context = new DBContext();
            MstJurusanCategory Jurusan = new MstJurusanCategory();

            var Result = context.MstJurusanCategory
                .Where(p => p.idJurusanCategory == Post.idJurusanCategory)
                .FirstOrDefault();

            try
            {
                Jurusan = Result;
                Jurusan.jurusanCategory = Post.jurusanCategory;
                context.Entry(Jurusan).State = EntityState.Modified;
                context.SaveChanges();

                return "";
            }


            catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }
        
        public DropDownListModelBase GetJurusanCategory()
        {
            var listBase = new DropDownListModelBase();
            try
            {
                using (var context = new DBContext())
                {

                    var listCat = new List<MstJurusanCategory>();
                    string query = " select * from MstJurusanCategory ";
                    listCat = context.MstJurusanCategory.FromSqlRaw(query).ToList();
                    listBase.dropDownListModelList = (from a in listCat
                                                      select new DropDownListModel
                                                      {
                                                          Value = a.kodeJurusanCategory.ToString(),
                                                          Text = a.jurusanCategory
                                                      }).ToList();
                }
                return listBase;
            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }

    }
}
