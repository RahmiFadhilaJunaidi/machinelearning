using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;

namespace MachineLearningMinatBakat.DA.Service.MasterData
 {
 public class MstMappingKecerdasanService
 {
     BaseModel baseModel = new BaseModel();

     public vwMappingKecerdasanBase MstMappingKecerdasanGetList()
     {
         var listBase = new vwMappingKecerdasanBase();
         try
         {
             using (var context = new DBContext())
             {
                    listBase.vwMappingKecerdasanList = context.vwMappingKecerdasan.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstMappingKecerdasanDelete(MstMappingKecerdasan Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstMappingKecerdasan.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstMappingKecerdasanCreate(MstMappingKecerdasan Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 Post.CreatedDate = DateTime.Now;
                 context.MstMappingKecerdasan.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstMappingKecerdasanUpdate(MstMappingKecerdasan Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstMappingKecerdasan.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
