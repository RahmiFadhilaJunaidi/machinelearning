﻿using LibrarySys.DA;
using LibrarySysDA.Models.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachineLearningMinatBakat.DA.Service.MasterData
{
    public class vwDataAnaliticsService
    {
        BaseModel baseModel = new BaseModel();

        public vwAccuracyBase vwAccuracyGetList()
        {
            var listBase = new vwAccuracyBase();
            try
            {
                using (var context = new DBContext())
                {
                    listBase.vwAccuracyList = context.vwAccuracy.OrderBy(p => p.Metode).ToList();
                    context.Dispose();
                }
                return listBase;
                
            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }

        public vwAccuracyDataBase vwAccuracyDataGetList()
        {
            var listBase = new vwAccuracyDataBase();
            try
            {
                using (var context = new DBContext())
                {
                    listBase.vwAccuracyDataList = context.vwAccuracyData.OrderBy(p => p.Metode).ToList();
                    context.Dispose();
                }
                return listBase;

            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }

        public vwAnaliticsBase vwAnaliticsGetList()
        {
            var listBase = new vwAnaliticsBase();
            try
            {
                using (var context = new DBContext())
                {
                    listBase.vwAnaliticsList = context.vwAnalitics.ToList();
                    context.Dispose();
                }
                return listBase;

            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }

        public vwDetailChartCategoryUtilizedBase vwDetailCategoryUtilizedGetList(string KodeCategory)
        {
            var listBase = new vwDetailChartCategoryUtilizedBase();
            try
            {
                using (var context = new DBContext())
                {
                    listBase.VwDetailChartCategoryUtilizedList = context.vwDetailChartCategoryUtilized.Where(p=> p.KodeCategory == KodeCategory).ToList();
                    context.Dispose();
                }
                return listBase;

            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }

        public trxPrediksiTestBase vwDetailChartAccuracy(string metode)
        {
            var listBase = new trxPrediksiTestBase();
            try
            {
                using (var context = new DBContext())
                {
                    listBase.trxPrediksiTestList = context.trxPrediksiTest.Where(p => p.metode == metode).ToList();
                    context.Dispose();
                }
                return listBase;

            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }


        public List<vwDetailChartCategoryUtilized> exportCategoryUtilizedGetList(string KodeCategory)
        {
            var listBase = new vwDetailChartCategoryUtilizedBase();
            try
            {
                using (var context = new DBContext())
                {
                    listBase.VwDetailChartCategoryUtilizedList = context.vwDetailChartCategoryUtilized.Where(p => p.KodeCategory == KodeCategory).ToList();
                    context.Dispose();
                }
                return listBase.VwDetailChartCategoryUtilizedList;

            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase.VwDetailChartCategoryUtilizedList;
            }
        }

        public List<trxPrediksiTest> exportDetailChartAccuracy(string metode)
        {
            var listBase = new trxPrediksiTestBase();
            try
            {
                using (var context = new DBContext())
                {
                    listBase.trxPrediksiTestList = context.trxPrediksiTest.Where(p => p.metode == metode).ToList();
                    context.Dispose();
                }
                return listBase.trxPrediksiTestList;

            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase.trxPrediksiTestList;
            }
        }
    }
}
