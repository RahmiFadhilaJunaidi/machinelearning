using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA;
using MachineLearningMinatBakat;
using MechineLearningMinatBakat.DA.Models.MasterData;
using Microsoft.EntityFrameworkCore;
using Models.MachineLearningMinatBakat;

namespace MechineLearningMinatBakat.DA.Service.MasterData
 {
 public class MstMetodePakarService
 {
     BaseModel baseModel = new BaseModel();

        public MstMetodePakarBase MstMetodePakarGetList()
        {
            var listBase = new MstMetodePakarBase();
            try
            {
                using (var context = new DBContext())
                {
                    listBase.MstMetodePakarList = context.MstMetodePakar.ToList();
                    context.Dispose();
                }
                return listBase;
            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }

        public string MstMetodePakarDelete(MstMetodePakar Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstMetodePakar.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstMetodePakarCreate(MstMetodePakar Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 Post.CreatedDate = DateTime.Now;
                 context.MstMetodePakar.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstMetodePakarUpdate(MstMetodePakar Post)
     {
            var context = new DBContext();
            MstMetodePakar Metode = new MstMetodePakar();

            var Result = context.MstMetodePakar
                .Where(p => p.IdMetode == Post.IdMetode)
                .FirstOrDefault();
        try
         {
                Metode = Result;
                Metode.Metode = Post.Metode;
                Metode.Keterangan = Post.Keterangan;
                context.Entry(Metode).State = EntityState.Modified;
                context.SaveChanges();

                return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }
    }
}
