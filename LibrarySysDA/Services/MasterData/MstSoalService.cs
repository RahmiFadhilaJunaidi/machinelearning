using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA;
using LibrarySys.DA.Models;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Microsoft.EntityFrameworkCore;
using Models.MachineLearningMinatBakat;

namespace MachineLearningMinatBakat.DA.Service.MasterData
 {
 public class MstSoalService
 {
     BaseModel baseModel = new BaseModel();

        public string GenerateKodeSoal()
        {

            var prefix = "L";
            MstSoal genSoal = new MstSoal();
            //var context = new DBContext();
            using (var context = new DBContext())
            {
                var data = context.MstSoal.OrderByDescending(p => p.CreatedDate).FirstOrDefault();
                //var data = context.MstSoal.FirstOrDefault();
                if (data != null)
                {
                    var number = data.KodeSoal.Substring(1, 3);
                    var countNumber = Convert.ToInt32(number) + 1;
                    var leadingZero = Convert.ToString(countNumber).PadLeft(3, '0');
                    genSoal.KodeSoal = prefix + leadingZero;
                }
                else
                {
                    genSoal.KodeSoal = prefix + "1".PadLeft(3, '0');
                }
            }
            return genSoal.KodeSoal;
        }

        public MstSoalBase MstSoalGetList()
     {
         var listBase = new MstSoalBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.MstSoalList = context.MstSoal.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstSoalDelete(MstSoal Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstSoal.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstSoalCreate(MstSoal Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                    Post.KodeSoal = GenerateKodeSoal();
                    Post.CreatedDate = DateTime.Now;
                    context.MstSoal.Add(Post);
                    context.SaveChanges();
                    context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstSoalUpdate(MstSoal Post)
     {
        
        var context = new DBContext();
        MstSoal Soal = new MstSoal();

        var Result = context.MstSoal
            .Where(p => p.IdSoal == Post.IdSoal)
            .FirstOrDefault();

        try
        {
            Soal = Result;
            Soal.Soal = Post.Soal;
            context.Entry(Soal).State = EntityState.Modified;
            context.SaveChanges();
                    
            return "";
        }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

        public DropDownListModelBase GetSoal(String KodeCategory)
        {
            var listBase = new DropDownListModelBase();
            try
            {
                using (var context = new DBContext())
                {
                    var start = 0;
                    var end = 0;

                    if (KodeCategory == "K001")
                    {
                        start = 1;
                        end = 12;
                    }
                    else if(KodeCategory == "K002")
                    {
                        start = 13;
                        end = 24;
                    }
                    else if (KodeCategory == "K003")
                    {
                        start = 25;
                        end = 36;
                    }
                    else if (KodeCategory == "K004")
                    {
                        start = 37;
                        end = 48;
                    }
                    else if (KodeCategory == "K005")
                    {
                        start = 49;
                        end = 60;
                    }
                    else if (KodeCategory == "K006")
                    {
                        start = 61;
                        end = 72;
                    }
                    else if (KodeCategory == "K007")
                    {
                        start = 73;
                        end = 84;
                    }
                    else if (KodeCategory == "K008")
                    {
                        start = 85;
                        end = 96;
                    }
                    else if (KodeCategory == "K009")
                    {
                        start = 97;
                        end = 108;
                    }

                    var listSoal = new List<MstSoal>();
                    string query = " SELECT * FROM [dbo].[mstSoal]" +
                                    " where RIGHT(kodeSoal,3) >= "+ start +" and RIGHT(kodeSoal,3) <= "+end+" " +
                                    " AND kodeSoal NOT IN(SELECT kodeSoal FROM[dbo].[mstMappingKecerdasan]) ";
                    listSoal = context.MstSoal.FromSqlRaw(query).ToList();
                    listBase.dropDownListModelList = (from a in listSoal
                                                      select new DropDownListModel
                                                      {
                                                          Value = a.KodeSoal.ToString(),
                                                          Text = a.Soal
                                                      }).ToList();
                }
                return listBase;
            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }

    }
}
