﻿using LibrarySys.DA;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;
using System;
using System.Linq;

namespace MachineLearningMinatBakat.DA.Service.MasterData
{
    public class MstDataService
    {
        BaseModel baseModel = new BaseModel();

        public string GenerateUserID()
        {
           
            var prefix = "USR";
            mstUser genUserID = new mstUser();
            //var context = new DBContext();
            using (var context = new DBContext())
            {
                var data = context.mstUser.OrderByDescending(p => p.createdDate).FirstOrDefault();
                //var data = context.mstUser.FirstOrDefault();
                if (data != null)
                {
                    var number = data.userID.Substring(3, 4);
                    var countNumber = Convert.ToInt32(number) + 1;
                    var leadingZero = Convert.ToString(countNumber).PadLeft(4, '0');
                    genUserID.userID = prefix + leadingZero;
                }
                else
                {
                    genUserID.userID = prefix + "1".PadLeft(4, '0');
                }

            }

            
            return genUserID.userID;
        }

        public MstUserBase mstUserGetList()
        {
            var listBase = new MstUserBase();
            try
            {
                //using (var context = new DBContext())
                //{
                //    //listBase.mstUserList = context.mstUser.ToList();
                //    var data = context.mstUser.ToList();
                //    context.Dispose();
                //}
                var context = new DBContext();
                var data = context.mstUser.OrderByDescending(p => p.createdDate).FirstOrDefault();
                listBase.MstUserList = context.mstUser.ToList();
                
                context.Dispose();
                return listBase;
            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }

        public string MstDataUserCreate(mstUser Post)
        {
            try
            {
                using (var context = new DBContext())
                {
                    Post.userID = GenerateUserID();
                    Post.createdDate = DateTime.Now;
                    Post.createdBy = Post.userID;
                    context.mstUser.Add(Post);
                    context.SaveChanges();
                    context.Dispose();
                }
                return "";
            }
            catch (Exception ex)
            {
                return baseModel + ex.Message;
            }
        }

        public string CekDataUser(mstUser Post)
        {
            try
            {
                //mstUser data = new mstUser();
                var userName = "";
                using (var context = new DBContext())
                {
                    var data = context.mstUser.Where(p => p.userName == Post.userName).FirstOrDefault();
                    if (data != null)
                    {
                        userName = data.userName;
                    }
                    else
                    {
                        userName = "";
                    }
                }
                return userName;
            }
            catch (Exception ex)
            {
                return baseModel + ex.Message;
            }
        }
    }
}
