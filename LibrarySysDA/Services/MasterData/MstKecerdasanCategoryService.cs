using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA;
using LibrarySys.DA.Models;
using MachineLearningMinatBakat;
using MechineLearningMinatBakat.DA.Models.MasterData;
using Microsoft.EntityFrameworkCore;
using Models.MachineLearningMinatBakat;

namespace MechineLearningMinatBakat.DA.Service.MasterData
 {
 public class MstKecerdasanCategoryService
 {
     BaseModel baseModel = new BaseModel();

     public MstKecerdasanCategoryBase MstKecerdasanCategoryGetList()
     {
         var listBase = new MstKecerdasanCategoryBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.MstKecerdasanCategoryList = context.MstKecerdasanCategory.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string MstKecerdasanCategoryDelete(MstKecerdasanCategory Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstKecerdasanCategory.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstKecerdasanCategoryCreate(MstKecerdasanCategory Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 Post.KodeCategory = GenerateCodeCategory();
                 Post.CreatedDate = DateTime.Now;
                 context.MstKecerdasanCategory.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstKecerdasanCategoryUpdate(MstKecerdasanCategory Post)
     {
            var context = new DBContext();
            MstKecerdasanCategory Category = new MstKecerdasanCategory();

            var Result = context.MstKecerdasanCategory
                .Where(p => p.IdKecerdasanCategory == Post.IdKecerdasanCategory)
                .FirstOrDefault();

        try
         {
                Category = Result;
                Category.CategoryName = Post.CategoryName;
                context.Entry(Category).State = EntityState.Modified;
                context.SaveChanges();

                //using (var context = new DBContext())
                //{

                //    Post.CreatedDate = DateTime.Now; 
                //    context.MstKecerdasanCategory.Update(Post);
                //    context.SaveChanges();
                //    context.Dispose();
                //}
                return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string GenerateCodeCategory()
        {
            var prefix = "K";

            var context = new DBContext();
            
            var data = context.MstKecerdasanCategory.OrderByDescending(p => p.CreatedDate).FirstOrDefault();
            MstJurusan genCodeJurusan = new MstJurusan();

            if (data != null)
            {
                //genUserID.UserId = prefix + "/" + "1".PadLeft(4, '0');
                var number = data.KodeCategory.Substring(1);
                var countNumber = Convert.ToInt32(number) + 1;
                var leadingZero = Convert.ToString(countNumber).PadLeft(3, '0');
                genCodeJurusan.kodeJurusan = prefix.ToUpper() + leadingZero;
            }
            else
            {
                genCodeJurusan.kodeJurusan = prefix + "1".PadLeft(3, '0');
            }
            return genCodeJurusan.kodeJurusan;
        }

        public DropDownListModelBase GetProjectListNotExist()
        {
            var listBase = new DropDownListModelBase();
            try
            {
                using (var context = new DBContext())
                {

                    var listCategory = new List<MstKecerdasanCategory>();
                    string query = " select * from MstKecerdasanCategory ";
                    listCategory = context.MstKecerdasanCategory.FromSqlRaw(query).ToList();
                    listBase.dropDownListModelList = (from a in listCategory
                                                      select new DropDownListModel
                                                      {
                                                          Value = a.KodeCategory.ToString(),
                                                          Text = a.CategoryName
                                                      }).ToList();
                }
                return listBase;
            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }

    }
}
