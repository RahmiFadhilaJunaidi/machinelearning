﻿using DatabaseAccess;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachineLearningMinatBakat.DA.Service
{
    public class AccessService
    {
        public static vmLogin LoginValidation(vmLogin login)
        {
            vmLogin userLogin = new vmLogin();
            var context = new DBContext();
            //var uow = context.CreateUnitOfWork();

            try
            {
                
                mstUser user = context.mstUser.Where(p => p.userName == login.UserName.Trim()).FirstOrDefault();

                //Login Validation
                if (user == null)
                    return new vmLogin((int)Helper.ErrorType.Validation, "User ID is incorrect!");

                //if (!user.IsActive)
                //    return new mstUser((int)Helper.ErrorType.Validation, "User ID is Inactive!");

                //if (Helper.HashingMD5(login.Password) != user.Password)
                if (login.Password != user.password)
                {
                    logIncorrectPasswordAttempt incorrectPasswordAttempt = new logIncorrectPasswordAttempt();
                    var dataUser = context.mstUser.Where(p => p.userName == login.UserName.Trim()).FirstOrDefault();

                    if (dataUser != null)
                    {
                        //incorrectPasswordAttempt = context.mstUser.Where(p => p.UserName == login.UserName.Trim()).FirstOrDefault();
                        incorrectPasswordAttempt.AttemptCount = (Helper.GetDateTimeNow() - incorrectPasswordAttempt.CreatedDate).TotalSeconds > 300 ? 1 : incorrectPasswordAttempt.AttemptCount + 1;
                        incorrectPasswordAttempt.CreatedDate = Helper.GetDateTimeNow();
                        //incorrectPasswordAttemptRepo.Update(incorrectPasswordAttempt);
                    }
                    else
                    {
                        incorrectPasswordAttempt.UserID = user.userID;
                        incorrectPasswordAttempt.AttemptCount = 1;
                        incorrectPasswordAttempt.CreatedDate = Helper.GetDateTimeNow();
                        //incorrectPasswordAttemptRepo.Create(incorrectPasswordAttempt);
                    }

                    //uow.SaveChanges();
                    //return new mstUser((int)Helper.ErrorType.Validation, "Password is incorrect! Attempt: " + incorrectPasswordAttempt.AttemptCount + ".");
                }

                userLogin.UserName = user.userName;
                userLogin.Password = user.password;
                userLogin.UserID = user.userID;

                //if (token != null && login.IP != token.IP && token.ExpiredDate > DateTime.Now)
                //    return new vmUserLogin((int)Helper.ErrorType.Validation, "User ID has been used on another computer!");

                return userLogin;
            }
            catch (Exception ex)
            {
                //uow.Dispose();
                //return new mstUser((int)Helper.ErrorType.Error, ex.Message.ToString());
                return userLogin;
            }
            finally
            {
                context.Dispose();
            }
        }

        public static mstUser Login(mstUser login)
        {
            
            mstUser userLogin = new mstUser();
            var context = new DBContext();

            try
            {
                mstUser user = context.mstUser.Where(p => p.userName == login.userName.Trim()).FirstOrDefault();

                //Login
                userLogin.userID = user.userID;
                userLogin.nama = user.nama;
                //userLogin.UserRole = user.Role;
                //userLogin.RoleID = user.RoleID;
                //userLogin.UserEmail = user.Email;
                //userLogin.Avatar = user.Avatar;

                //trxToken tokenNew = new trxToken();
                //tokenNew.UserID = login.UserID.Trim();
                //tokenNew.UserToken = token != null ? token.UserToken : Guid.NewGuid().ToString();
                //tokenNew.IP = login.IP;
                //tokenNew.ExpiredDate = Helper.GetDateTimeNow().AddMinutes(Helper.GetIdletime());
                //tokenRepo.Create(tokenNew);

                //userLogin.UserToken = tokenNew.UserToken;

                // delete previous token
                //if (token != null)
                //    tokenRepo.DeleteByPK(token.trxTokenID);

                // delete incorrect password attempt
                //incorrectPasswordAttemptRepo.DeleteByFilter("UserID = '" + login.UserID + "'");

                //uow.SaveChanges();

                return userLogin;
            }
            catch (Exception ex)
            {
                //uow.Dispose();
                //return new mstUser((int)Helper.ErrorType.Error, ex.Message.ToString());
                return userLogin;
            }
            finally
            {
                context.Dispose();
            }
        }
    }
}
