using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;

namespace MachineLearningMinatBakat.DA.Service.MasterData
 {
 public class TrxSoalService
 {
     BaseModel baseModel = new BaseModel();

     public TrxSoalBase TrxSoalGetList()
     {
         var listBase = new TrxSoalBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.TrxSoalList = context.TrxSoal.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string TrxSoalDelete(TrxSoal Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.TrxSoal.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string TrxSoalCreate(TrxSoal Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.TrxSoal.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string TrxSoalUpdate(TrxSoal Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.TrxSoal.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
