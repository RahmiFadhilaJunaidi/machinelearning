using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA;
using LibrarySys.DA.Models;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Microsoft.EntityFrameworkCore;
using Models.MachineLearningMinatBakat;

namespace MachineLearningMinatBakat.DA.Service.MasterData
 {
 public class MstJurusanService
 {
     BaseModel baseModel = new BaseModel();

        public MstJurusanBase MstJurusanGetList()
        {
            var listBase = new MstJurusanBase();
            try
            {
                using (var context = new DBContext())
                {
                    listBase.MstJurusanList = context.MstJurusan.ToList();
                    context.Dispose();
                }
                return listBase;
            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }

        public string GenerateCodeJurusan(string Jurusan)
        {
            var prefix = "";
            if (Jurusan.Length > 3)
            {
                prefix = Jurusan.Substring(0, 3);
            }
            else
            {
                prefix = Jurusan;
            }

            var context = new DBContext();
            var data = context.MstJurusan.Where(p => p.kodeJurusan.Substring(0, 3) == Jurusan.Substring(0,3)).OrderByDescending(p => p.createdDate).FirstOrDefault();
            MstJurusan genCodeJurusan = new MstJurusan();

            if (data != null)
            {
                var number = data.kodeJurusan.Substring(3, 3);
                var countNumber = Convert.ToInt32(number) + 1;
                var leadingZero = Convert.ToString(countNumber).PadLeft(3, '0');
                genCodeJurusan.kodeJurusan = prefix.ToUpper() + leadingZero;
            }
            else
            {
                genCodeJurusan.kodeJurusan = prefix.ToUpper() + "1".PadLeft(3, '0');
            }
            return genCodeJurusan.kodeJurusan;
        }

        public string MstJurusanDelete(MstJurusan Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.MstJurusan.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string MstJurusanCreate(MstJurusan Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 var codeJurusan = GenerateCodeJurusan(Post.jurusan);
                 Post.kodeJurusan = codeJurusan;
                 Post.createdDate = DateTime.Now;
                 context.MstJurusan.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string MstJurusanUpdate(MstJurusan Post)
     {
            var context = new DBContext();
            MstJurusan Jurusan = new MstJurusan();

            var Result = context.MstJurusan
                .Where(p => p.idJurusan == Post.idJurusan)
                .FirstOrDefault();

            try
         {
                Jurusan = Result;
                Jurusan.jurusan = Post.jurusan;
                Jurusan.kodeJurusanCategory = Post.kodeJurusanCategory;
                context.Entry(Jurusan).State = EntityState.Modified;
                context.SaveChanges();

                return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

        public DropDownListModelBase GetJurusanListNotExist()
        {
            var listBase = new DropDownListModelBase();
            try
            {
                using (var context = new DBContext())
                {

                    var listJurusan = new List<MstJurusan>();
                    string query = " select * from MstJurusan ";
                    listJurusan = context.MstJurusan.FromSqlRaw(query).ToList();
                    listBase.dropDownListModelList = (from a in listJurusan
                                                      select new DropDownListModel
                                                      {
                                                          Value = a.kodeJurusan.ToString(),
                                                          Text = a.jurusan
                                                      }).ToList();
                }
                return listBase;
            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }

    }
}
