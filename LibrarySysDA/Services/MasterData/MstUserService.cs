using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Microsoft.EntityFrameworkCore;
using Models.MachineLearningMinatBakat;

namespace MachineLearningMinatBakat.DA.Service.MasterData
 {
 public class mstUserService
 {
     BaseModel baseModel = new BaseModel();

     public MstUserBase mstUserGetList()
     {
         var listBase = new MstUserBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.VwUserList = context.vwUser.ToList();
                 
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             //listBase.errorMessage = listBase.errorService+ ex.Message;
             //listBase.errorType = 1;
             return listBase;
         }
     }

     public string mstUserDelete(mstUser Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.mstUser.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string mstUserCreate(mstUser Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.mstUser.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string mstUserUpdate(mstUser Post)
     {

            var context = new DBContext();
            mstUser usr = new mstUser();

            var Result = context.mstUser
                .Where(p => p.userID == Post.userID)
                .FirstOrDefault();

            try
            {
                usr = Result;
                usr.nama = Post.nama;
                usr.userName = Post.userName;
                usr.tempatLahir = Post.tempatLahir;
                usr.tglLahir = Post.tglLahir;
                usr.alamat = Post.alamat;
                usr.email = Post.email;
                usr.telp = Post.telp;
                usr.jenisKelamin = Post.jenisKelamin.ToString();
                context.Entry(usr).State = EntityState.Modified;
                context.SaveChanges();

                return "";
            }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

        public string mstUserLogin(string userID)
        {

            var context = new DBContext();
            mstUser usr = new mstUser();

            var Result = context.mstUser
                .Where(p => p.userID == userID)
                .FirstOrDefault();

            try
            {
                usr = Result;
                usr.isActive = true;
                context.Entry(usr).State = EntityState.Modified;
                context.SaveChanges();

                return "";
            }
            catch (Exception ex)
            {
                return baseModel + ex.Message;
            }
        }
    }
}
