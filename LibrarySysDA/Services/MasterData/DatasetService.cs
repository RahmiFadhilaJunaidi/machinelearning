﻿using LibrarySys.DA;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Microsoft.EntityFrameworkCore;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningMinatBakat.DA.Service.MasterData
{
    public class DatasetService
    {
        BaseModel baseModel = new BaseModel();
        public idxDatasetBase vwDatasetGetList()
        {
            var listBase = new idxDatasetBase();
            try
            {
                using (var context = new DBContext())
                {
                    //var users = context
                    //       .idxDataset
                    //       .FromSqlRaw("exec dbo.spDataSetNaiveBayes")
                    //       .ToList();
                    listBase.IdxDatasetList = context.idxDataset.ToList();
                    
                }
                return listBase;

            }
            catch (Exception ex)
            {
                listBase.errorMessage = listBase.errorService + ex.Message;
                listBase.errorType = 1;
                return listBase;
            }
        }
    }
}
