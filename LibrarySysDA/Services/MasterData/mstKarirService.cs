using System;
using System.Collections.Generic;
using System.Linq;
using LibrarySys.DA;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;

namespace MachineLearningMinatBakat.DA.Service.MasterData
 {
 public class mstKarirService
 {
     BaseModel baseModel = new BaseModel();

     public mstKarirBase mstKarirGetList()
     {
         var listBase = new mstKarirBase();
         try
         {
             using (var context = new DBContext())
             {
                 listBase.mstKarirList = context.mstKarir.ToList();
                 context.Dispose();
             }
             return listBase;
         }
         catch (Exception ex)
         {
             listBase.errorMessage = listBase.errorService+ ex.Message;
             listBase.errorType = 1;
             return listBase;
         }
     }

     public string mstKarirDelete(mstKarir Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.mstKarir.Remove(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel.errorService + ex.Message;
         }
     }

     public string mstKarirCreate(mstKarir Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.mstKarir.Add(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

     public string mstKarirUpdate(mstKarir Post)
     {
         try
         {
             using (var context = new DBContext())
             {
                 context.mstKarir.Update(Post);
                 context.SaveChanges();
                 context.Dispose();
             }
            return "";
         }
         catch (Exception ex)
         {
             return baseModel+ ex.Message;
         }
     }

 }
}
