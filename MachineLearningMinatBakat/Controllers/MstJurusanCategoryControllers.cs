using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;
using LibrarySys.DA.DBContext;
using LibrarySys.DA.Models;

namespace MachineLearningMinatBakat.Controllers
{
 public class MstJurusanCategoryController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/MstJurusanCategory.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstJurusanCategoryBase();
         try
         {
             var service = new MstJurusanCategoryService();
             result = service.MstJurusanCategoryGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstJurusanCategoryList });
     }


     [HttpPost]
     public IActionResult Create(MstJurusanCategory Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstJurusanCategoryService();
             respon.errorMessage = service.MstJurusanCategoryCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstJurusanCategory Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstJurusanCategoryService();
             respon.errorMessage = service.MstJurusanCategoryUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstJurusanCategory Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstJurusanCategoryService();
             respon.errorMessage = service.MstJurusanCategoryDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }

    [HttpGet]
    public IActionResult DdlJurusanCategory()
    {
        var respon = new Respon();
        var result = new DropDownListModelBase();

        try
        {
            var service = new MstJurusanCategoryService();
            result = service.GetJurusanCategory();
            respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
        }
        catch (Exception ex)
        {
            respon.errorMessage = respon.errorController + ex.Message;
            respon.errorType = 1;
        }
        return Ok(new { respon = respon, data = result.dropDownListModelList });
    }
    }
}
