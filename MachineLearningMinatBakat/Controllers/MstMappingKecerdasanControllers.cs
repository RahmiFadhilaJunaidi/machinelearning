using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;

namespace MachineLearningMinatBakat.Controllers
{
 public class MstMappingKecerdasanController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/MstMappingKecerdasan.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new vwMappingKecerdasanBase();
         try
         {
             var service = new MstMappingKecerdasanService();
             result = service.MstMappingKecerdasanGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.vwMappingKecerdasanList });
     }


     [HttpPost]
     public IActionResult Create(MstMappingKecerdasan Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstMappingKecerdasanService();
             respon.errorMessage = service.MstMappingKecerdasanCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstMappingKecerdasan Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstMappingKecerdasanService();
             respon.errorMessage = service.MstMappingKecerdasanUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstMappingKecerdasan Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstMappingKecerdasanService();
             respon.errorMessage = service.MstMappingKecerdasanDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
