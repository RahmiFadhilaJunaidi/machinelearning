﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MachineLearningMinatBakat.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace MachineLearningMinatBakat.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        //public IActionResult Index()
        //{
        //    return View();
        //}
        [AllowAnonymous]
        [Route("")]
        public ActionResult Index()
        {
            var test = ViewBag.UserName;
            
            ViewBag.data = HttpContext.Session.GetString("UserName"); ;

            if (ViewBag.data != null && ViewBag.data != "")
            {
                ViewBag.UserName = ViewBag.data;
                return RedirectToAction("Home", "Dashboard");
            }
            else
            {
                string strYear = "2017";
                ViewBag.CopyrightYear = DateTime.Now.Year.ToString() != strYear ? strYear + "-" + DateTime.Now.Year.ToString() : strYear;

                return View();
            }
        }

        public IActionResult Register()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult IndexTest()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
