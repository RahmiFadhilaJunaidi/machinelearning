using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;

namespace MachineLearningMinatBakat.Controllers
{
 public class TrxSoalController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/TrxSoal.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new TrxSoalBase();
         try
         {
             var service = new TrxSoalService();
             result = service.TrxSoalGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.TrxSoalList });
     }


     [HttpPost]
     public IActionResult Create(TrxSoal Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxSoalService();
             respon.errorMessage = service.TrxSoalCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(TrxSoal Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxSoalService();
             respon.errorMessage = service.TrxSoalUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(TrxSoal Post)
     {
         var respon = new Respon();
         try
         {
             var service = new TrxSoalService();
             respon.errorMessage = service.TrxSoalDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
