﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MachineLearningMinatBakat.Controllers
{
    public static class HelperController
    {
        // 1 Error system
        // 2 Warning
        public static bool CheckError(string errorMessage)
        {
            return errorMessage == "" ? true : false;
        }

        public static bool CheckError(int errorType)
        {
            return errorType == 0 ? true : false;
        }

        public static int ErrorTpe(string errorMessage)
        {
            if (errorMessage=="")
                return 0;
            else if (errorMessage.ToLower().Contains("error :"))
                return 1;
            else
                return 2;
        }
    }
}
