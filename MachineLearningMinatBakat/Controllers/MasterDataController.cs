﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MachineLearningMinatBakat.DA.Models.MasterData;
using MachineLearningMinatBakat;
using Microsoft.AspNetCore.Mvc;
using Models.MachineLearningMinatBakat;
using MachineLearningMinatBakat.DA.Service.MasterData;

namespace MachineLearningMinatBakat.Controllers
{
    public class MasterDataController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        //public IActionResult User()
        //{
        //    return View();
        //}

        public IActionResult Jurusan()
        {
            return View();
        }
        public IActionResult MetodePakar()
        {
            return View();
        }
        public IActionResult CategoryKecerdasan()
        {
            return View();
        }

        public IActionResult MstUser()
        {
            return View();
        }

        public IActionResult UserList()
        {
            return View();
        }

        public IActionResult MstSoal()
        {
            return View();
        }

        public IActionResult MstMappingKecerdasan()
        {
            return View();
        }
        public IActionResult MstJurusanCategory()
        {
            return View();
        }
        public IActionResult Universitas()
        {
            return View();
        }

        public IActionResult Karir()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(mstUser Post)
        {
            var respon = new Respon();
            try
            {
                var service = new MstDataService();
                respon.errorMessage = service.MstDataUserCreate(Post);
                respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon});
        }

        [HttpPost]
        public IActionResult CekDataUser(mstUser Post)
        {
            var respon = new Respon();
            var userName = "";
            try
            {
                var service = new MstDataService();
                userName = service.CekDataUser(Post);
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = userName });
        }

        [HttpGet]
        public IActionResult GetDataList()
        {
            var respon = new Respon();
            var result = new MstUserBase();
            try
            {
                var service = new MstDataService();
                result = service.mstUserGetList();
                respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon, data = result.MstUserList });
        }
    }
}
