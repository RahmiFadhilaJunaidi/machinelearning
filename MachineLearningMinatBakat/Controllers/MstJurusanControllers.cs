using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;
using LibrarySys.DA.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace MachineLearningMinatBakat.Controllers
{
 public class MstJurusanController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/MstJurusan.cshtml");
     }

        [HttpGet]
        public IActionResult GetDataList()
        {
            var respon = new Respon();
            var result = new MstJurusanBase();
            try
            {
                var service = new MstJurusanService();
                result = service.MstJurusanGetList();
                respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon, data = result.MstJurusanList });
        }


        [HttpPost]
     public IActionResult Create(MstJurusan Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstJurusanService();
             respon.errorMessage = service.MstJurusanCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstJurusan Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstJurusanService();
             respon.errorMessage = service.MstJurusanUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstJurusan Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstJurusanService();
             respon.errorMessage = service.MstJurusanDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }

        [HttpGet]
        public IActionResult DdlJurusan()
        {
            var respon = new Respon();
            var result = new DropDownListModelBase();

            try
            {
                var service = new MstJurusanService();
                result = service.GetJurusanListNotExist();
                respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon, data = result.dropDownListModelList });
        }


        private readonly ILogger<MstJurusanController> _logger;

        private readonly IWebHostEnvironment _env;

        public MstJurusanController(ILogger<MstJurusanController> logger, IWebHostEnvironment env)
        {
            _logger = logger;
            _env = env;
        }

        private string EnsureCorrectFilename(string filename)
        {
            if (filename.Contains("\\"))
                filename = filename.Substring(filename.LastIndexOf("\\") + 1);

            return filename;
        }

        private string GetPathDirectoryAndFilename(string dir, string filename)
        {
            var directory = this._env.WebRootPath + dir + filename;
            return this._env.WebRootPath + dir + filename;
        }


        [HttpPost]
        public async Task<IActionResult> UploadFile(List<IFormFile> File)
        {
            var respon = new Respon();
            try
            {
                var service = new MstJurusanService();
                MstJurusan post = new MstJurusan();

                post.jurusan = HttpContext.Request.Form["jurusan"];
                post.kodeJurusanCategory = HttpContext.Request.Form["kodeJurusanCategory"];


                foreach (IFormFile source in File)
                {
                    string filename = ContentDispositionHeaderValue.Parse(source.ContentDisposition).FileName.Trim('"');
                    string contentType = source.ContentType;

                    filename = this.EnsureCorrectFilename(filename);

                    //System.IO.Directory.CreateDirectory(this._env.WebRootPath + "/object/UserFiles/");


                    var dir = "/object/" + post.jurusan;

                    if (!System.IO.Directory.Exists(this._env.WebRootPath + dir))
                    {
                        System.IO.Directory.CreateDirectory(this._env.WebRootPath + dir);
                    }

                    using (FileStream output = System.IO.File.Create(this.GetPathDirectoryAndFilename(dir, filename)))
                        //(FileStream output = System.IO.File.Create(this.GetPathAndFilename(filename)))
                        await source.CopyToAsync(output);

                    post.objectPath = this.GetPathDirectoryAndFilename(dir, filename).Replace(this._env.WebRootPath, ""); //this.GetPathAndFilename(filename);
                    post.contentType = source.ContentType;
                    post.File = filename;
                    Create(post);
                }

                //return this.View();
                return Ok(new { respon = respon });
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon });
        }
    }
}
