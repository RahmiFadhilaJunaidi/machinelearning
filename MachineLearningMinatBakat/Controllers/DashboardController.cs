﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using FastMember;
using LibrarySysDA.Models.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.MachineLearningMinatBakat;
using OfficeOpenXml;

namespace MachineLearningMinatBakat.Controllers
{
    //[Route("{action=Index}")]

    public class DashboardController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Home()
        {
            return View();
        }

        public IActionResult DataAnalitics()
        {
            return View();
        }

        public IActionResult Dataset()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetDatasetList()
        {
            var respon = new Respon();
            var result = new idxDatasetBase();
            try
            {
                var service = new DatasetService();
                result = service.vwDatasetGetList();
                respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon, data = result.IdxDatasetList });
        }

        [HttpGet]
        public IActionResult GetDataList()
        {
            var respon = new Respon();
            var result = new vwAccuracyBase();
            try
            {
                var service = new vwDataAnaliticsService();
                result = service.vwAccuracyGetList();
                respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon, data = result.vwAccuracyList });
        }

        [HttpGet]
        public IActionResult GetAccuracyDataList()
        {
            var respon = new Respon();
            var result = new vwAccuracyDataBase();
            try
            {
                var service = new vwDataAnaliticsService();
                result = service.vwAccuracyDataGetList();
                respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon, data = result.vwAccuracyDataList });
        }

        [HttpGet]
        public IActionResult GetDataListAnalitics()
        {
            var respon = new Respon();
            var result = new vwAnaliticsBase();
            try
            {
                var service = new vwDataAnaliticsService();
                result = service.vwAnaliticsGetList();
                respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon, data = result.vwAnaliticsList });
        }

        [HttpGet]
        public IActionResult GetDetailCategoryUtilizedList(string KodeCategory)
        {
            var respon = new Respon();
            var result = new vwDetailChartCategoryUtilizedBase();
            try
            {
                var service = new vwDataAnaliticsService();
                result = service.vwDetailCategoryUtilizedGetList(KodeCategory);
                respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon, data = result.VwDetailChartCategoryUtilizedList });
        }

        [HttpGet]
        public IActionResult GetDetailAccuracyList(string metode)
        {
            var respon = new Respon();
            var result = new trxPrediksiTestBase();
            try
            {
                var service = new vwDataAnaliticsService();
                result = service.vwDetailChartAccuracy(metode);
                respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon, data = result.trxPrediksiTestList });
        }


        [Route("Dashboard/ExportCategoryUtilized/{kodeCategory}")]

        public IActionResult ExportExcel(string kodeCategory)
        {
            var service = new vwDataAnaliticsService();
            var data = new List<vwDetailChartCategoryUtilized>();

            var filename = "DataAnalitics";
            data = service.exportCategoryUtilizedGetList(kodeCategory);

            using (var workbook = new XLWorkbook()) 
            {
                var worksheet = workbook.Worksheets.Add(filename);
                var currentRow = 1;

                #region Header
                worksheet.Cell(currentRow, 1).Value = "nama";
                worksheet.Cell(currentRow, 2).Value = "Setuju";
                worksheet.Cell(currentRow, 3).Value = "KurangSetuju";
                worksheet.Cell(currentRow, 4).Value = "TidakSetuju";
                worksheet.Cell(currentRow, 5).Value = "SangatTidakSetuju";
                worksheet.Cell(currentRow, 6).Value = "KodeCategory";

                #endregion

                #region Body
                foreach (var result in data)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = result.nama;
                    worksheet.Cell(currentRow, 2).Value = result.Setuju;
                    worksheet.Cell(currentRow, 3).Value = result.KurangSetuju;
                    worksheet.Cell(currentRow, 4).Value = result.TidakSetuju;
                    worksheet.Cell(currentRow, 5).Value = result.SangatTidakSetuju;
                    worksheet.Cell(currentRow, 6).Value = result.KodeCategory;

                }

                #endregion

                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.Headers.Add("Content-Disposition", "attachment; filename=Dataanalitics.xls");

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    stream.Close();
                    return File(
                        content,
                        Response.ContentType,
                        "Dataanalitics.xls"
                        );
                }


            }

        }

        [Route("Dashboard/ExportAccuracyDetail/{metode}")]

        public IActionResult ExportAccuracyDetail(string metode)
        {
            var service = new vwDataAnaliticsService();
            var data = new List<trxPrediksiTest>();

            var filename = "DataAnaliticsAccuracy";
            data = service.exportDetailChartAccuracy(metode);

            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add(filename);
                var currentRow = 1;

                #region Header
                worksheet.Cell(currentRow, 1).Value = "Nama";
                worksheet.Cell(currentRow, 2).Value = "Metode";
                worksheet.Cell(currentRow, 3).Value = "Prediksi";
                worksheet.Cell(currentRow, 4).Value = "Accuracy";
                
                #endregion

                #region Body
                foreach (var result in data)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = result.userID;
                    worksheet.Cell(currentRow, 2).Value = result.metode;
                    worksheet.Cell(currentRow, 3).Value = result.prediksi;
                    worksheet.Cell(currentRow, 4).Value = result.accuracy;

                }

                #endregion

                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.Headers.Add("Content-Disposition", "attachment; filename=DataanaliticsAccuracy.xls");

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    stream.Close();
                    return File(
                        content,
                        Response.ContentType,
                        "DataanaliticsAccuracy.xls"
                        );
                }


            }

        }

        [Route("Dashboard/ExportDataset")]

        public IActionResult ExportDataset()
        {
            var service = new DatasetService();
            var data = new idxDatasetBase();

            var filename = "DataSet";
            data = service.vwDatasetGetList();

            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add(filename);
                var currentRow = 1;

                #region Header
                worksheet.Cell(currentRow, 1).Value = "Nama";
                worksheet.Cell(currentRow, 2).Value = "KodeCategory";
                worksheet.Cell(currentRow, 3).Value = "SangatTidakSetuju";
                worksheet.Cell(currentRow, 4).Value = "TidakSetuju";
                worksheet.Cell(currentRow, 5).Value = "KurangSetuju";
                worksheet.Cell(currentRow, 6).Value = "Setuju";


                #endregion

                #region Body
                foreach (var result in data.IdxDatasetList)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = result.nama;
                    worksheet.Cell(currentRow, 2).Value = result.KodeCategory;
                    worksheet.Cell(currentRow, 3).Value = result.SangatTidakSetuju;
                    worksheet.Cell(currentRow, 4).Value = result.TidakSetuju;
                    worksheet.Cell(currentRow, 5).Value = result.KurangSetuju;
                    worksheet.Cell(currentRow, 6).Value = result.Setuju;
                    

                }

                #endregion

                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.Headers.Add("Content-Disposition", "attachment; filename=DataSet.xls");

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    stream.Close();
                    return File(
                        content,
                        Response.ContentType,
                        "DataSet.xls"
                        );
                }


            }

        }
    }
}


