using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;

namespace MachineLearningMinatBakat.Controllers
{
 public class MstUserController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/MstUser.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstUserBase();
         try
         {
             var service = new mstUserService();
             result = service.mstUserGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.VwUserList });
     }


     [HttpPost]
     public IActionResult Create(mstUser Post)
     {
         var respon = new Respon();
         try
         {
             var service = new mstUserService();
             respon.errorMessage = service.mstUserCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(mstUser Post)
     {
         var respon = new Respon();
         try
         {
             var service = new mstUserService();
             respon.errorMessage = service.mstUserUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(mstUser Post)
     {
         var respon = new Respon();
         try
         {
             var service = new mstUserService();
             respon.errorMessage = service.mstUserDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
