using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;
using LibrarySys.DA.Models;

namespace MachineLearningMinatBakat.Controllers
{
 public class MstSoalController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/MstSoal.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstSoalBase();
         try
         {
             var service = new MstSoalService();
             result = service.MstSoalGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstSoalList });
     }


     [HttpPost]
     public IActionResult Create(MstSoal Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstSoalService();
             respon.errorMessage = service.MstSoalCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstSoal Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstSoalService();
             respon.errorMessage = service.MstSoalUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }

     [HttpGet]
     public IActionResult Delete(MstSoal Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstSoalService();
             respon.errorMessage = service.MstSoalDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
        
        [HttpGet]
        public IActionResult DdlSoal(String KodeCategory)
        {
            var respon = new Respon();
            var result = new DropDownListModelBase();

            try
            {
                var service = new MstSoalService();
                result = service.GetSoal(KodeCategory);
                respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon, data = result.dropDownListModelList });
        }
    }
}
