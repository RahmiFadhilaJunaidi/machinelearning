using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;

namespace MachineLearningMinatBakat.Controllers
{
 public class mstKampusController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/mstKampus.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new mstKampusBase();
         try
         {
             var service = new mstKampusService();
             result = service.mstKampusGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.mstKampusList });
     }


     [HttpPost]
     public IActionResult Create(mstKampus Post)
     {
         var respon = new Respon();
         try
         {
             var service = new mstKampusService();
             respon.errorMessage = service.mstKampusCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }

        private readonly ILogger<mstKampusController> _logger;

        private readonly IWebHostEnvironment _env;

        public mstKampusController(ILogger<mstKampusController> logger, IWebHostEnvironment env)
        {
            _logger = logger;
            _env = env;
        }

        private string EnsureCorrectFilename(string filename)
        {
            if (filename.Contains("\\"))
                filename = filename.Substring(filename.LastIndexOf("\\") + 1);

            return filename;
        }

        private string GetPathDirectoryAndFilename(string dir, string filename)
        {
            var directory = this._env.WebRootPath + dir + filename;
            return this._env.WebRootPath + dir + filename;
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(List<IFormFile> File)
        {
            var respon = new Respon();
            try
            {
                var service = new mstKampusService();
                mstKampus post = new mstKampus();

                post.namaKampus = HttpContext.Request.Form["namaKampus"];
                post.website = HttpContext.Request.Form["website"];
                post.email = HttpContext.Request.Form["email"];
                post.telp = HttpContext.Request.Form["telp"];
                post.deskripsi = HttpContext.Request.Form["deskripsi"];

                //var objects = service.MstObjectGetById(post.MstObjectId);
                //var objCategory = service.MstCategoryGetById(post.ObjectCategoryId);
                //var objectName = "";
                //if (objects.MstObject.ObjectParentId == 0)
                //{
                //    objectName = objects.MstObject.ObjectName;
                //}
                //else
                //{
                //    var objectsParent = service.MstObjectGetById(objects.MstObject.ObjectParentId);
                //    objectName = objectsParent.MstObject.ObjectName;
                //}

                //var categoryName = objCategory.MstObjectCategory.ObjectCategory;

                foreach (IFormFile source in File)
                {
                    string filename = ContentDispositionHeaderValue.Parse(source.ContentDisposition).FileName.Trim('"');
                    string contentType = source.ContentType;

                    filename = this.EnsureCorrectFilename(filename);

                    //System.IO.Directory.CreateDirectory(this._env.WebRootPath + "/object/UserFiles/");


                    var dir = "/object/" + post.namaKampus;

                    if (!System.IO.Directory.Exists(this._env.WebRootPath + dir))
                    {
                        System.IO.Directory.CreateDirectory(this._env.WebRootPath + dir);
                    }

                    using (FileStream output = System.IO.File.Create(this.GetPathDirectoryAndFilename(dir, filename)))
                        //(FileStream output = System.IO.File.Create(this.GetPathAndFilename(filename)))
                        await source.CopyToAsync(output);

                    post.objectPath = this.GetPathDirectoryAndFilename(dir, filename).Replace(this._env.WebRootPath, ""); //this.GetPathAndFilename(filename);
                    post.contentType = source.ContentType;
                    post.File = filename;
                    Create(post);
                }

                //return this.View();
                return Ok(new { respon = respon });
            }
            catch (Exception ex)
            {
                respon.errorMessage = respon.errorController + ex.Message;
                respon.errorType = 1;
            }
            return Ok(new { respon = respon });
        }

        [HttpPost]
     public IActionResult Update(mstKampus Post)
     {
         var respon = new Respon();
         try
         {
             var service = new mstKampusService();
             respon.errorMessage = service.mstKampusUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(mstKampus Post)
     {
         var respon = new Respon();
         try
         {
             var service = new mstKampusService();
             respon.errorMessage = service.mstKampusDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
