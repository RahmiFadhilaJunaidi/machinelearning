using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;
using LibrarySys.DA.DBContext;

namespace MachineLearningMinatBakat.Controllers
{
 public class vwMappingKecerdasanController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/vwMappingKecerdasan.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new vwMappingKecerdasanBase();
         try
         {
             var service = new vwMappingKecerdasanService();
             result = service.vwMappingKecerdasanGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.vwMappingKecerdasanList });
     }


     [HttpPost]
     public IActionResult Create(vwMappingKecerdasan Post)
     {
         var respon = new Respon();
         try
         {
             var service = new vwMappingKecerdasanService();
             respon.errorMessage = service.vwMappingKecerdasanCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(vwMappingKecerdasan Post)
     {
         var respon = new Respon();
         try
         {
             var service = new vwMappingKecerdasanService();
             respon.errorMessage = service.vwMappingKecerdasanUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(vwMappingKecerdasan Post)
     {
         var respon = new Respon();
         try
         {
             var service = new vwMappingKecerdasanService();
             respon.errorMessage = service.vwMappingKecerdasanDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
