﻿using MachineLearningMinatBakat.DA.Service;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.Provider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Models.MachineLearningMinatBakat;
using System;

namespace MachineLearningMinatBakat.Controllers
{
    //[RoutePrefix("Login")]
    public class LoginController : Controller
    {
        //[AllowAnonymous]
        //[Route("Index")]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Home", "Dashboard");
            }
            else
            {
                string strYear = "2017";
                ViewBag.CopyrightYear = DateTime.Now.Year.ToString() != strYear ? strYear + "-" + DateTime.Now.Year.ToString() : strYear;

                return View();
            }
        }

        [HttpPost]
        //[Route("Login")]
        public ActionResult Login(vmLogin login)
        {
            login.UserName = login.UserName.Trim();
            //login.IP = WebHelper.GetIPAddress();

            vmLogin userLogin = new vmLogin();
            var respon = new Respon();

            

            ViewBag.data = HttpContext.Session.GetString("UserName"); ;

            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Home", "Dashboard");
            }
            else
            {
                userLogin = AccessService.LoginValidation(login);
                if (!string.IsNullOrWhiteSpace(userLogin.ErrorMessage))
                {
                    //return Json(userLogin);
                    return Ok(new { respon = userLogin });
                }
                //else
                //{
                //    if (UserManager.ValidateUser(login, Response))
                //    {
                //        //return Json(userLogin);
                //        return Ok(new { respon = userLogin });
                //    }
                //    else
                //    {
                //        userLogin.ErrorMessage = "Username or Password is incorrect!";
                //        //return Json(userLogin);
                //        return Ok(new { respon = userLogin });
                //    }
                //}

                if (userLogin.UserName == login.UserName)
                {
                    //return Json(userLogin);
                    var service = new mstUserService();
                    ViewBag.UserName = login.UserName;
                    //var remoteIpAddress = HttpContext.GetFeature<IHttpConnectionFeature>()?.RemoteIpAddress;
                    var data = service.mstUserLogin(userLogin.UserID);

                    HttpContext.Session.SetString("UserName", login.UserName);
                    return Ok(new { respon = userLogin });
                }
                else
                {
                    userLogin.ErrorMessage = "Username or Password is incorrect!";
                    userLogin.ErrorType = 1;
                    //return Json(data = userLogin);
                    return Ok(new { respon = userLogin });
                }

            }
        }


        //[AllowAnonymous]
        //[Route("Logout")]
        public ActionResult Logout()
        {
            //UserManager.Logout(Session, Response);
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}
