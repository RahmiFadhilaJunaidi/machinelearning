using System;
using Microsoft.AspNetCore.Mvc;
using MechineLearningMinatBakat.DA.Service.MasterData;
using MechineLearningMinatBakat.DA.Models.MasterData;
using MachineLearningMinatBakat;
using MachineLearningMinatBakat.Controllers;
using Models.MachineLearningMinatBakat;

namespace MechineLearningMinatBakat.Controllers
{
 public class MstMetodePakarController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/MstMetodePakar.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new MstMetodePakarBase();
         try
         {
             var service = new MstMetodePakarService();
             result = service.MstMetodePakarGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.MstMetodePakarList });
     }


     [HttpPost]
     public IActionResult Create(MstMetodePakar Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstMetodePakarService();
             respon.errorMessage = service.MstMetodePakarCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(MstMetodePakar Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstMetodePakarService();
             respon.errorMessage = service.MstMetodePakarUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(MstMetodePakar Post)
     {
         var respon = new Respon();
         try
         {
             var service = new MstMetodePakarService();
             respon.errorMessage = service.MstMetodePakarDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
