using System;
using Microsoft.AspNetCore.Mvc;
using MachineLearningMinatBakat.DA.Service.MasterData;
using MachineLearningMinatBakat.DA.Models.MasterData;
using Models.MachineLearningMinatBakat;

namespace MachineLearningMinatBakat.Controllers
{
 public class mstKarirController : Controller
 {
     public IActionResult Index()
     {
         return View("~/Views/MasterData/mstKarir.cshtml");
     }

    [HttpGet]
     public IActionResult GetDataList()
     {
         var respon = new Respon();
         var result = new mstKarirBase();
         try
         {
             var service = new mstKarirService();
             result = service.mstKarirGetList();
             respon.errorType = HelperController.CheckError(result.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
             respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon, data = result.mstKarirList });
     }


     [HttpPost]
     public IActionResult Create(mstKarir Post)
     {
         var respon = new Respon();
         try
         {
             var service = new mstKarirService();
             respon.errorMessage = service.mstKarirCreate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }


     [HttpPost]
     public IActionResult Update(mstKarir Post)
     {
         var respon = new Respon();
         try
         {
             var service = new mstKarirService();
             respon.errorMessage = service.mstKarirUpdate(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + ex.Message;
             respon.errorType = 1;
          }
          return Ok(new { respon = respon });
     }


     [HttpGet]
     public IActionResult Delete(mstKarir Post)
     {
         var respon = new Respon();
         try
         {
             var service = new mstKarirService();
             respon.errorMessage = service.mstKarirDelete(Post);
             respon.errorType = HelperController.CheckError(respon.errorMessage) ? 0 : 1;
         }
         catch (Exception ex)
         {
            respon.errorMessage = respon.errorController  + respon.errorController  + ex.Message;
             respon.errorType = 1;
         }
         return Ok(new { respon = respon });
     }
  }
}
