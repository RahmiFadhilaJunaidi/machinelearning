﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MachineLearningMinatBakat
{
    public class Respon
    {
        public string errorMessage { get; set; }
        public int errorType { get; set; }

        public string errorController { get; set; }
        public Respon()
        {
            errorMessage = string.Empty;
            errorType = 0;
            errorController = "error controller : ";
        }
    }
}
