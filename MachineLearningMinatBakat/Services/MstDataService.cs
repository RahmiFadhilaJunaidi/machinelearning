﻿using LibrarySys.DA;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MachineLearningMinatBakat.Services
{
    public class MstDataService
    {
        BaseModel baseModel = new BaseModel();
        public string MstDataUserCreate(MstUser Post)
        {
            try
            {
                using (var context = new DBContext())
                {
                    context.MstUser.Add(Post);
                    context.SaveChanges();
                    context.Dispose();
                }
                return "";
            }
            catch (Exception ex)
            {
                return baseModel + ex.Message;
            }
        }
    }
}
