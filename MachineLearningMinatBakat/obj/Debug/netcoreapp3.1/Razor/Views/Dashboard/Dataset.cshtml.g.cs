#pragma checksum "D:\KULIAH\PROJECT TA\SISTEM PAKAR\MachineLearningMinatBakat\MachineLearningMinatBakat\Views\Dashboard\Dataset.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1baead59a4a94604f83ca532ce60d384a34be570"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Dashboard_Dataset), @"mvc.1.0.view", @"/Views/Dashboard/Dataset.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\KULIAH\PROJECT TA\SISTEM PAKAR\MachineLearningMinatBakat\MachineLearningMinatBakat\Views\_ViewImports.cshtml"
using MachineLearningMinatBakat;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\KULIAH\PROJECT TA\SISTEM PAKAR\MachineLearningMinatBakat\MachineLearningMinatBakat\Views\_ViewImports.cshtml"
using MachineLearningMinatBakat.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1baead59a4a94604f83ca532ce60d384a34be570", @"/Views/Dashboard/Dataset.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2c0da3c4a20e5e2a227a0cebe5292acac9daddd7", @"/Views/_ViewImports.cshtml")]
    public class Views_Dashboard_Dataset : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/Common.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/Dashboard/Dataset.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "D:\KULIAH\PROJECT TA\SISTEM PAKAR\MachineLearningMinatBakat\MachineLearningMinatBakat\Views\Dashboard\Dataset.cshtml"
  
    ViewBag.Title = "Dataset";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1baead59a4a94604f83ca532ce60d384a34be5704243", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1baead59a4a94604f83ca532ce60d384a34be5705282", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"

<div class=""m-content pnlHeader"">
    <div class=""row"">
        <div class=""col-lg-12"">
            <div class=""m-portlet"">
                <div class=""m-portlet__head"">
                    <div class=""m-portlet__head-caption"">
                        <div class=""m-portlet__head-title"">
                            <h3 class=""m-portlet__head-text"">
                                Dataset List
                            </h3>
                        </div>
                    </div>
                </div>

                <div class=""m-portlet__body"">
                    <!--begin: Search Form -->
                    <div class=""m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30"">
                        <div class=""row align-items-center"">
                            <div class=""col-xl-8 order-2 order-xl-1"">
                                <div class=""form-group m-form__group row align-items-center"">
                                    <div class=""col-md-5"">
       ");
            WriteLiteral(@"                                 <div class=""m-form__group m-form__group--inline"">
                                            <div class=""m-form__label"">
                                                <label class=""m-label m-label--single"">
                                                    <span>
                                                        Search:
                                                    </span>
                                                </label>
                                            </div>
                                            <div class=""m-form__control"">
                                                <select class=""form-control m-bootstrap-select"" id=""slsProjectManager""></select>
                                            </div>
                                        </div>
                                        <div class=""d-md-none m--margin-bottom-10""></div>
                                    </div>
                                    <div c");
            WriteLiteral(@"lass=""col-md-5"">
                                        <div class=""m-input-icon m-input-icon--left"">
                                            <input type=""text"" class=""form-control m-input"" placeholder=""Search..."" id=""tbxSearch"">
                                            <span class=""m-input-icon__icon m-input-icon__icon--left"">
                                                <span>
                                                    <i class=""la la-search""></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=""col-xl-4 order-1 order-xl-2 m--align-right"">
                                <button class=""btn btn-warning"" type=""button"" id=""btndownload""><i class=""la la-download""></i> Excel</button>
");
            WriteLiteral(@"                                <div class=""m-separator m-separator--dashed d-xl-none""></div>
                            </div>
                        </div>
                    </div>
                    <!--end: Search Form -->
                    <!--begin: Datatable -->
                    
                    <div class=""m_datatable"" id=""divDatasetList"">

                    </div>


                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>
</div>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
