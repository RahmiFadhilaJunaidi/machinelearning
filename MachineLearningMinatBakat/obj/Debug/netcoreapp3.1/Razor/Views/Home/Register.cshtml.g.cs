#pragma checksum "D:\KULIAH\PROJECT TA\SISTEM PAKAR\MachineLearningMinatBakat\MachineLearningMinatBakat\Views\Home\Register.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "427fadc2f3991de7a8552af01abf41e2a7e23925"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Register), @"mvc.1.0.view", @"/Views/Home/Register.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\KULIAH\PROJECT TA\SISTEM PAKAR\MachineLearningMinatBakat\MachineLearningMinatBakat\Views\_ViewImports.cshtml"
using MachineLearningMinatBakat;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\KULIAH\PROJECT TA\SISTEM PAKAR\MachineLearningMinatBakat\MachineLearningMinatBakat\Views\_ViewImports.cshtml"
using MachineLearningMinatBakat.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"427fadc2f3991de7a8552af01abf41e2a7e23925", @"/Views/Home/Register.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2c0da3c4a20e5e2a227a0cebe5292acac9daddd7", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Register : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("m-form m-form--fit m-form--label-align-right"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("MstUserForm"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "D:\KULIAH\PROJECT TA\SISTEM PAKAR\MachineLearningMinatBakat\MachineLearningMinatBakat\Views\Home\Register.cshtml"
  
    ViewBag.Title = "Register";
    Layout = "~/Views/Shared/_LayoutLogin.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n");
            WriteLiteral(@"
<div class=""m-content"">
    <div class=""row"">
        <div class=""col-lg-12"">
            <div class=""m-portlet"">
                <div class=""m-portlet__head"">
                    <div class=""m-portlet__head-caption"">
                        <div class=""m-portlet__head-title"">
                            <span class=""m-portlet__head-icon m--hide"">
                                <i class=""la la-gear""></i>
                            </span>
                            <h3 class=""m-portlet__head-text"">
                                Create Account
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "427fadc2f3991de7a8552af01abf41e2a7e239255161", async() => {
                WriteLiteral(@"

                    <div class=""m-form__content"">
                        <div class=""m-alert m-alert--icon alert alert-danger m--hide"" role=""alert"" id=""msgLogFail"">
                            <div class=""m-alert__icon"">
                                <i class=""la la-warning""></i>
                            </div>
                            <div class=""m-alert__text"">
                                Oh sorry! Please check your form Log again :).
                            </div>
                            <div class=""m-alert__close"">
                                <button type=""button"" class=""close"" data-close=""alert"" aria-label=""Close""></button>
                            </div>
                        </div>
                    </div>

                    <div class=""m-portlet__body"">

                        <div class=""form-group m-form__group row"">
                            <label class=""col-form-label col-lg-4 col-sm-12"">
                                Full Name <strong ");
                WriteLiteral(@"style=""color:red"" ;>*</strong>:
                            </label>
                            <div class=""col-lg-6"">
                                <input type=""text"" class=""form-control m-input"" name=""Nama"" id=""Nama"" required />
                            </div>
                        </div>
                        <div class=""form-group m-form__group row"">
                            <label class=""col-form-label col-lg-4 col-sm-12"">
                                User Name <strong style=""color:red"" ;>*</strong>:
                            </label>
                            <div class=""col-lg-6"">
                                <input type=""text"" class=""form-control m-input"" name=""UserName"" id=""UserName"" required />
                            </div>
                        </div>
                        <div class=""form-group m-form__group row"">
                            <label class=""col-form-label col-lg-4 col-sm-12"">
                                Tempat Lahir <strong style=""");
                WriteLiteral(@"color:red"" ;>*</strong>:
                            </label>
                            <div class=""col-lg-6"">
                                <input type=""text"" class=""form-control m-input"" name=""TempatLahir"" id=""TempatLahir"" required />
                            </div>
                        </div>
                        <div class=""form-group m-form__group row"">
                            <label class=""col-form-label col-lg-4 col-sm-12"">
                                Tgl Lahir <strong style=""color:red"" ;>*</strong>:
                            </label>
                            <div class=""col-lg-6"">
                                <div class=""input-group date"">
                                    <input type=""text"" class=""form-control m-input datepicker"" name=""TglLahir"" readonly id=""TglLahir"" />
                                    <div class=""input-group-append"">
                                        <span class=""input-group-text"">
                                            <i");
                WriteLiteral(@" class=""la la-calendar""></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=""form-group m-form__group row"">
                            <label class=""col-form-label col-lg-4 col-sm-12"">
                                Alamat <strong style=""color:red"" ;>*</strong>:
                            </label>
                            <div class=""col-lg-6"">
                                <textarea type=""text"" class=""form-control m-input"" name=""Alamat"" id=""Alamat"" rows=""4""></textarea>
                            </div>
                        </div>
                        <div class=""form-group m-form__group row"">
                            <label class=""col-form-label col-lg-4 col-sm-12"">
                                Email <strong style=""color:red"" ;>*</strong>:
                            </label>
             ");
                WriteLiteral(@"               <div class=""col-lg-6"">
                                <input type=""email"" class=""form-control m-input"" name=""Email"" id=""Email"" required />
                            </div>
                        </div>
                        <div class=""form-group m-form__group row"">
                            <label class=""col-form-label col-lg-4 col-sm-12"">
                                No Telp <strong style=""color:red"" ;>*</strong>:
                            </label>
                            <div class=""col-lg-6"">
                                <input type=""tel"" class=""form-control m-input"" maxlength=""13"" name=""NoTelp"" id=""NoTelp"" required />
                            </div>
                        </div>
                        <div class=""form-group m-form__group row"">
                            <label class=""col-form-label col-lg-4 col-sm-12"">
                                Password <strong style=""color:red"" ;>*</strong>:
                            </label>
             ");
                WriteLiteral(@"               <div class=""col-lg-6"">
                                <input type=""password"" class=""form-control m-input"" name=""Password"" id=""Password"" required />
                            </div>
                        </div>
                        <div class=""form-group m-form__group row"">
                            <label class=""col-form-label col-lg-4 col-sm-12"">
                                Confirm Password <strong style=""color:red"" ;>*</strong>:
                            </label>
                            <div class=""col-lg-6"">
                                <input type=""password"" class=""form-control m-input"" name=""tbxConfirmPassword"" id=""tbxConfirmPassword"" required />
                            </div>
                        </div>
                    </div>
                    <div class=""m-portlet__foot m-portlet__foot--fit"">
                        <div class=""m-form__actions m-form__actions"">
                            <div class=""row"">
                              ");
                WriteLiteral(@"  <div class=""col-lg-9 ml-lg-auto"">
                                    <button onclick=""JavaScript: window.history.back(1); return false;"" class=""btn btn-secondary"">
                                        Cancel
                                    </button>
                                    <button id=""btnRegister"" type=""submit"" class=""btn btn-success"">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                <!--end::Form-->\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n</div>\r\n\r\n\r\n");
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
