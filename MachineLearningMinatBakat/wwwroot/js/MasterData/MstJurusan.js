
jQuery(document).ready(function () {
 Table.InitJurusan();
 //Table.LoadTable();
 Helper.PanelHeaderShow();
 //Helper.InitButtons();
    
    $(".pnlDetail").hide();
    $('#IdJurusan').hide();
    $('.dropify').dropify();

    $("#btnAddData").click(function() {
        Helper.PanelDetailShow();
        Dropdownlist.ddlKategoriJurusan();
        Dropdownlist.ddlKecerdasan();
        
        $('#btnUpdate').hide();
        $('#btnCreate').show();
    });

    $("#btnCancel").click(function () {
        Helper.PanelHeaderShow();

    });

    $("#btnCreate").click(function () {
        Form.SubmitJurusan();
    });

    $("#btnUpdate").click(function () {
        Form.UpdateJurusan();
        $('#btnUpdate').show();
        $('#btnCreate').hide();
    });

});

var Form = {
    SubmitJurusan: function () {

        var data = {
            IdJurusan: $('#IdJurusan').val(),
            KodeJurusan: $('#KodeJurusan').val(),
            Jurusan: $('#Jurusan').val(),
            CreatedBy: $('#CreatedBy').val(),
            CreatedDate: $('#CreatedDate').val(),
            UpdatedBy: $('#UpdatedBy').val(),
            UpdateDate: $('#UpdateDate').val(),
            KodeJurusanCategory: $('#slsCategoryJurusan').val()
        };


        var fileInput = document.getElementById("fuEvidence");
        var fileInputPSB = $("#fuEvidence")[0].files[0];

        if (fileInput.files.length != 0) {
            fsFileName = fileInput.files[0].name;
            fsFile = fileInput.files[0];
            fsExtension = fsFileName.split('.').pop().toUpperCase();
        }

        var model = new FormData();
        model.append('File', fileInputPSB);
        model.append('jurusan', data.Jurusan);
        model.append('kodeJurusanCategory', data.KodeJurusanCategory);
     
        var result = Common.Form.SaveFile('/MstJurusan/UploadFile', model);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        //Helper.PanelHeaderShow();
        //Table.LoadTable();
         Table.InitJurusan();
         Table.ClearForm();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },

    UpdateJurusan: function () {
        var data = {
            IdJurusan: $('#IdJurusan').val(),
            Jurusan: $('#Jurusan').val(),
            KodeKecerdasanCategory: $("#slsKodeKecerdasan").val()
        };
        var result = Common.Form.Save('/MstJurusan/Update', data);
        if (Common.CheckError.Object(result)) {
            Common.Alert.Success('Data  has been updated.');
            Table.InitJurusan();
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    },

    DeleteJurusan: function (param) {
     var result = Common.Form.Delete('/MstJurusan/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
          Table.InitJurusan();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },

    ClearForm: function () {
      $('#IdJurusan').val('');
      $('#KodeJurusan').val('');
      $('#Jurusan').val('');
      $('#CreatedBy').val('');
      $('#CreatedDate').val('');
      $('#UpdatedBy').val('');
      $('#UpdateDate').val('');
    },
}

var FormValidation = {
    IdJurusan: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
    },
}

var Table = {
    InitJurusan: function () {
          //Common.Table.InitClient(tblMstJurusan);
        t = $("#divJurusanList").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: "/MstJurusan/GetDataList",
                        method: "GET",
                        map: function (r) {
                            var e = r;
                            return void 0 !== r.data && (e = r.data), e;
                        }
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                },
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false
            },
            layout: {
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            },
            search: {
                input: $("#tbxSearch")
            },
            columns: [
                {
                    field: "idJurusan", title: "Actions", sortable: false, textAlign: "center", template: function (t) {
                        console.log("t: ", t);
                        var strBuilder = '<a href="javascript:;' + t.idJurusan + '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" id="btnEdit" title="Edit details"  onclick="Table.UpdateData(' + t.idJurusan + ', \'' + t.jurusan + '\', \'' + t.kodeJurusanCategory + '\', \'' + t.kodeKecerdasanCategory + '\' );"><i class="la la-edit"></i></a>\t\t\t\t\t\t';
                        strBuilder += '<a id="deleteConfirmation" href="javascript:;' + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=Table.RemoveData(' + t.idJurusan + ')><i class="la la-eraser"></i></a>';
                        
                        return strBuilder;
                    }
                },
                { field: "kodeJurusan", title: "Kode Jurusan", textAlign: "center" },
                { field: "jurusan", title: "Jurusan", textAlign: "center" }
            ]
        })
    },

    RemoveData: function (id) {
            var param = {
                IdJurusan: id,
        };

        swal({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: true
        }).then((willDelete) => {
            if (willDelete.value) {
                console.log(willDelete.value);
                Form.DeleteJurusan(param);
                
                swal("Deleted!", "Plan has been Deleted.", "success");
            }
        })

    },

    UpdateData: function (id, jurusan, jurusanCategory, kodeKecerdasanCategory) {
        $('#IdJurusan').val(id);
        $('#Jurusan').val(jurusan);
        $('#btnUpdate').show();
        $('#btnCreate').hide();
        Helper.PanelDetailShow();
        Dropdownlist.ddlKategoriJurusan();
        Dropdownlist.ddlKecerdasan();
        $("#slsCategoryJurusan").val(jurusanCategory).trigger('change');
        $("#slsKodeKecerdasan").val(kodeKecerdasanCategory).trigger('change');
    },

}

var Dropdownlist = {
    ddlKategoriJurusan: function () {
        var result = Common.GetData.Get('/MstJurusanCategory/DdlJurusanCategory');
        if (Common.CheckError.Object(result)) {
            $("#slsCategoryJurusan").html('');
            $("#slsCategoryJurusan").append('<option value="0">-- select one-- </option>');
            Common.Helper.Dropdownlist("#slsCategoryJurusan", result.data);
            //console.log(result);
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    },
    ddlKecerdasan: function () {
        var result = Common.GetData.Get('/MstKecerdasanCategory/DdlKecerdasanCategory');
        if (Common.CheckError.Object(result)) {
            $("#slsKodeKecerdasan").html('');
            $("#slsKodeKecerdasan").append('<option value="0">-- select one-- </option>');
            //Common.Helper.Dropdownlist("#slsKodeKecerdasan", result.data);
            if (result.data.length > 0) {
                $.each(result.data, function (i, item) {
                    $("#slsKodeKecerdasan").append('<option value=' + item.value + '>' + '[' + item.value + '] - ' + item.text + '</option>');
                });
                $("#slsKodeKecerdasan").select2();
            }
            console.log(result);
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    },
}
