var tblMstMetodePakar='#tblMstMetodePakar';
jQuery(document).ready(function () {
 Table.InitMetode();
 //Table.LoadTable();
 Helper.PanelHeaderShow();
 //Helper.InitButtons();

    $('#IdMetode').hide();

    $("#btnAddData").click(function () {
        Helper.PanelDetailShow();
        $('#btnUpdate').hide();
        $('#btnCreate').show();

    });

    $("#btnCancel").click(function () {
        Helper.PanelHeaderShow();

    });

    $("#btnCreate").click(function () {
        Form.SubmitMetode();
    });

    $("#btnUpdate").click(function () {
        Form.UpdateMetode();
    });
});

var Form = {
    SubmitMetode: function () {
     var data = {
                Metode: $('#Metode').val(),
                Keterangan: $('#Keterangan').val()
        };

     var result = Common.Form.Save('/MstMetodePakar/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        //Table.LoadTable();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    DeleteMetode: function (param) {
     var result = Common.Form.Delete('/MstMetodePakar/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.InitMetode();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },

    UpdateMetode: function () {
        var data = {
            IdMetode: $('#IdMetode').val(),
            Metode: $('#Metode').val(),
            Keterangan: $('#Keterangan').val()
        };
        var result = Common.Form.Save('/MstMetodePakar/Update', data);
        if (Common.CheckError.Object(result)) {
            Common.Alert.Success('Data  has been updated.');
            Table.InitMetode();
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    },

    ClearForm: function () {
      $('#IdMetode').val('');
      $('#Metode').val('');
      $('#CreatedDate').val('');
      $('#CreatedBy').val('');
      $('#UpdatedDate').val('');
      $('#UpdatedBy').val('');
    },
}

var FormValidation = {
    IdMetode: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       InitMetode: function () {
          //Common.Table.InitClient(tblMstMetodePakar);
        t = $("#divMetodeList").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: "/MstMetodePakar/GetDataList",
                        method: "GET",
                        map: function (r) {
                            var e = r;
                            return void 0 !== r.data && (e = r.data), e;
                        }
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                },
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false
            },
            layout: {
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            },
            search: {
                input: $("#tbxSearch")
            },
            columns: [
                //{
                //    field: "idMetode", title: "Actions", sortable: false, textAlign: "center", template: function (t) {
                       
                //        var strBuilder = '<a href="javascript:;' + t.idMetode + '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" id="btnEdit" title="Edit details"  onclick="Table.UpdateData(' + t.idMetode + ', \'' + t.metode + '\');"><i class="la la-edit"></i></a>\t\t\t\t\t\t';
                //        strBuilder += '<a id="deleteConfirmation" href="javascript:;' + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=Table.RemoveData(' + t.idMetode + ')><i class="la la-eraser"></i></a>';

                //        return strBuilder;
                //    }
                //},
                { field: "metode", title: "Kode Jurusan", textAlign: "center" },
                { field: "keterangan", title: "Keterangan", textAlign: "center" },
                {
                    field: "createdDate", title: "Created Date", sortable: false, textAlign: "center", template: function (t) {
                        return t.createdDate != null ? Common.Format.Date(t.createdDate) : "-"
                    }
                },
            ]
        })
    },

    RemoveData: function (id) {
        var param = {
            IdMetode: id,
        };

        swal({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: true
        }).then((willDelete) => {
            if (willDelete.value) {
                console.log(willDelete.value);
                Form.DeleteMetode(param);

                swal("Deleted!", "Plan has been Deleted.", "success");
            }
        })

    },

    UpdateData: function (id, metode) {
        $('#IdMetode').val(id);
        $('#Metode').val(metode);
        $('#btnUpdate').show();
        $('#btnCreate').hide();

        Helper.PanelDetailShow();
    },
}
