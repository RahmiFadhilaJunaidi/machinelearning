var tblmstKarir='#tblmstKarir';
jQuery(document).ready(function () {
 Table.InitKarir();
 //Table.LoadTable();
 Helper.PanelHeaderShow();
 //Helper.InitButtons();

    $('#IdKarir').hide();

    $("#btnAddData").click(function () {
        Helper.PanelDetailShow();

        $('#btnUpdate').hide();
        $('#btnCreate').show();
    });

    $("#btnCancel").click(function () {
        Helper.PanelHeaderShow();

    });

    $("#btnCreate").click(function () {
        Form.Submit();
    });

    $("#btnUpdate").click(function () {
        Form.Update();
        $('#btnUpdate').show();
        $('#btnCreate').hide();
    });

 //var form = document.querySelector('form#mstKarirForm');
 //form.addEventListener('submit', function (ev) {
 //    ev.preventDefault();
 //    if (Common.Form.HandleFormSubmit(form, FormValidation)) {
 //        Form.Submit();
 //    }
 //});
});

var Form = {
    Submit: function () {
     var data = {
                IdKarir: $('#IdKarir').val(),
                Karir: $('#Karir').val(),
                Description: $('#Description').val(),
                CreatedBy: $('#CreatedBy').val(),
                CreatedDate: $('#CreatedDate').val(),
                UpdateBy: $('#UpdateBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                };
     var result = Common.Form.Save('/mstKarir/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.InitKarir();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/mstKarir/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.InitKarir();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#IdKarir').val('');
      $('#Karir').val('');
      $('#Description').val('');
      $('#CreatedBy').val('');
      $('#CreatedDate').val('');
      $('#UpdateBy').val('');
      $('#UpdatedDate').val('');
    },
    Update: function () {
        var data = {
            IdKarir: $('#IdKarir').val(),
            Karir: $('#Karir').val(),
            Description: $('#Description').val()
        };
        var result = Common.Form.Save('/mstKarir/Update', data);
        if (Common.CheckError.Object(result)) {
            Common.Alert.Success('Data  has been updated.');
            Table.InitKarir();
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    },

}

var FormValidation = {
    IdKarir: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       InitKarir: function () {
          //Common.Table.InitClient(tblmstKarir);
        t = $("#divKarirList").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: "/mstKarir/GetDataList",
                        method: "GET",
                        map: function (r) {
                            var e = r;
                            return void 0 !== r.data && (e = r.data), e;
                        }
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                },
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false
            },
            layout: {
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            },
            search: {
                input: $("#tbxSearch")
            },
            columns: [
                {
                    field: "idKarir", title: "Actions", sortable: false, textAlign: "center", template: function (t) {
                        var strBuilder = '<a href="javascript:;' + t.karir + '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" id="btnEdit" title="Edit details"  onclick="Table.UpdateData(\'' + t.idKarir + '\', \'' + t.karir + '\', \'' + t.description + '\');"><i class="la la-edit"></i></a>\t\t\t\t\t\t';
                        strBuilder += '<a id="deleteConfirmation" href="javascript:;' + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=Table.RemoveData(' + t.idKarir + ')><i class="la la-eraser"></i></a>';

                        return strBuilder;
                    }
                },
                { field: "karir", title: "Karir", textAlign: "center" },
                {
                    field: "description", title: "Description", textAlign: "left", template: function (t) {
                        return t.description != null ? (t.description).substring(0,100) + '...' : "-"
                    }
                },
                {
                    field: "createdDate", title: "Created Date", sortable: false, textAlign: "center", template: function (t) {
                        console.log(t)
                        return t.createdDate != null ? Common.Format.Date(t.createdDate) : "-"
                    }
                },
            ]
        })
    },

       LoadData: function () {
          return Common.GetData.Get('/mstKarir/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'idKarir'},
                    {'data':'karir'},
                    {'data':'description'},
                    {'data':'createdBy'},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updateBy'},
                    {'data':'updatedDate', render: function (data){ return Common.Format.Date(data);}},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblmstKarir, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblmstKarir).unbind();
              $(tblmstKarir).on('click', '.btnRemove', function (e) {
                  var tb = $(tblmstKarir).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        IdKarir : row.idKarir,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblmstKarir).on('click', '.btnEdit', function (e) {
                  var tb = $(tblmstKarir).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#IdKarir').val(row.idKarir);
                  $('#Karir').val(row.karir);
                  $('#Description').val(row.description);
                  $('#CreatedBy').val(row.createdBy);
                  $('#CreatedDate').val(row.createdDate);
                  $('#UpdateBy').val(row.updateBy);
                  $('#UpdatedDate').val(row.updatedDate);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
    },
    UpdateData: function (id, karir, description) {
        $('#IdKarir').val(id);
        $('#Karir').val(karir);
        $('#Description').val(description);
        $('#btnUpdate').show();
        $('#btnCreate').hide();

        Helper.PanelDetailShow();
    },
    RemoveData: function (id) {
        var param = {
            IdKarir: id,
        };

        swal({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: true
        }).then((willDelete) => {
            if (willDelete.value) {
                Form.Delete(param);

                swal("Deleted!", "Plan has been Deleted.", "success");
            }
        })

    },
}
