var tblmstKampus='#tblmstKampus';
jQuery(document).ready(function () {
    Table.InitKampus();
    Helper.PanelHeaderShow();

    $('#IdKampus').hide();
    $('.dropify').dropify();

    $("#btnAddData").click(function () {
        Helper.PanelDetailShow();

        $('#btnUpdate').hide();
        $('#btnCreate').show();
    });

    $("#btnCancel").click(function () {
        Helper.PanelHeaderShow();
    });

    $("#btnCreate").click(function () {
        Form.Submit();
    });

    $("#btnUpdate").click(function () {
        Form.Update();
        $('#btnUpdate').show();
        $('#btnCreate').hide();
    });
});

var Form = {
    Submit: function () {
     var data = {
                IdKampus: $('#IdKampus').val(),
                NamaKampus: $('#NamaKampus').val(),
                Deskripsi: $('#Deskripsi').val(),
                Website: $('#Website').val(),
                Email: $('#Email').val(),
                Telp: $('#Telp').val(),
                CreatedDate: $('#CreatedDate').val(),
                CreatedBy: $('#CreatedBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
        };


        var fileInput = document.getElementById("fuEvidence");
        var fileInputPSB = $("#fuEvidence")[0].files[0];

        if (fileInput.files.length != 0) {
            fsFileName = fileInput.files[0].name;
            fsFile = fileInput.files[0];
            fsExtension = fsFileName.split('.').pop().toUpperCase();
        }

        var model = new FormData();
        model.append('File', fileInputPSB);
        model.append('namaKampus', data.NamaKampus);
        model.append('deskripsi', data.Deskripsi);
        model.append('website', data.Website);
        model.append('email', data.Email);
        model.append('telp', data.Telp);

        var result = Common.Form.SaveFile('/mstKampus/UploadFile', model);
     if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been saved.');
         Table.InitKampus();
        Helper.PanelHeaderShow();
         
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/mstKampus/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
          Table.InitKampus();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    Update: function () {
        var data = {
            IdKampus: $('#IdKampus').val(),
            NamaKampus: $('#NamaKampus').val(),
            Deskripsi: $('#Deskripsi').val(),
            Website: $('#Website').val(),
            Email: $('#Email').val(),
            Telp: $('#Telp').val()
        };
        var result = Common.Form.Save('/mstKampus/Update', data);
        if (Common.CheckError.Object(result)) {
            Common.Alert.Success('Data  has been updated.');
            Table.InitKampus();
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    },

    ClearForm: function () {
      $('#IdKampus').val('');
      $('#NamaKampus').val('');
      $('#Deskripsi').val('');
      $('#Website').val('');
      $('#Email').val('');
      $('#Telp').val('');
      $('#CreatedDate').val('');
      $('#CreatedBy').val('');
      $('#UpdatedDate').val('');
      $('#UpdatedBy').val('');
    },
}

var FormValidation = {
    IdKampus: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       InitKampus: function () {
          //Common.Table.InitClient(tblmstKampus);
        t = $("#divUniversitasList").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: "/mstKampus/GetDataList",
                        method: "GET",
                        map: function (r) {
                            var e = r;
                            return void 0 !== r.data && (e = r.data), e;
                        }
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                },
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false
            },
            layout: {
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            },
            search: {
                input: $("#tbxSearch")
            },
            columns: [
                {
                    field: "idKampus", title: "Actions", sortable: false, textAlign: "center", template: function (t) {
                        var strBuilder = '<a href="javascript:;' + t.namaKampus + '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" id="btnEdit" title="Edit details"  onclick="Table.UpdateData(\'' + t.idKampus + '\', \'' + t.namaKampus + '\', \'' + t.Deskripsi + '\', \'' + t.telp + '\', \'' + t.email + '\', \'' + t.website + '\');"><i class="la la-edit"></i></a>\t\t\t\t\t\t';
                        strBuilder += '<a id="deleteConfirmation" href="javascript:;' + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=Table.RemoveData(' + t.idKampus + ')><i class="la la-eraser"></i></a>';

                        return strBuilder;
                    }
                },
                { field: "namaKampus", title: "Universitas", textAlign: "center" },
                { field: "website", title: "Website", textAlign: "center" },
                { field: "email", title: "Email", textAlign: "center" },
                { field: "telp", title: "No. Telp", textAlign: "center" },
                { field: "deskripsi", title: "Description", textAlign: "center" },
                {
                    field: "updatedDate", title: "Created Date", sortable: false, textAlign: "center", template: function (t) {
                        return t.updatedDate != null ? Common.Format.Date(t.updatedDate) : "-"
                    }
                },
            ]
        })
       },
       LoadData: function () {
          return Common.GetData.Get('/mstKampus/GetDataList');
       },

    UpdateData: function (id, nama, description, telp, email, website) {
        $('#IdKampus').val(id);
        $('#NamaKampus').val(nama);
        $('#Deskripsi').val(description);
        $('#Website').val(website);
        $('#Telp').val(telp);
        $('#Email').val(email);
        $('#btnUpdate').show();
        $('#btnCreate').hide();

        Helper.PanelDetailShow();
    },
    RemoveData: function (id) {
        var param = {
            IdKampus: id,
        };

        swal({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: true
        }).then((willDelete) => {
            if (willDelete.value) {
                Form.Delete(param);

                swal("Deleted!", "Plan has been Deleted.", "success");
            }
        })

    },
}
