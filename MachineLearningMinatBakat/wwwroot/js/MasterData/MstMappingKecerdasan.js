
var tblMstMappingKecerdasan='#tblMstMappingKecerdasan';
jQuery(document).ready(function () {
 Table.Init();
 Helper.PanelHeaderShow();


    $("#btnAddData").click(function () {
        Helper.PanelDetailShow();
        $('#btnUpdate').hide();
        $('#btnCreate').show();
        //$("#slsKodeSoal").disableSelection();
        $("#slsKodeSoal").attr('disabled', true);
        $("#slsKodeJurusan").attr('disabled', true);
        Dropdownlist.ddlKecerdasan();
        Dropdownlist.ddlJurusan();
        Dropdownlist.ddlSoal();
       
    });

    $("#slsKodeKecerdasan").change(function () {
        $("#slsKodeSoal").attr('disabled', false);
        $("#slsKodeJurusan").attr('disabled', true);
        Dropdownlist.ddlJurusan();
        Dropdownlist.ddlSoal();
    });

    $("#slsKodeSoal").change(function () {
        $("#slsKodeSoal").attr('disabled', false);
        $("#slsKodeJurusan").attr('disabled', false);
        Dropdownlist.ddlJurusan();
    });


    $("#btnCancel").click(function () {
        Helper.PanelHeaderShow();

    });

    $("#btnCreate").click(function () {
        Form.Submit();
    });

    $("#btnUpdate").click(function () {
        Form.Update();
    });

});

var Form = {
    Submit: function () {
     var data = {
                IdMappingKecerdasan: $('#IdMappingKecerdasan').val(),
                KodeCategory: $('#slsKodeKecerdasan').val(),
                KodeJurusan: $('#slsKodeJurusan').val(),
                KodeSoal: $('#slsKodeSoal').val()
                };
     var result = Common.Form.Save('/MstMappingKecerdasan/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.Init();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/MstMappingKecerdasan/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#IdMappingKecerdasan').val('');
      $('#KodeCategory').val('');
      $('#KodeJurusan').val('');
      $('#KodeSoal').val('');
      $('#CreatedBy').val('');
      $('#CreatedDate').val('');
      $('#UpdatedBy').val('');
      $('#UpdateDate').val('');
    },

}

var FormValidation = {
    IdMappingKecerdasan: {
     presence: true,
    },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
    Init: function () {
          //Common.Table.InitClient(tblMstMappingKecerdasan);
        t = $("#divMappingKecerdasanList").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: "/MstMappingKecerdasan/GetDataList",
                        method: "GET",
                        map: function (r) {
                            var e = r;
                            return void 0 !== r.data && (e = r.data), e;
                        }
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                },
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false
            },
            layout: {
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            },
            search: {
                input: $("#tbxSearch")
            },
            columns: [
                //{
                //    field: "idMappingKecerdasan", title: "Actions", sortable: false, textAlign: "center", template: function (t) {
                //        var strBuilder = '<a href="javascript:;' + t.categoryName + '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" id="btnEdit" title="Edit details"  onclick="Table.UpdateData(\'' + t.categoryName + '\', \'' + t.jurusan + '\', \'' + t.soal + '\');"><i class="la la-edit"></i></a>\t\t\t\t\t\t';
                //        strBuilder += '<a id="deleteConfirmation" href="javascript:;' + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=Table.RemoveData(' + t.idMappingKecerdasan + ')><i class="la la-eraser"></i></a>';

                //        return strBuilder;
                //    }
                //},
                { field: "categoryName", title: "Category Name", textAlign: "center" },
                { field: "jurusan", title: "Jurusan", textAlign: "center" },
                { field: "soal", title: "Soal", textAlign: "left" },
                {
                    field: "createdDate", title: "Created Date", sortable: false, textAlign: "center", template: function (t) {
                        console.log(t)
                        return t.createdDate != null ? Common.Format.Date(t.createdDate) : "-"
                    }
                },
            ]
        })
    },
    UpdateData: function (categoryName, jurusan, soal) {
        $('#slsKodeKecerdasan').val(categoryName);
        //$('#CategoryName').val(metode);
        $('#btnUpdate').show();
        $('#btnCreate').hide();

        Helper.PanelDetailShow();
    },
}

var Dropdownlist = {
    ddlKecerdasan: function() {
        var result = Common.GetData.Get('/MstKecerdasanCategory/DdlKecerdasanCategory');
        if (Common.CheckError.Object(result)) {
            $("#slsKodeKecerdasan").html('');
            $("#slsKodeKecerdasan").append('<option value="0">-- select one-- </option>');
            //Common.Helper.Dropdownlist("#slsKodeKecerdasan", result.data);
            if (result.data.length > 0) {
                $.each(result.data, function (i, item) {
                    $("#slsKodeKecerdasan").append('<option value=' + item.value + '>' + '['+ item.value+ '] - '+ item.text + '</option>');
                });
                $("#slsKodeKecerdasan").select2();
            }
            console.log(result);
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    },
    ddlJurusan: function () {
        var result = Common.GetData.Get('/MstJurusan/DdlJurusan');
        if (Common.CheckError.Object(result)) {
            $("#slsKodeJurusan").html('');
            $("#slsKodeJurusan").append('<option value="0">-- select one-- </option>');
            Common.Helper.Dropdownlist("#slsKodeJurusan", result.data);
            console.log(result);
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    },
    ddlSoal: function () {
        var result = Common.GetData.Get('/MstSoal/DdlSoal?KodeCategory=' + $("#slsKodeKecerdasan").val());
        if (Common.CheckError.Object(result)) {
            $("#slsKodeSoal").html('');
            $("#slsKodeSoal").append('<option value="0">-- select one-- </option>');
            //Common.Helper.Dropdownlist("#slsKodeSoal", result.data);
            if (result.data.length > 0) {
                $.each(result.data, function (i, item) {
                    $("#slsKodeSoal").append('<option value=' + item.value + '>' + '[' + item.value + '] - ' + item.text + '</option>');
                });
                $("#slsKodeSoal").select2();
            }
            console.log(result);
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    }
}
