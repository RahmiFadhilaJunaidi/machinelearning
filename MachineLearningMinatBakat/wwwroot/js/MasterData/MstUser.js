var tblMstUser='#tblMstUser';
jQuery(document).ready(function () {

    Table.Init();
    Helper.BootstrapDatepicker();
     //Table.LoadTable();
     Helper.PanelHeaderShow();
     //Helper.InitButtons();

    $(".AddData").hide();
    $("#btnCancel").click(function () {
        Helper.PanelHeaderShow();

    });

    $("#btnUpdate").click(function () {
        Form.Update();
    });

    $("#btnCreate").click(function () {
        Form.Submit();
    });

    $("#btnRegister").click(function () {
        Form.Submit();
    });

});

var Form = {
    Submit: function () {
     var data = {
             UserID: $('#UserID').val(),
             Nama: $('#Nama').val(),
             TglLahir: $('#TglLahir').val(),
             JenisKelamin: $('#JenisKelamin').val(),
             Alamat: $('#Alamat').val(),
             UserName: $('#UserName').val(),
             Password: $('#Password').val(),
             TempatLahir: $('#TempatLahir').val(),
             Email: $('#Email').val(),
             Telp: $('#NoTelp').val(),
        };

        var cekData = Common.GetData.Post('/MasterData/CekDataUser', data);
        
        if (cekData.respon == "") {
            if ($('#Password').val() != $('#tbxConfirmPassword').val()) {
                Common.Alert.Warning('Password is doesnt match !!');
            } else {
                if (data.userName != "") {
                    var result = Common.Form.Save('/MasterData/Create', data);
                    if (Common.CheckError.Object(result)) {
                        Common.Alert.Success('Data  has been saved.');
                        //this.LoadLogin();
                        Form.ClearForm();
                    } else {
                        Common.Alert.Error(result.respon.errorMessage);
                    }
                } else {
                    Common.Alert.Warning('Please fill all field.');
                }
                
            }
        } else {
            Common.Alert.Warning('Username  has been used.');
        }
    },

    LoadLogin: function () {
        $(".clsLogin").show();
        $(".clsRegister").hide();
        //window.history.back(1)
    },

    Delete: function (param) {
     var result = Common.Form.Delete('/MstUser/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         //Table.LoadTable();
          Table.init();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },

    Update: function () {
        var data = {
            userID: $('#UserID').val(),
            nama: $('#Nama').val(),
            tglLahir: $('#TglLahir').val(),
            jenisKelamin: $('#JenisKelamin').val(),
            alamat: $('#Alamat').val(),
            email: $('#Email').val(),
            telp: $('#NoTelp').val(),
            tempatLahir: $('#TempatLahir').val(),
            userName: $('#UserName').val()
            //Password: $('#Password').val()
        };
        var result = Common.Form.Save('/MstUser/Update', data);
        if (Common.CheckError.Object(result)) {
            Common.Alert.Success('Data  has been updated.');
            Table.Init();
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    },
    ClearForm: function () {
        $('#UserID').val('');
        $('#Nama').val('');
        $('#TglLahir').val('');
        $('#JenisKelamin').val('');
        $('#Alamat').val('');
        $('#UserName').val('');
        $('#Password').val('');
        $('#IsActive').val('');
        $('#CreatedDate').val('');
        $('#CreatedBy').val('');
        $('#UpdatedDate').val('');
        $('#UpdatedBy').val('');
        $('#TempatLahir').val("");
        $('#Email').val("");
        $('#NoTelp').val("");
        $('#Alamat').val("");
    },
}

var FormValidation = {
    UserID: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
    },
    BootstrapDatepicker: function () {
        $(".datepicker").datepicker({
            format: 'dd-M-yyyy',
            todayBtn: "linked", clearBtn: !0, todayHighlight: !0, templates: {
                leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'
            }
        })
    },
}

var Table = {
       Init: function () {
          //Common.Table.InitClient(tblMstUser);

            t = $("#divUserList").mDatatable({
                data: {
                    type: "remote",
                    source: {
                        read: {
                            url: "/MstUser/GetDataList",
                            method: "GET",
                            map: function (r) {
                                var e = r;
                                return void 0 !== r.data && (e = r.data), e;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: true,
                        webstorage: true
                    },
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false
                },
                layout: {
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                toolbar: {
                    items: {
                        pagination: {
                            pageSizeSelect: [10, 20, 30, 50, 100]
                        }
                    }
                },
                search: {
                    input: $("#tbxSearch")
                },
                columns: [
                    {
                        field: "userID", title: "Actions", sortable: false, textAlign: "center", template: function (t) {
                            console.log(t);
                            var strBuilder = '<a href="javascript:;' + t.UserID + '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" id="btnEdit" title="Edit details"  onclick="Table.UpdateData(\'' + t.userID + '\', \'' + t.userName + '\', \'' + t.nama + '\', \'' + t.tempatLahir + '\', \'' + t.tglLahir + '\', \'' + t.alamat + '\', \'' + t.email + '\', \'' + t.telp + '\');"><i class="la la-edit"></i></a>\t\t\t\t\t\t';
                            strBuilder += '<a id="deleteConfirmation" href="javascript:;' + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=Table.RemoveData(' + t.UserId  + ')><i class="la la-eraser"></i></a>';

                            return strBuilder;
                        }
                    },
                    { field: "nama", title: "Nama", textAlign: "center" },
                    //{ field: "tglLahir", title: "Tanggal Lahir", textAlign: "center" },
                    {
                        field: "tglLahir", title: "Tanggal Lahir", sortable: false, textAlign: "center", template: function (t) {
                            return t.tglLahir != null ? Common.Format.Date(t.tglLahir) : "-"
                        }
                    },
                    { field: "jenisKelamin", title: "Jenis Kelamin", textAlign: "center" },
                    { field: "alamat", title: "Alamat", textAlign: "center" },
                    { field: "userName", title: "UserName", textAlign: "center" }
                ]
            })
    },


    RemoveData: function (id) {
        var param = {
            UserId: id,
        };

        swal({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: true
        }).then((willDelete) => {
            if (willDelete.value) {
                console.log(willDelete.value);
                Form.Delete(param);

                swal("Deleted!", "Plan has been Deleted.", "success");
            }
        })

    },

    UpdateData: function (userID, UserName, fullName, tempatLahir, tglLahir, alamat, email, telp) {
        $('#UserID').val(userID);
        $('#UserName').val(UserName);
        $('#Nama').val(fullName);
        $('#TempatLahir').val(tempatLahir);
        $('#TglLahir').val(Common.Format.Date(tglLahir));
        $('#Alamat').val(alamat);
        $('#Email').val(email);
        $('#NoTelp').val(telp);
        $('#btnUpdate').show();
        $('#btnCreate').hide();

        Helper.PanelDetailShow();
    },

}