var tblMstSoal='#tblMstSoal';
jQuery(document).ready(function () {
 Table.Init();
 //Table.LoadTable();
 Helper.PanelHeaderShow();
 //Helper.InitButtons();

    $('#IdSoal').hide();
    $('.KodeSoal').hide();

    $("#btnAddData").click(function () {
        Helper.PanelDetailShow();
        $('#btnUpdate').hide();
        $('#btnCreate').show();

    });

    $("#btnCancel").click(function () {
        Helper.PanelHeaderShow();

    });

    $("#btnCreate").click(function () {
        Form.Submit();
    });

    $("#btnUpdate").click(function () {
        Form.Update();
    });

     //var form = document.querySelector('form#MstSoalForm');
     //form.addEventListener('submit', function (ev) {
     //    ev.preventDefault();
     //    if (Common.Form.HandleFormSubmit(form, FormValidation)) {
     //        Form.Submit();
     //    }
     //});
});

var Form = {
    Submit: function () {
     var data = {
                IdSoal: $('#IdSoal').val(),
                KodeSoal: $('#KodeSoal').val(),
                Soal: $('#Soal').val(),
                CreatedDate: $('#CreatedDate').val(),
                CreatedBy: $('#CreatedBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdatedBy: $('#UpdatedBy').val(),
                };
     var result = Common.Form.Save('/MstSoal/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.Init();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/MstSoal/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         Table.LoadTable();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    Update: function () {
        var data = {
            IdSoal: $('#IdSoal').val(),
            Soal: $('#Soal').val()
        };
        var result = Common.Form.Save('/MstSoal/Update', data);
        if (Common.CheckError.Object(result)) {
            Common.Alert.Success('Data  has been updated.');
            Table.Init();
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    },

    ClearForm: function () {
      $('#IdSoal').val('');
      $('#KodeSoal').val('');
      $('#Soal').val('');
      $('#CreatedDate').val('');
      $('#CreatedBy').val('');
      $('#UpdatedDate').val('');
      $('#UpdatedBy').val('');
    },
}

var FormValidation = {
    IdSoal: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          //Common.Table.InitClient(tblMstSoal);
            t = $("#divSoalList").mDatatable({
                data: {
                    type: "remote",
                    source: {
                        read: {
                            url: "/MstSoal/GetDataList",
                            method: "GET",
                            map: function (r) {
                                var e = r;
                                return void 0 !== r.data && (e = r.data), e;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: true,
                        webstorage: true
                    },
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false
                },
                layout: {
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                toolbar: {
                    items: {
                        pagination: {
                            pageSizeSelect: [10, 20, 30, 50, 100]
                        }
                    }
                },
                search: {
                    input: $("#tbxSearch")
                },
                columns: [
                    {
                        field: "idSoal", title: "Actions", sortable: false, textAlign: "center", template: function (t) {

                            var strBuilder = '<a href="javascript:;' + t.idSoal + '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" id="btnEdit" title="Edit details"  onclick="Table.UpdateData(' + t.idSoal + ', \'' + t.soal + '\');"><i class="la la-edit"></i></a>\t\t\t\t\t\t';
                            strBuilder += '<a id="deleteConfirmation" href="javascript:;' + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=Table.RemoveData(' + t.idSoal + ')><i class="la la-eraser"></i></a>';

                            return strBuilder;
                        }
                    },
                    { field: "kodeSoal", title: "Kode Soal", textAlign: "center" },
                    { field: "soal", title: "Soal", textAlign: "left" },
                    {
                        field: "createdDate", title: "Created Date", sortable: false, textAlign: "center", template: function (t) {
                            return t.createdDate != null ? Common.Format.Date(t.createdDate) : "-"
                        }
                    },
                ]
            })
        },
        UpdateData: function (id, metode) {
            $('#IdSoal').val(id);
            $('#Soal').val(metode);
            $('#btnUpdate').show();
            $('#btnCreate').hide();

            Helper.PanelDetailShow();
        },
        RemoveData: function (id) {
            var param = {
                IdSoal: id,
            };

            swal({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: true,
                closeOnCancel: true,
                allowOutsideClick: true
            }).then((willDelete) => {
                if (willDelete.value) {
                    Form.Delete(param);

                    swal("Deleted!", "Plan has been Deleted.", "success");
                }
            })

        },
}
