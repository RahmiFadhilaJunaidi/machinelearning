var tblMstJurusanCategory='#tblMstJurusanCategory';
jQuery(document).ready(function () {
    Table.Init();
    Helper.PanelHeaderShow();
    Helper.InitButtons();
    $('#IdJurusanCategory').hide();

});

var Form = {
    Submit: function () {
     var data = {
                idJurusanCatgeory: $('#IdJurusanCatgeory').val(),
                kodeJurusanCategory: $('#KodeJurusanCategory').val(),
                jurusanCategory: $('#JurusanCategory').val()
                };
     var result = Common.Form.Save('/MstJurusanCategory/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        Table.Init();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/MstJurusanCategory/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
          Table.Init();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },
    ClearForm: function () {
      $('#IdJurusanCatgeory').val('');
      $('#KodeJurusanCategory').val('');
      $('#JurusanCategory').val('');
      $('#CreatedBy').val('');
      $('#CreatedDate').val('');
      $('#UpdatedBy').val('');
      $('#UpdateDate').val('');
    },
    UpdateJurusan: function () {
        var data = {
            idJurusanCatgeory: $('#IdJurusanCatgeory').val(),
            kodeJurusanCategory: $('#KodeJurusanCategory').val(),
            jurusanCategory: $('#JurusanCategory').val()
        };
        var result = Common.Form.Save('/MstJurusanCategory/Update', data);
        if (Common.CheckError.Object(result)) {
            Common.Alert.Success('Data  has been updated.');
            Table.Init();
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    },
}

var FormValidation = {
    IdJurusanCatgeory: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
       });

        $("#btnAddData").click(function () {
            Helper.PanelDetailShow();

            $('#btnUpdate').hide();
            $('#btnCreate').show();
        });

        $("#btnCancel").click(function () {
            Helper.PanelHeaderShow();

        });

        $("#btnCreate").click(function () {
            Form.Submit();
        });

        $("#btnUpdate").click(function () {
            Form.Update();
        });

     },
}

var Table = {
       Init: function () {
        t = $("#divJurusanCategoryList").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: "/MstJurusanCategory/GetDataList",
                        method: "GET",
                        map: function (r) {
                            var e = r;
                            return void 0 !== r.data && (e = r.data), e;
                        }
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                },
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false
            },
            layout: {
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            },
            search: {
                input: $("#tbxSearch")
            },
            columns: [
                {
                    field: "idJurusanCategory", title: "Actions", sortable: false, textAlign: "center", template: function (t) {
                        
                        var strBuilder = '<a href="javascript:;' + t.idJurusanCategory + '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" id="btnEdit" title="Edit details"  onclick="Table.UpdateData(' + t.idJurusanCategory + ', \'' + t.jurusanCategory + '\');"><i class="la la-edit"></i></a>\t\t\t\t\t\t';
                        strBuilder += '<a id="deleteConfirmation" href="javascript:;' + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=Table.RemoveData(' + t.idJurusanCategory + ')><i class="la la-eraser"></i></a>';

                        return strBuilder;
                    }
                },
                { field: "kodeJurusanCategory", title: "Kode Jurusan Catgeory", textAlign: "center" },
                { field: "jurusanCategory", title: "Category Name", textAlign: "center" }
            ]
        })
       },
       LoadData: function () {
          return Common.GetData.Get('/MstJurusanCategory/GetDataList');
       },
       LoadTable: function () {
          var data = Table.LoadData();
          if (Common.CheckError.Object(data)){
              var columns = [
                   {
                       render: function () {
                         var str = '<div class="btn-group">';
                         str += '<button type="button" class="btn btn-primary btn-xl btn-icon btnEdit"  data-toggle="tooltip" data-placement="top" title="edit row"><i class="icofont icofont-pencil-alt-5"></i></button> &nbsp;&nbsp;';
                         str += '<button type="button" class="btn btn-danger btn-xl btn-icon btnRemove"  data-toggle="tooltip" data-placement="top" title="remove row"><i class="icofont icofont-trash"></i></button>';
                         str += '</div>';
                         return str;
                       }},
                    {'data':'idJurusanCatgeory'},
                    {'data':'kodeJurusanCategory'},
                    {'data':'jurusanCategory'},
                    {'data':'createdBy'},
                    {'data':'createdDate', render: function (data){ return Common.Format.Date(data);}},
                    {'data':'updatedBy'},
                    {'data':'updateDate', render: function (data){ return Common.Format.Date(data);}},
                   ];
              var columnDefs = [];
              var lengthMenu = [[5, 10, 25, 50], ['5', '10', '25', '50']];
              Common.Table.LoadTableClient(tblMstJurusanCategory, data.data, columns, lengthMenu, columnDefs, null, '.cbRowIsActive', ['excel']);
              $(tblMstJurusanCategory).unbind();
              $(tblMstJurusanCategory).on('click', '.btnRemove', function (e) {
                  var tb = $(tblMstJurusanCategory).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  var param={
                        IdJurusanCatgeory : row.idJurusanCatgeory,
                             };
                  swal({
                      title: 'Are you sure ? ',
                      text: '',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false
                      },
                      function () {
                          Form.Delete(param);
                      });
              });
              $(tblMstJurusanCategory).on('click', '.btnEdit', function (e) {
                  var tb = $(tblMstJurusanCategory).DataTable();
                  var row = tb.row($(this).parents('tr')).data();
                  $('#IdJurusanCatgeory').val(row.idJurusanCatgeory);
                  $('#KodeJurusanCategory').val(row.kodeJurusanCategory);
                  $('#JurusanCategory').val(row.jurusanCategory);
                  $('#CreatedBy').val(row.createdBy);
                  $('#CreatedDate').val(row.createdDate);
                  $('#UpdatedBy').val(row.updatedBy);
                  $('#UpdateDate').val(row.updateDate);
                  Helper.PanelDetailShow();
              });
          } else {
               Common.Alert.Error(data.respon.errorMessage);
          }
    },

    UpdateData: function (id, jurusanCategory) {
        $('#IdJurusanCategory').val(id);
        $('#JurusanCategory').val(jurusanCategory);
        $('#btnUpdate').show();
        $('#btnCreate').hide();
        Helper.PanelDetailShow();
    },

}
