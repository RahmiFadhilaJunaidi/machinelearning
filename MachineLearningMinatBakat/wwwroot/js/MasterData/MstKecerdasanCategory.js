

var tblMstKecerdasanCategory='#tblMstKecerdasanCategory';
jQuery(document).ready(function () {
 Table.Init();
 //Table.LoadTable();
 Helper.PanelHeaderShow();
 //Helper.InitButtons();

    $('#IdKecerdasanCategory').hide();

    $("#btnAddData").click(function () {
        Helper.PanelDetailShow();
        $('#btnUpdate').hide();
        $('#btnCreate').show();

    });

    $("#btnCancel").click(function () {
        Helper.PanelHeaderShow();

    });

    $("#btnCreate").click(function () {
        Form.Submit();
    });

    $("#btnUpdate").click(function () {
        Form.Update();
    });
});

var Form = {
    Submit: function () {
     var data = {
                IdKecerdasanCategory: $('#IdKecerdasanCategory').val(),
                KodeCategory: $('#KodeCategory').val(),
                CategoryName: $('#CategoryName').val(),
                CreatedDate: $('#CreatedDate').val(),
                CreatedBy: $('#CreatedBy').val(),
                UpdatedDate: $('#UpdatedDate').val(),
                UpdateBy: $('#UpdateBy').val(),
                };
     var result = Common.Form.Save('/MstKecerdasanCategory/Create', data);
     if (Common.CheckError.Object(result)) {
        Common.Alert.Success('Data  has been saved.');
        Helper.PanelHeaderShow();
        //Table.LoadTable();
         Table.Init();
      } else {
        Common.Alert.Error(result.respon.errorMessage);
      }
    },
    Delete: function (param) {
     var result = Common.Form.Delete('/MstKecerdasanCategory/Delete', param);
      if (Common.CheckError.Object(result)) {
         Common.Alert.Success('Data  has been deleted.');
         //Table.LoadTable();
          Table.Init();
       } else {
         Common.Alert.Error(result.respon.errorMessage);
       }
    },

    Update: function () {
        var data = {
            IdKecerdasanCategory: $('#IdKecerdasanCategory').val(),
            CategoryName: $('#CategoryName').val()
        };
        var result = Common.Form.Save('/MstKecerdasanCategory/Update', data);
        if (Common.CheckError.Object(result)) {
            Common.Alert.Success('Data  has been updated.');
            Table.InitMetode();
        } else {
            Common.Alert.Error(result.respon.errorMessage);
        }
    },

    ClearForm: function () {
      $('#IdKecerdasanCategory').val('');
      $('#KodeCategory').val('');
      $('#CategoryName').val('');
      $('#CreatedDate').val('');
      $('#CreatedBy').val('');
      $('#UpdatedDate').val('');
      $('#UpdateBy').val('');
    },
}

var FormValidation = {
    IdKecerdasanCategory: {
     presence: true,
     },
}

var Helper = {
     PanelHeaderShow: function () {
       $('.pnlHeader').css('visibility', 'visible');
       $('.pnlDetail').css('visibility', 'hidden');
       $('.pnlHeader').show();
       $('.pnlDetail').hide();
     },
     PanelDetailShow: function () {
       $('.pnlHeader').css('visibility', 'hidden');
       $('.pnlDetail').css('visibility', 'visible');
       $('.pnlHeader').hide();
       $('.pnlDetail').show();
     },
     InitButtons: function () {
       $('.btnCancel').unbind().click(function () {
          Helper.PanelHeaderShow();
       });
       $('.btnAddData').unbind().click(function () {
           Form.ClearForm();
           Helper.PanelDetailShow();
        });
     },
}

var Table = {
       Init: function () {
          //Common.Table.InitClient(tblMstKecerdasanCategory);
        t = $("#divCategoryKecerdasanList").mDatatable({
                data: {
                    type: "remote",
                    source: {
                        read: {
                            url: "/MstKecerdasanCategory/GetDataList",
                            method: "GET",
                            map: function (r) {
                                var e = r;
                                return void 0 !== r.data && (e = r.data), e;
                            }
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: true,
                        webstorage: true
                    },
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false
                },
                layout: {
                    scroll: false,
                    footer: false
                },
                sortable: true,
                pagination: true,
                toolbar: {
                    items: {
                        pagination: {
                            pageSizeSelect: [10, 20, 30, 50, 100]
                        }
                    }
                },
                search: {
                    input: $("#tbxSearch")
                },
                columns: [
                    {
                        field: "idKecerdasanCategory", title: "Actions", sortable: false, textAlign: "center", template: function (t) {

                            var strBuilder = '<a href="javascript:;' + t.idKecerdasanCategory + '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" id="btnEdit" title="Edit details"  onclick="Table.UpdateData(' + t.idKecerdasanCategory + ', \'' + t.categoryName + '\');"><i class="la la-edit"></i></a>\t\t\t\t\t\t';
                            strBuilder += '<a id="deleteConfirmation" href="javascript:;' + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=Table.RemoveData(' + t.idKecerdasanCategory + ')><i class="la la-eraser"></i></a>';

                            return strBuilder;
                        }
                    },
                    { field: "kodeCategory", title: "Cetegory Code", textAlign: "center" },
                    { field: "categoryName", title: "Category Name", textAlign: "center" }
                ]
            })
    },

    RemoveData: function (id) {
        var param = {
            IdKecerdasanCategory: id,
        };

        swal({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: true
        }).then((willDelete) => {
            if (willDelete.value) {
                console.log(willDelete.value);
                Form.Delete(param);

                swal("Deleted!", "Plan has been Deleted.", "success");
            }
        })

    },

    UpdateData: function (id, metode) {
        $('#IdKecerdasanCategory').val(id);
        $('#CategoryName').val(metode);
        $('#btnUpdate').show();
        $('#btnCreate').hide();

        Helper.PanelDetailShow();
    },
}
