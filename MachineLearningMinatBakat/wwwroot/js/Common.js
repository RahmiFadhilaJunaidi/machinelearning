﻿jQuery(document).ready(function () {
    var path = window.location.pathname;
    path2 = path.split('/')[1];


    $('.sidebarActive').each(function () {
        if (path2 == 'PinnedProject') {
            if (this.id == path)
                $(this).addClass('m-menu__item--active').siblings().removeClass("m-menu__item--active");

        }
        else if (this.id == path2)
            $(this).addClass('m-menu__item--active').siblings().removeClass("m-menu__item--active");

    })

    Theme.Init();
});

var Common = {
	//Check Error
	CheckError: {
		Object: function (data) {
			//console.log(data);
            if (data.respon.errorType == 0) {
				return true;
            } else if (data.respon.errorType == 1) {
				Common.Alert.Warning(data.ErrorMessage);
				return false;
            } else if (data.respon.errorType == 2) {
				Common.Alert.Error(data.ErrorMessage);
				return false;
			}
		},
		List: function (data) {
			if (data.length > 0) {
				if (data[0].ErrorType == 0) {
					return true;
				} else if (data[0].ErrorType == 1) {
					Common.Alert.Warning(data[0].ErrorMessage);
					return false;
				} else if (data[0].ErrorType == 2) {
					Common.Alert.Error(data[0].ErrorMessage);
					return false;
				}
			}
			return true
		}
    },
    Table: {
        InitClient: function (idTB) {
            $(idTB).DataTable({
                "destroy": true,
                "filter": true,
                "serverSide": false,
                "language": {
                    "emptyTable": "No data available in table"
                },
                "data": [],
            });
        },
        LoadTableClient: function (_idTB, _data, _columns, _lengthMenu, _columnDefs, _colFooter, _idCheckbox, _buttons) {
            var istest = false;
            $(_idTB).DataTable({
                "deferRender": true,
                "proccessing": true,
                "serverSide": false,
                "destroy": true,
                "filter": true,
                "language": {
                    "emptyTable": "No data available in table"
                },

                //"dom": 'Bfrtip',
                //"dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
                //"buttons": [
                //    'copy', 'excel', 'pdf', 'print'
                //],
                //"buttons": _buttons,
                "data": _data,

                "lengthMenu": _lengthMenu,
                "columns": _columns,
                "columnDefs": _columnDefs,
                "drawCallback": function (row, data) {

                    var switcheryList = [];
                    switcheryList = _idCheckbox.split(";");
                    if (switcheryList.length > 0 && switcheryList[0] != "") {
                        $.each(switcheryList, function (i, item) {
                            //$('.switchery-default').remove();
                            $(item + 'Parent').children(".switchery-default").remove();
                            $(item).removeAttr('data-switchery');
                            var elems = Array.prototype.slice.call(document.querySelectorAll(item));
                            if (elems.length > 0 && !elems[0].dataset.switchery) {
                                elems.forEach(function (html) {

                                    var switchery = new Switchery(html, { color: '#4680ff', jackColor: '#fff' });
                                    html.onchange = function () {
                                        swal({
                                            title: "Are you sure?",
                                            text: "",
                                            type: "warning",
                                            showCancelButton: true,
                                            confirmButtonClass: "btn-danger",
                                            confirmButtonText: "Yes",
                                            closeOnConfirm: false
                                        },
                                            function () {

                                                var check = html.checked == true ? true : false;
                                                Form.CheckBox(item, html.value, check);

                                            });
                                    }

                                });
                            }
                        });
                    }
                },
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var col = _colFooter;
                    if (col != null) {
                        for (var i = 0; i < col.length; i++) {


                            // Total over all pages
                            total = api
                                .column(col[i])
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                            // Total over this page
                            pageTotal = api
                                .column(col[i], { page: 'current' })
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                            // Update footer
                            $(api.column(col[i]).footer()).html(
                                '<b>' + Common.CommaSeparation(total) + '</b>'
                            );
                        }
                    }
                }
            });
        },
    },
	Alert: {
		Error: function (message) {
			swal({
				title: "Error!",
				text: message,
				type: "error",
			})
		},
		ErrorRoute: function (message, url) {
			swal({
				title: "Error!",
				text: message,
				type: "error",
			})
				.then(function (isConfirm) {
					if (isConfirm)
						window.location.href = url;
				})
		},
		Warning: function (message) {
			swal({
				title: "Warning!",
				text: message,
				type: "warning",
			})
		},
		Success: function (message) {
			swal({
				title: "Success!",
				text: message,
				type: "success",
			})
		},
		SuccessRoute: function (message, url) {
			swal({
				title: "Success!",
				text: message,
				type: "success"
			})
				.then(function (isConfirm) {
					if (isConfirm)
						window.location.href = url;
				})
		},
		TextHTML: function (message) {
		    swal({
		        html: message,
		    })
		},
	},
	Format: {
		Date(data) {
			var date = new Date(data)
			var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			return (parseInt(date.getDate()) < 10 ? "0" + date.getDate() : date.getDate()) + "-" + monthNames[date.getMonth()] + "-" + date.getFullYear();
        },
        DateHour: function (data) {
            var date = new Date(data);
            return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        },
        DateFull(data) {
            var date = new Date(data)
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            return (parseInt(date.getDate()) < 10 ? "0" + date.getDate() : date.getDate()) + "-" + monthNames[date.getMonth()] + "-" + date.getFullYear() + " " + (parseInt(date.getHours()) < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (parseInt(date.getMinutes()) < 10 ? "0" + date.getMinutes() : date.getMinutes()) + ":" + (parseInt(date.getSeconds()) < 10 ? "0" + date.getSeconds() : date.getSeconds());
        },
        CommaSeparation: function (yourNumber) {
            //Seperates the components of the number
            var n = parseFloat(yourNumber).toFixed(2).toString().split(".");
            //Comma-fies the first part
            n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            //Combines the two sections
            return n.join(".");
        },
        DateOnly(data) {
            var date = new Date(data)
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            return (parseInt(date.getDate()) < 10 ? "0" + date.getDate() : date.getDate()) + " " + monthNames[date.getMonth()] + " " + date.getFullYear();
        },
	},
    Form: {
        Save: function (_url, _data) {
            var result = [];
            $.ajax({
                url: _url,
                type: 'POST',
                dataType: 'json',
                data: _data,
                beforeSend: function () {
                    $(".loader").show();
                },
                //contentType: false,
                //processData: false,
                async: false,
                cache: false,
            }).done(function (data, textStatus, jqXHR) {
                result = data;
                $(".loader").hide();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                result.respon.errorMessage = 'Save data failed!';
                result.respon.errorType = 1;
            });
            return result;
        },
        SaveFile: function (_url, _data) {
            var result = [];
            $.ajax({
                url: _url,
                type: 'POST',
                dataType: 'json',
                data: _data,
                beforeSend: function () {
                    $(".loader").show();
                },
                contentType: false,
                processData: false,
                async: false,
                cache: false,
            }).done(function (data, textStatus, jqXHR) {
                result = data;
                $(".loader").hide();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                result.respon.errorMessage = 'Save data failed!';
                result.respon.errorType = 1;
            });
            return result;
        },
        SaveWithJSON: function (_url, data) {
            var result = [];
            $.ajax({
                url: _url,
                type: "POST",
                datatype: "json",
                contentType: "application/json; charset=utf-8",
                data: data,
                cache: false
            }).done(function (data, textStatus, jqXHR) {
                result = data;
            }).fail(function (jqXHR, textStatus, errorThrown) {
                result.respon.errorMessage = 'Save data failed!';
                result.respon.errorType = 1;
            });
            return result;

        },
        Delete: function (_url, _data) {
            var result = '';
            $.ajax({
                url: _url,
                type: 'GET',
                data: _data,
                async: false,
                cache: false,
            }).done(function (data, textStatus, jqXHR) {
                result = data;
            }).fail(function (jqXHR, textStatus, errorThrown) {
                result.respon.errorMessage = 'Delete data failed!'
                result.respon.errorType = 1;

            });
            return result;
        },
        HandleFormSubmit: function (form, constraints) {
            // validate the form aainst the constraints
            var result = false;
            var errors = validate(form, constraints);
            // then we update the form to reflect the results
            Common.Form.ShowErrors(form, errors || {});

            if (errors == undefined) {
                result = true;
            }
            return result;
        },
        ShowErrors: function (form, errors) {

            // We loop through all the inputs and show the errors for that input
            $.each(form.elements, function (input, element) {
                // Since the errors can be null if no errors were found we need to handle
                // that
                Common.Form.ShowErrorsForInput(element, errors && errors[element.id]);
            });
        },
        ShowErrorsForInput: function (input, errors) {
            // This is the root of the input

            var formGroup = Common.Form.ClosestParent(input.parentNode, "form-group");
            var messages = input.parentNode.querySelector(".messages");
            // Find where the error messages will be insert into

            //var messages = ".messages";//formGroup.querySelector(".messages");
            // First we remove any old messages and resets the classes
            Common.Form.ResetFormGroup(formGroup);
            // If we have errors
            if (errors) {
                // we first mark the group has having errors
                formGroup.classList.add("has-error");
                // then we append all the errors
                $.each(errors, function (i, error) {
                    Common.Form.AddError(messages, error, input);
                });
            } else {
                // otherwise we simply mark it as success
                if (formGroup != null)
                    formGroup.classList.add("has-success");
            }
        },
        ResetFormGroup: function (formGroup) {

            // Remove the success and error classes
            //var hasError = formGroup.getElementsByClassName("has-error");
            //while (hasError.length) {
            //    formGroup.classList.remove("has-error");
            //}
            if (formGroup != null) {
                if (formGroup.classList.value.includes("has-error"))
                    formGroup.classList.remove("has-error");


                if (formGroup.classList.value.includes("has-success"))
                    formGroup.classList.remove("has-success");
                // and remove any old messages
                $.each(formGroup.querySelectorAll(".text-danger"), function (i, el) {
                    el.parentNode.removeChild(el);
                });
            }
        },
        AddError: function (messages, error, input) {

            var block = document.createElement("p");
            block.classList.add("text-danger");
            block.classList.add("error");
            block.innerText = error;
            messages.appendChild(block);
            $(input).addClass("input-danger");
        },
        ClosestParent: function (child, className) {
            if (!child || child == document) {
                return null;
            }
            if (child.classList.contains(className)) {
                return child;
            } else {
                return Common.Form.ClosestParent(child.parentNode, className);
            }
        },


    },

    GetData: {
        Post: function (_url, _data) {
            var result = [];
            $.ajax({
                url: _url,
                type: 'POST',
                dataType: 'json',
                data: _data,
                async: false,
                cache: false,
            }).done(function (data, textStatus, jqXHR) {
                result = data;
            }).fail(function (jqXHR, textStatus, errorThrown) {
                result.respon.errorMessage = 'Get data failed!';
                result.respon.errorType = 1;
            });
            return result;
        },
        Get: function (_url) {
            var result = [];
            $.ajax({
                url: _url,
                type: "Get",
                async: false,
                cache: false,
            }).done(function (data, textStatus, jqXHR) {
                result = data;
            }).fail(function (jqXHR, textStatus, errorThrown) {
                result = [];
            });
            return result;
        },
        Get: function (_url, param) {
            var result = [];
            $.ajax({
                url: _url,
                data: param,
                type: "Get",
                async: false,
                cache: false,
            }).done(function (data, textStatus, jqXHR) {
                result = data;
            }).fail(function (jqXHR, textStatus, errorThrown) {
                result = [];
            });
            return result;
        },
    },
    Validation: {
        Number: function (number) {
            var numbers = /^[0-9]+$/;
            if (number.match(numbers)) {
                return true;
            } else {
                return false;
            }
        },
    },
    Helper: {
        CommaSeparation: function (yourNumber) {
            var temp = yourNumber + "";
            var value = parseFloat(temp.replace(/,/g, ""));
            if (value != "" && !isNaN(value)) {
                var n = parseFloat(value).toFixed(2).toString().split(".");
                //Comma-fies the first part
                n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                //Combines the two sections
                return n.join(".");
            } else {
                return "0.00";
            }
        },
        ReplaceComa: function (number) {
            return number.replace(/\s*,\s*|\s+,/g, '');
        },
        Datepicker: function () {
            var dateToday = new Date();
            $('.datePicker').datepicker({
                dateFormat: "dd-M-yy",
                autoclose: true,
            });
        },
        Dropdownlist: function (id, data) {
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    console.log("item: ", item);
                    $(id).append('<option value=' + item.value + '>' + item.text + '</option>');
                    //$(id)
                    //    .append($("<option>", { value: item.value })
                    //        .text(item.text));
                });
                $(id).select2();
            }
        },
        Encryption: function (data) {
            var params = { StrData: data };
            var result = Common.GetData.Post('/Helper/Encryption', params);
            return result;
        }
    }
}

var Theme = {
    Init: function (state) {
        Theme.Event();
        Theme.Theme($("#hdThemeID").val())
    },
    Theme: function (themeID) {
        if (themeID == 1) {
            //$('#themeicon').attr("class", "fa fa-moon-o");
            $('#themeicon').attr("class", "la la-sun-o");
            $('#themestyle').attr("href", "/Content/assets/demo/default/base/style.bundle.css");
            $('#themebg').attr("style", "background-image: url(../../Content/images/bgv2.jpg);  width:3000px; height:2000px;");
        }
        else {
            $('#themeicon').attr("class", "la la-sun-o");
            //$('#themestyle').attr("href", "/Content/assets/demo/default/base/styledark.bundle.css");
            //$('#themebg').attr("style", "background-image: url(../../Content/images/background/backgrounddark.png); height:3000px;");
			 //$('#themeicon').attr("class", "fa fa-moon-o");
            $('#themestyle').attr("href", "/Content/assets/demo/default/base/style.bundle.css");
            $('#themebg').attr("style", "background-image: url(../../Content/images/bgv2.jpg); width:3000px; height:2000px;");
        }
    },
    Event: function () {
        $("#chkTheme").unbind().on("click", function () {
            var themeID = 0;

            if ($('#themeicon').attr("class") == "la la-sun-o") {
                themeID = 1;
                Theme.Theme(themeID);
            }
            else {

                themeID = 2;
                Theme.Theme(themeID);
            }

            // Update to DB
            //$.ajax({
            //    url: "/api/user/SaveTheme/" + themeID,
            //    type: "POST",
            //}).done(function (data, textStatus, jqXHR) {
            //}).fail(function (jqXHR, textStatus, errorThrown) {
            //    Common.Alert.Error(errorThrown);
            //})
        });
    },
}
    
