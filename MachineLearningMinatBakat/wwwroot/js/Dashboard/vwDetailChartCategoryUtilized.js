var tblvwDetailChartCategoryUtilized='#tblvwDetailChartCategoryUtilized';
jQuery(document).ready(function () {

 var form = document.querySelector('form#vwDetailChartCategoryUtilizedForm');
 form.addEventListener('submit', function (ev) {
     ev.preventDefault();
     if (Common.Form.HandleFormSubmit(form, FormValidation)) {
         Form.Submit();
     }
 });
});

var Table = {
    CategoryUtilizedDetail: function (kodeCategory) {
        t = $("#divDetailKecerdasanChartList").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: "/Dashboard/GetDetailCategoryUtilizedList?KodeCategory=" + kodeCategory,
                        method: "GET",
                        map: function (r) {
                            var e = r;
                            return void 0 !== r.data && (e = r.data), e;
                        }
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                },
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false
            },
            layout: {
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            },
            search: {
                input: $("#tbxSearch")
            },
            columns: [
                { field: "nama", title: "Nama", textAlign: "left" },
                { field: "SangatTidakSetuju", title: "Jumlah Sangat Tidak Setuju", textAlign: "center" },
                { field: "TidakSetuju", title: "Jumlah Tidak Setuju", textAlign: "center" },
                { field: "KurangSetuju", title: "Jumlah Kurang Setuju", textAlign: "center" },
                { field: "Setuju", title: "Jumlah Setuju", textAlign: "center" },
                { field: "KodeCategory", title: "Kode Kecerdasan", textAlign: "center" }
            ]
        })
    }
}
