﻿$(function () {
    Table.Init();

    $("#btndownload").on("click", function () {
        window.location.href = "/Dashboard/ExportDataset";
    });
});

var Table = {
    Init: function () {
        //Common.Table.InitClient(tblMstJurusan);
        t = $("#divDatasetList").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: "/Dashboard/GetDatasetList",
                        method: "GET",
                        map: function (r) {
                            var e = r;
                            return void 0 !== r.data && (e = r.data), e;
                        }
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                },
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false
            },
            layout: {
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            },
            search: {
                input: $("#tbxSearch")
            },
            columns: [
                //{
                //    field: "idDataset", title: "Actions", sortable: false, textAlign: "center", template: function (t) {
                //        console.log("t: ", t);
                //        //var strBuilder = '<a href="javascript:;' + t.idJurusan + '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" id="btnEdit" title="Edit details"  onclick="Table.UpdateData(' + t.idJurusan + ', \'' + t.jurusan + '\', \'' + t.kodeJurusanCategory + '\', \'' + t.kodeKecerdasanCategory + '\' );"><i class="la la-edit"></i></a>\t\t\t\t\t\t';
                //        //strBuilder += '<a id="deleteConfirmation" href="javascript:;' + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=Table.RemoveData(' + t.idJurusan + ')><i class="la la-eraser"></i></a>';

                //        //return strBuilder;
                //    }
                //},
                { field: "nama", title: "Nama", textAlign: "left" },
                { field: "kodeCategory", title: "Kode Category", textAlign: "center" },
                { field: "sangatTidakSetuju", title: "SangatTidakSetuju", textAlign: "center" },
                { field: "tidakSetuju", title: "TidakSetuju", textAlign: "center" },
                { field: "kurangSetuju", title: "KurangSetuju", textAlign: "center" },
                { field: "setuju", title: "Setuju", textAlign: "center" }
            ]
        })
    }
}