﻿_PreviousMonthName = "";
_MonthName = "";
kodeCategory = "";
metode = "";

$(function () {
    Data.ChartAcuracy();
    Data.ChartAnalitics();
    Data.ChartAcuracyData();
    $(".pnlHeader").hide();
    $(".pnlHeaderAccuracy").hide();
    $("#divSummary").show();

    $(".btBack").click(function () {
        //$(".pnlHeader").hide();
        //$(".pnlHeaderAccuracy").hide();
        //$("#divSummary").show();
        //Data.ChartAcuracy();
        //Data.ChartAnalitics();
        //Data.ChartAcuracyData();
        window.location.href = "/Dashboard/DataAnalitics";
    });

    $("#btndownload").on("click", function () {
        window.location.href = "/Dashboard/ExportCategoryUtilized/" + kodeCategory;
    });

    $("#btndownloadAccuracy").on("click", function () {
        window.location.href = "/Dashboard/ExportAccuracyDetail/" + metode;
    });
});

var Control = {
    ID: {
        Chart: {
            DashByAccuracy: 'DashByAccuracy'
        },
    },
};

var Data = {
    ChartAcuracy: function () {
       
        var result = Common.GetData.Get('/Dashboard/GetDataList');
        if (Common.CheckError.Object(result)) {
            console.log(result);
            Chart.Pie("DashByAccuracy", result.data);
        }

    },
    ChartAcuracyData: function () {

        var result = Common.GetData.Get('/Dashboard/GetAccuracyDataList');
        if (Common.CheckError.Object(result)) {
            console.log(result);
            Chart.PieAccuracy("DashByAccuracyData", result.data);
        }

    },
    ChartAnalitics: function () {
        var result = Common.GetData.Get('/Dashboard/GetDataListAnalitics');
        if (Common.CheckError.Object(result)) {
            console.log(result);
            Chart.ColumnChart("dashByPecission", result.data);
        }
    }
}
var Chart = {
    Pie: function (divId, data) {
        console.log("charttt: ", divId, data, status)
        var resData = [];
        var colors = [];
        var count = 0;

        for (i = 0; i < data.length; i++) {
            var obj = new Object();

                obj.name = data[i].metode;
                obj.y = data[i].accuracy;
                obj.color = data[i].color;

            count = count + 1;
            var jsonString = JSON.stringify(obj);

            resData.push(obj);

        }

        if (data.length == 0) {
            $('#' + divId).show();
        } else {
            $('#' + divId).show();

            $('#' + divId).highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    //pointFormat: '<b> ({point.y:,.0f})</b>'
                    pointFormat: '<b>{point.y:.1f}%</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: 5,
                            format: '<b>{point.name}</b>: ({point.y:.0f}%) ' //'<b>{point.name}</b>: ({point.y:,.0f}) '
                        },
                        point: {
                            events: {
                                click: function (e) {
                                    console.log(e);
                                    kodeCategory = e.point.name;
                                    Table.Init(e.point.name);
                                }
                            }
                        },
                        showInLegend: true,
                        colors: colors
                    }
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: '',
                    //  innerSize: isStatus == 1 ? '0%' : '0%',
                    data: resData
                }]

            });
        }

    },
    PieAccuracy: function (divId, data) {
        var resData = [];
        var colors = [];
        var count = 0;

        for (i = 0; i < data.length; i++) {
            var obj = new Object();

            obj.name = data[i].metode;
            obj.y = data[i].accuracy;
            obj.color = data[i].color;

            count = count + 1;
            var jsonString = JSON.stringify(obj);

            resData.push(obj);

        }

        if (data.length == 0) {
            $('#' + divId).show();
        } else {
            $('#' + divId).show();

            $('#' + divId).highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    //pointFormat: '<b> ({point.y:,.0f})</b>'
                    pointFormat: '<b>{point.y:.1f}%</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: 5,
                            format: '<b>{point.name}</b>: ({point.y:.0f}%) ' //'<b>{point.name}</b>: ({point.y:,.0f}) '
                        },
                        point: {
                            events: {
                                click: function (e) {
                                    console.log(e);
                                    metode = e.point.name;
                                    Table.InitAccuracy(e.point.name);
                                }
                            }
                        },
                        showInLegend: true,
                        colors: colors
                    }
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: '',
                    //  innerSize: isStatus == 1 ? '0%' : '0%',
                    data: resData
                }]

            });
        }

    },
    ColumnChart: function (divId, data) {
        var resCategory = [];
        var resDataPreviousPeriod = [];
        var resDataCurrentPeriod = [];
        for (i = 0; i < data.length; i++) {
            var obj = new Object();
            obj.name = data[i].type;
            var data1 = data[0].type;
            var data2 = data[1].type;
            var data3 = data[2].type;

            resCategory.push(data[i].type);
            resDataPreviousPeriod.push(data[i].naiveBayes);
            resDataCurrentPeriod.push(data[i].knn);
        }
        $('#' + divId).highcharts({
            chart: {
                type: 'column'
            },
            title: false,
            subtitle: false,
            xAxis: {
                categories: resCategory,
                crosshair: true
            },
            yAxis: {
                min: 0,
                max: 12,
                tickInterval: 2,
                title: {
                    text: 'Total Point'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.2f} </b></td></tr>',
                footerFormat: '</table>',
                shared: false,
                useHTML: true
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        crop: false,
                        overflow: 'none'
                    }
                }
            },
            series: [{
                name: 'Naive Bayes',
                data: resDataPreviousPeriod,
                color: '#70AD47'

            }, {
                name: 'KNN',
                data: resDataCurrentPeriod,
                color: '#C6E0B4'

                },
                //{
                //    name: data3,
                //    data: resDataCurrentPeriod,
                //    color: '#c8e1b7'

                //},
            {
                type: "spline",
                name: "Target",
                data: [10, 10, 10, 10],
                color: "red"
            }
            ]
        });


    },
}

var Table = {
    Init: function (kodeCategory) {
        t = $("#divDetailKecerdasanChartList").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: "/Dashboard/GetDetailCategoryUtilizedList?KodeCategory=" + kodeCategory,
                        method: "GET",
                        map: function (r) {
                            var e = r;
                            return void 0 !== r.data && (e = r.data), e;
                        }
                    }
                },
                pageSize: 10,
                //saveState: {
                //    cookie: true,
                //    webstorage: true
                //},
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false,
            },
            layout: {
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            },
            search: {
                input: $("#tbxSearch")
            },
            columns: [
                { field: "nama", title: "Nama", textAlign: "left" },
                { field: "sangatTidakSetuju", title: "Jumlah Sangat Tidak Setuju", textAlign: "center" },
                { field: "tidakSetuju", title: "Jumlah Tidak Setuju", textAlign: "center" },
                { field: "kurangSetuju", title: "Jumlah Kurang Setuju", textAlign: "center" },
                { field: "setuju", title: "Jumlah Setuju", textAlign: "center" },
                { field: "kodeCategory", title: "Kode Kecerdasan", textAlign: "center" }
            ]
        });

        $(".pnlHeader").show();
        $(".pnlHeaderAccuracy").hide();
        $("#divSummary").hide();
    },

    InitAccuracy: function (metode) {
        t = $("#divDetailAccuracyChartList").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: "/Dashboard/GetDetailAccuracyList?metode=" + metode,
                        method: "GET",
                        map: function (r) {
                            var e = r;
                            return void 0 !== r.data && (e = r.data), e;
                        }
                    }
                },
                pageSize: 10,
                //saveState: {
                //    cookie: true,
                //    webstorage: true
                //},
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false
            },
            layout: {
                scroll: false,
                footer: false
            },
            sortable: true,
            pagination: true,
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 50, 100]
                    }
                }
            },
            search: {
                input: $("#tbxSearch")
            },
            columns: [
                //{
                //    field: "idKecerdasanCategory", title: "Actions", sortable: false, textAlign: "center", template: function (t) {
                //        console.log("data: ", t);
                //        var strBuilder = '<a href="javascript:;' + t.idKecerdasanCategory + '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" id="btnEdit" title="Edit details"  onclick="Table.UpdateData(' + t.idKecerdasanCategory + ', \'' + t.categoryName + '\');"><i class="la la-edit"></i></a>\t\t\t\t\t\t';
                //        strBuilder += '<a id="deleteConfirmation" href="javascript:;' + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=Table.RemoveData(' + t.idKecerdasanCategory + ')><i class="la la-eraser"></i></a>';

                //        return strBuilder;
                //    }
                //},
                { field: "userID", title: "Nama", textAlign: "left" },
                { field: "metode", title: "Metode", textAlign: "center" },
                { field: "accuracy", title: "Accuracy", textAlign: "center" },
                { field: "prediksi", title: "Prediksi", textAlign: "center" }
            ]
        })
        $(".pnlHeader").hide();
        $(".pnlHeaderAccuracy").show();
        $("#divSummary").hide();
    },

}