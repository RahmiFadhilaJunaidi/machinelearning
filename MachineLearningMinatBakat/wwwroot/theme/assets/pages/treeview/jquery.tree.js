/**
 * Theme: Uplon Admin Template
 * Author: Coderthemes
 * Tree view
 */

$(document).ready(function () {
    // Basic
    $('#basicTree').jstree({
        'core': {
            'themes': {
                'responsive': false
            },
            'data': [
                {
                    "id": "1", "parent": "#", "text": "Simple root node", 'state': {
                        'opened': true,
                    },
                },
                {
                    "id": "2", "parent": "#", "text": "Root node 2", 'state': {
                        'opened': true,
                    }, },
                {
                    "id": "3", "parent": "2", "text": "Child 1", 'state': {
                        'opened': true,
                    }, },
                {
                    "id": "4", "parent": "2", "text": "Child 2", 'state': {
                        'opened': true,
                    }, },
            ]
        },
        'types': {
            'default': {
                'icon': 'icofont icofont-folder'
            },
            'file': {
                'icon': 'icofont icofont-file-alt'
            }
        },
        'plugins': ['types']
    });

    // Checkbox
    $('#checkTree').jstree({
        'core': {
            'themes': {
                'responsive': false
            }
        },
        'types': {
            'default': {
                'icon': 'icofont icofont-folder'
            },
            'file': {
                'icon': 'icofont icofont-file-alt'
            }
        },
        'plugins': ['types', 'checkbox']
    });

    // Drag & Drop
    $('#dragTree').jstree({
        'core': {
            'check_callback': true,
            'themes': {
                'responsive': false
            }
        },
        'types': {
            'default': {
                'icon': 'icofont icofont-folder'
            },
            'file': {
                'icon': 'icofont icofont-file-alt'
            }
        },
        'plugins': ['types', 'dnd']
    });

    // Ajax
    $('#ajaxTree').jstree({
        'core': {
            'check_callback': true,
            'themes': {
                'responsive': false
            },
            'data': {
                'url': function (node) {
                    return node.id === '#' ? 'assets/plugins/jstree/ajax_roots.json' : 'assets/plugins/jstree/ajax_children.json';
                },
                'data': function (node) {
                    return { 'id': node.id };
                }
            }
        },
        "types": {
            'default': {
                'icon': 'icofont icofont-folder'
            },
            'file': {
                'icon': 'icofont icofont-file-alt'
            }
        },
        "plugins": ["contextmenu", "dnd", "search", "state", "types", "wholerow"]
    });
});