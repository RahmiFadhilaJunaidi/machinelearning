﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using TeamMate.Domain.Models;

namespace TeamMate.Web.Helpers
{
    public static class HTMLHelper
    {
        public static MvcHtmlString ProjectList(this HtmlHelper helper, List<vwProject> pinnedList)
        {
            String str = "";

            foreach (var pinnedProject in pinnedList)
            {
                str += "<li class='m-menu__item sidebarActive' id='/PinnedProject/" + pinnedProject.ProjectID + "'  aria-haspopup='true' style='margin-top: 10px'> <a href='/PinnedProject/" + pinnedProject.ProjectID + "' class='m-menu__link '> <i class='m-menu__link-bullet m-menu__link-bullet--dot'> <span></span> </i> <span class='m-menu__link-text'>" + pinnedProject.ProjectName + "</span> </a> </li>";
            }
            return MvcHtmlString.Create(str);
        }
    }
}