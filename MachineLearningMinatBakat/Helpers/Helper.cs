﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using TBGSys.Domain.Models;
using System.Xml;
using System.Web.Caching;
using System.Xml.Serialization;

namespace TBGSys.Web.Helper
{
    public static class Helper
    {
        #region Configuration

        /// <summary>
        /// Get Application ID
        /// </summary>
        /// <returns></returns>
        public static int GetApplicationID()
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings["ApplicationID"]);
        }

        /// <summary>
        /// Get Application Name
        /// </summary>
        /// <returns></returns>
        public static string GetApplicationName()
        {
            return ConfigurationManager.AppSettings["ApplicationName"].ToString();
        }

        /// <summary>
        /// Get Doc Path
        /// </summary>
        /// <returns></returns>
        public static string GetDocumentPath()
        {
            return ConfigurationManager.AppSettings["DocumentPath"].ToString();
        }

        /// <summary>
        /// Get Doc Path
        /// </summary>
        /// <returns></returns>
        public static string GetDocumentPath(string configName)
        {
            return ConfigurationManager.AppSettings[configName].ToString();
        }

        /// <summary>
        /// Get Doc Mobile Path
        /// </summary>
        /// <returns></returns>
        public static string GetDocumentMobilePath()
        {
            return ConfigurationManager.AppSettings["DocumentMobilePath"].ToString();
        }
        /// <summary>
        /// Get Idle Time
        /// </summary>
        /// <returns></returns>
        public static int GetIdleTime()
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings["IdleTime"]);
        }

        /// <summary>
        /// Get Image Path
        /// </summary>
        /// <returns></returns>
        public static string GetImagePath()
        {
            return ConfigurationManager.AppSettings["ImageTBGSys"].ToString();
        }

        /// <summary>
        /// Get User Image Path
        /// </summary>
        /// <returns></returns>
        public static string GetUserImagePath()
        {
            return ConfigurationManager.AppSettings["UserImagePath"].ToString();
        }

        /// <summary>
        /// Get Token Web Service MSS
        /// </summary>
        /// <returns></returns>
        public static string GetTokenWebService()
        {
            return ConfigurationManager.AppSettings["TokenWebService"];
        }

        /// <summary>
        /// Token Mobile
        /// </summary>
        /// <returns></returns>
        public static string GetMobileToken()
        {
            var Token = ConfigurationManager.AppSettings["MobileToken"];
            return Token;
        }

        public static string GetMobileBaseUrl()
        {
            var Url = ConfigurationManager.AppSettings["MobileBaseUrl"];
            return Url;
        }

        public static string MobileCreateTask()
        {
            var Url = ConfigurationManager.AppSettings["MobileCreateTask"];
            return Url;
        }

        public static string MobileGetTask()
        {
            var Url = ConfigurationManager.AppSettings["MobileGetTask"];
            return Url;
        }

        public static string MobileCancelTask()
        {
            var Url = ConfigurationManager.AppSettings["MobileCancelTask"];
            return Url;
        }

        public static string MobileGetResponse()
        {
            var Url = ConfigurationManager.AppSettings["MobileResponse"];
            return Url;
        }
        #endregion

        #region File

        /// <summary>
        /// Create Folder
        /// </summary>
        /// <param name="strPath"></param>
        public static void CreateNewFolder(string strPath)
        {
            if (!Directory.Exists(strPath))
            {
                DirectoryInfo di = Directory.CreateDirectory(strPath);
            }
        }

        /// <summary>
        /// Save File
        /// </summary>
        /// <param name="strFolderName"></param>
        /// <param name="postedFile"></param>
        /// <returns></returns>
        public static string UploadFile(string strFolderName, HttpPostedFile postedFile)
        {
            try
            {
                string timeStamp = Helper.GetTimeStamp();
                string targetPath = System.Web.HttpContext.Current.Server.MapPath(Helper.GetDocumentPath() + "\\" + strFolderName + "\\");
                Helper.CreateNewFolder(targetPath);
                targetPath = targetPath + timeStamp + "." + postedFile.FileName;
                postedFile.SaveAs(targetPath);
                return "\\" + strFolderName + "\\" + timeStamp + "." + postedFile.FileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Upload File Mobile
        /// </summary>
        /// <param name="strFolderName"></param>
        /// <param name="postedFile"></param>
        /// <returns></returns>
        public static string UploadFileMobile(string strFolderName, HttpPostedFile postedFile)
        {
            try
            {
                string timeStamp = Helper.GetTimeStamp();
                string targetPath = System.Web.HttpContext.Current.Server.MapPath(Helper.GetDocumentMobilePath() + "\\" + strFolderName + "\\");
                Helper.CreateNewFolder(targetPath);
                targetPath = targetPath + timeStamp + "." + postedFile.FileName;
                postedFile.SaveAs(targetPath);
                return "\\" + strFolderName + "\\" + timeStamp + "." + postedFile.FileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Function

        /// <summary>
        /// Get IP Address
        /// </summary>
        /// <returns></returns>
        public static string GetIPAddress()
        {
            //return HttpContext.Current.Request.UserHostAddress;
			
			// Look for a proxy address first
            string _ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            // If there is no proxy, get the standard remote address
            if (_ip == null || _ip.ToLower() == "unknown")
                _ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            return _ip;
        }

        /// <summary>
        /// Get Time Stamp
        /// </summary>
        /// <returns></returns>
        public static string GetTimeStamp()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmss");
        }

        /// <summary>
        /// Get Browser
        /// </summary>
        /// <returns></returns>
        public static string GetBrowser()
        {
            var strBrowser = HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version;
            return strBrowser;
        }

        /// <summary>
        /// Get Domain
        /// </summary>
        /// <returns></returns>
        public static string GetDomain()
        {
            try
            {
                IPAddress myIP = IPAddress.Parse(GetIPAddress());
                IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
                List<string> HostName = GetIPHost.HostName.ToString().Split('.').ToList();
                return HostName.Count() > 1 ? HostName[1] : "";
            }
            catch(Exception ex) { return "Error get domain " + ex.Message.ToString(); }

        }

        /// <summary>
        /// Get Device
        /// </summary>
        /// <returns></returns>
        public static string GetDevice()
        {
            try
            {
                IPAddress myIP = IPAddress.Parse(GetIPAddress());
                IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
                List<string> compName = GetIPHost.HostName.ToString().Split('.').ToList();
                return compName[0];
            }
            catch(Exception ex)
            { return "error Get Device "+ex.Message.ToString(); }
        }

        /// <summary>
        /// Get Mac Address
        /// </summary>
        /// <returns></returns>
        public static string GetMacAddress()
        {
            try
            {
                IPAddress myIP = IPAddress.Parse(GetIPAddress());
                IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
                var Mac = GetIPHost.AddressList[0].ToString();
                return Mac;
            }
            catch(Exception ex) { return "Error get Mac Address" + ex.Message.ToString(); }
            
        }

        /// <summary>
        /// Get User Agent
        /// </summary>
        /// <returns></returns>
        public static string GetUserAgent()
        {
            return HttpContext.Current.Request.UserAgent;
        }

        public static string NotificationList(List<vmUserNotification> notifiations)
        {
            string strNotification = "";

            foreach (vmUserNotification notification in notifiations)
            {
                TagBuilder liTag = new TagBuilder("li");

                TagBuilder aTag = new TagBuilder("a");
                aTag.MergeAttribute("href", notification.Url);

                TagBuilder spanTag = new TagBuilder("span");
                spanTag.AddCssClass("details");

                TagBuilder spanInnerTag = new TagBuilder("span");
                spanInnerTag.AddCssClass("label label-sm label-success");
                spanInnerTag.SetInnerText(notification.Count.ToString());

                spanTag.InnerHtml = spanInnerTag.ToString() + " " + notification.Notification;
                aTag.InnerHtml = spanTag.ToString();
                liTag.InnerHtml = aTag.ToString();

                strNotification += liTag.ToString();
            }

            return strNotification;
        }
        public static string BuildActionUri(string action)
        {
            return GetMobileBaseUrl() + action;
        }

        /// <summary>
        /// Replace URL Percent Encoding from JSON
        /// </summary>
        /// <param name="raw"></param>
        /// <returns></returns>
        public static string CleanJSONString(string raw)
        {
            return raw.Replace("%22", "\"").Replace("%2C", ",").Replace("%20", " ");
        }
        
        public static string ConvertDateTimeToIndDate(DateTime InvPrintDate)
        {
            if (InvPrintDate == null) return "";
            var month = "";
            switch (InvPrintDate.Month)
            {
                case 1:
                    month = "Januari";
                    break;
                case 2:
                    month = "Februari";
                    break;
                case 3:
                    month = "Maret";
                    break;
                case 4:
                    month = "April";
                    break;
                case 5:
                    month = "Mei";
                    break;
                case 6:
                    month = "Juni";
                    break;
                case 7:
                    month = "Juli";
                    break;
                case 8:
                    month = "Agustus";
                    break;
                case 9:
                    month = "September";
                    break;
                case 10:
                    month = "Oktober";
                    break;
                case 11:
                    month = "November";
                    break;
                case 12:
                    month = "Desember";
                    break;
            }
            return ((InvPrintDate.Day < 10) ? "0" + InvPrintDate.Day : InvPrintDate.Day.ToString()) + "-" + month + "-" + InvPrintDate.Year;
        }

        private static int GetBusinessDays(DateTime start, DateTime end)
        {
            if (start.DayOfWeek == DayOfWeek.Saturday)
            {
                start = start.AddDays(2);
            }
            else if (start.DayOfWeek == DayOfWeek.Sunday)
            {
                start = start.AddDays(1);
            }

            if (end.DayOfWeek == DayOfWeek.Saturday)
            {
                end = end.AddDays(-1);
            }
            else if (end.DayOfWeek == DayOfWeek.Sunday)
            {
                end = end.AddDays(-2);
            }

            int diff = (int)end.Subtract(start).TotalDays;

            int result = diff / 7 * 5 + diff % 7;

            if (end.DayOfWeek < start.DayOfWeek)
            {
                return result - 2;
            }
            else
            {
                return result;
            }
        }

        public static DateTime AddBusinessDays(DateTime date, int days)
        {
            if (days < 0)
            {
                throw new ArgumentException("days cannot be negative", "days");
            }

            if (days == 0) return date;

            if (date.DayOfWeek == DayOfWeek.Saturday)
            {
                date = date.AddDays(2);
                days -= 1;
            }
            else if (date.DayOfWeek == DayOfWeek.Sunday)
            {
                date = date.AddDays(1);
                days -= 1;
            }

            date = date.AddDays(days / 5 * 7);
            int extraDays = days % 5;

            if ((int)date.DayOfWeek + extraDays > 5)
            {
                extraDays += 2;
            }

            return date.AddDays(extraDays);

        }
        #endregion

        #region EMS
        public static string UploadEMSFile(string strFolderName, HttpPostedFile postedFile)
        {
            try
            {
                string timeStamp = Helper.GetTimeStamp();
                string targetPath = System.Web.HttpContext.Current.Server.MapPath(Helper.GetDocumentPath() + "\\" + strFolderName + "\\");
                Helper.CreateNewFolder(targetPath);
                targetPath = targetPath + timeStamp + "." + postedFile.FileName;
                postedFile.SaveAs(targetPath);
                return "\\" + strFolderName + "\\" + timeStamp + "." + postedFile.FileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Clone one entity data model to another with different type
        /// </summary>
        /// <typeparam name="T">Entity data model type to return</typeparam>
        /// <param name="source">Object source</param>
        /// <returns>Cloned object of entity data model</returns>
        public static T Copy<T>(this object source) where T : class
        {
            T instance = Activator.CreateInstance<T>();
            Type type = instance.GetType();
            foreach (var property in source.GetType().GetProperties())
            {
                PropertyInfo typeInfo = type.GetProperties().Where(w => w.Name.ToUpper() == property.Name.ToUpper()).FirstOrDefault();
                //type.GetProperty(property.Name);
                bool valid = typeInfo != null;
                if (valid)

                {
                    object value = property.GetValue(source, null);

                    if (value != null && value.GetType().IsClass)
                    {
                        var p = value.GetType();
                        if (!p.IsPrimitive && p != typeof(string) && !p.IsValueType)
                            continue;
                    }

                    Type propertyType = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
                    value = value == null ? null : Convert.ChangeType(value, propertyType);
                    typeInfo.SetValue(instance, value, null);
                }
            }

            return instance;
        }
        #endregion

        public static string XmlSerializer<T>(T dataToSerialize)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            StringWriter sw = new StringWriter();
            using (XmlWriter writer = XmlWriter.Create(sw, settings))
            {
                var xmlns = new XmlSerializerNamespaces();
                xmlns.Add(string.Empty, string.Empty);

                xsSubmit.Serialize(writer, dataToSerialize, xmlns);
                return sw.ToString();
            }
        }

        /// <summary>
        /// Deserialize XML to Object
        /// </summary>
        /// <typeparam name="T">Object</typeparam>
        /// <param name="xmlText">xml</param>
        /// <returns></returns>
        public static T XMLDeserializer<T>(string xmlText)
        {
            var stringReader = new System.IO.StringReader(xmlText);
            var serializer = new XmlSerializer(typeof(T));
            return (T)serializer.Deserialize(stringReader);
        }

        /// <summary>
        /// Save File
        /// </summary>
        /// <param name="strFolderName"></param>
        /// <param name="postedFile"></param>
        /// <returns></returns>
        public static string UploadReturnTimeStampFile(string strFolderName, string ConfigName, HttpPostedFile postedFile)
        {
            try
            {
                string timeStamp = Helper.GetTimeStamp();
                string targetPath = HttpContext.Current.Server.MapPath(Helper.GetDocumentPath(ConfigName) + "\\" + strFolderName + "\\");
                Helper.CreateNewFolder(targetPath);
                targetPath = targetPath + timeStamp + "." + postedFile.FileName;
                postedFile.SaveAs(targetPath);
                return timeStamp + "." + postedFile.FileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UploadFile(string strFolderName, string ConfigName, string FileName, HttpPostedFile postedFile)
        {
            try
            {
                string targetPath = HttpContext.Current.Server.MapPath(Helper.GetDocumentPath(ConfigName) + "\\" + strFolderName + "\\");
                Helper.CreateNewFolder(targetPath);
                targetPath = targetPath + FileName;
                postedFile.SaveAs(targetPath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public static class CacheExtensions
    {
        public static T GetOrStore<T>(this Cache cache, string key, Func<T> generator)
        {
            var result = cache[key];
            if (result == null)
            {
                result = generator();
                cache[key] = result;
            }
            return (T)result;
        }
    }

}