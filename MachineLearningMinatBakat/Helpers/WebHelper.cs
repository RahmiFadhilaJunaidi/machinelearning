﻿using Microsoft.AspNetCore.Http;
using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Ubiety.Dns.Core;
using Request = Ubiety.Dns.Core.Request;

namespace MachineLearningMinatBakat
{
    public static class WebHelper
    {
        /// <summary>
        /// Get IP Address
        /// </summary>
        /// <returns></returns>
        //public static string GetIPAddress()
        //{
        //    return Request.HttpContext.Connection.RemoteIpAddress;
        //}

        public static string GetDocumentPath()
        {
            return ConfigurationManager.AppSettings["DocPath"].ToString();
        }

    }
}