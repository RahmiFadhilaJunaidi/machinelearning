﻿using MachineLearningMinatBakat.DA.Service;
using Models.MachineLearningMinatBakat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MachineLearningMinatBakat.Provider
{
    public class UserManager
    {
        //public static MstUser User
        //{
        //    get
        //    {
        //        if (HttpContext.Current.User.Identity.IsAuthenticated)
        //        {
        //            return ((MyPrincipal)(HttpContext.Current.User)).User;
        //        }
        //        else if (HttpContext.Current.Items.Contains("UserLogin"))
        //        {
        //            return (vmUserLogin)HttpContext.Current.Items["UserLogin"];
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}

        public static mstUser AuthenticateUser(string strUserID, string strPassword)
        {
            mstUser userLogin = new mstUser();

            mstUser login = new mstUser();
            login.userID = strUserID;
            login.password = strPassword;
            //login.IP = WebHelper.GetIPAddress();
            userLogin = AccessService.Login(login);

            return userLogin;
        }

        //public static bool ValidateUser(vmLogin login, HttpResponseBase response)
        //{
        //    bool result = false;

        //    if (Membership.ValidateUser(login.UserID, login.Password))
        //    {
        //        var serializer = new JavaScriptSerializer();
        //        string userData = serializer.Serialize(UserManager.User);

        //        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
        //                login.UserID,
        //                DateTime.Now,
        //                DateTime.Now.AddMinutes(Helper.GetIdletime()),
        //                true,
        //                userData,
        //                FormsAuthentication.FormsCookiePath);

        //        string encTicket = FormsAuthentication.Encrypt(ticket);
        //        response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));

        //        result = true;
        //    }

        //    return result;
        //}

        //public static void Logout(HttpSessionStateBase session, HttpResponseBase response)
        //{
        //    session.Abandon();

        //    AccessServices.Logout(UserManager.User.UserToken);

        //    FormsAuthentication.SignOut();

        //    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
        //    cookie.Expires = DateTime.Now.AddYears(-1);
        //    response.Cookies.Add(cookie);
        //}
    }
}
